# IBM DataPower Operations Dashboard Simulator

The IBM DPOD Simulator is a collection of Docker containers that simulate a complete software integration environment monitored by DPOD. The simulator deploys all required configuration, services and applications and invokes demo transactions, so you can experience, learn and demonstrate the full power and value of IBM(C) DataPower(R) Operations Dashboard.

* **Version:** 2.5
* **Source:** [MSC-DPOD-Simulator](https://bitbucket.org/montier/msc-dpod-simulator)
* **License:** [Artistic License 2.0](LICENSE.md)
* **Copyright:** Copyright (C) 2015-2024 by [MonTier Software (2015) LTD](http://www.mon-tier.com/)
* **DPOD Documentation:** [https://ibm.biz/dpod-docs](https://ibm.biz/dpod-docs)


# Demonstration Scenarios and Videos

Once you setup and run the simulator Docker environment (see instructions below), we recommend demonstrating some of the most valuable features, or learning by example, using the following links:

1.  [Web Console Introduction](./scenarios/01-WebConsoleIntroduction.md) | [**Video**](https://youtu.be/VLxE9GrhF1U)
2.  [Troubleshooting a Back-End Latency Issue](./scenarios/02-TroubleshootingABackEndLatencyIssue.md) |  [**Video**](https://youtu.be/7JdQ9rsr5oo)
3.  [Troubleshooting a Side-Call Latency Issue](./scenarios/03-TroubleshootingASideCallLatencyIssue.md) |  [**Video**](https://youtu.be/cQrp39KpOV8)
4.  [Troubleshooting an API Error as A Developer using Self-Service](./scenarios/04-TroubleshootingAnApiErrorAsADeveloperUsingSelfService.md) |  [**Video**](https://youtu.be/fdCNXh72lL4)
5.  [Troubleshooting Malfunctioning Services](./scenarios/05-TroubleshootingMalfunctioningServices.md) |  [**Video**](https://youtu.be/NyVFi3NUavI)
6.  [Troubleshooting a Major Out-of-Service Issue](./scenarios/06-TroubleshootingAMajorOutOfServiceIssue.md)
7.  [Maintenance Plans: Backup, Sync & Firmware Upgrade](./scenarios/07-MaintenancePlansBackupSyncFirmwareUpgrade.md) |  [**Video**](https://youtu.be/85YZggkeVxM)
8.  APM / Splunk Integration (will be available soon)
9.  DevOps Services Portal (will be available soon)
10. [Investigating Common API-Connect Problems](./scenarios/10-InvestigatingCommonApiConnectProblems.md) | [**Video**](https://youtu.be/-3KHo6UqXfg)
11. [Investigating latency problems in API-Connect](./scenarios/11-InvestigatingCommonAPI-ConnectProblems.md) | [**Video**](https://youtu.be/__YnlNodL8Q)


# Included Containers

* **IBM(C) DataPower(R) Operations Dashboard** (Docker Container for Developer Edition)
* **IBM(C) DataPower(R) Gateway(s)** (with standalone services and API-Connect APIs that are automatically added to DPOD)
* **Splunk(R) Enterprise** (with DPOD Splunk App installed to demonstrate DPOD's integration with APM/Splunk)
* **IBM(C) MQ** (with a Queue Manager and a few local queues used by the services deployed on the IDG instance(s))
* **Generator** (used to deploy, configure and generate transactions on the IDG instance(s))


# Prerequisites

## Prerequisites for Linux:

* Physical or virtual machine.
* RHEL/Rocky Linux/Ubuntu Operating System.
* Intel-based machine, with a minimum of Intel i7 CPU.
* 12-14GB RAM (see table below).
* 25 GB free space in the Docker working directory (/var/lib/docker).
* The user should be part of the `sudoers` group and should be able to run the `sudo` command.
* The following packages should be installed: `curl` and `tar`.
* For RHEL-8, please consider the following commands that might be required to install packages:
	```
	subscription-manager register
	subscription-manager attach --auto
	subscription-manager repos --enable rhel-8-for-x86_64-baseos-rpms 
	subscription-manager repos --enable rhel-8-for-x86_64-appstream-rpms
	```
* For RHEL-9, please consider the following commands that might be required to install packages:
	```
	subscription-manager register
	subscription-manager attach --auto
	subscription-manager repos --enable rhel-9-for-x86_64-baseos-rpms 
	subscription-manager repos --enable rhel-9-for-x86_64-appstream-rpms
	```

## Prerequisites for Mac:

* Intel-based machine (Apple M1/M2 chips are not supported).
* 8 CPU cores.
* 12-14GB RAM (see table below).
* 25 GB free space in the Docker working directory.
* Docker Desktop should be installed by following [these instructions](https://docs.docker.com/docker-for-mac/install/).
* Docker Desktop should be configured to use up to 8 cores and 12-14GB of RAM (see table below).
* **Note:** Check the value of `credsStore` parameter in `~/.docker/config.json`. If the value is `desktop` - delete the parameter or change it to a credentials store which is supported by MacOS, for example `osxkeychain`:
	```
	"credsStore": "osxkeychain"
	```

## Prerequisites for Windows:

* You may use VMware Workstation to create a Linux VM on your Windows machine and follow the prerequisites for Linux.
* You may use Windows Subsystem for Linux (WSL 2) to launch Ubuntu:
	* It is mandatory to use WSL 2 for running Docker, see [comparison between WSL 1 and WSL 2](https://learn.microsoft.com/en-us/windows/wsl/compare-versions).
	* Ubuntu on WSL uses `iptables-nft` by default. You need to switch to `iptables-legacy` for Docker to work (see [this GitHub issue](https://github.com/docker/for-linux/issues/1406#issuecomment-1183487816)):
		```
		sudo update-alternatives --config iptables
		```
		Enter 1 to select `iptables-legacy`.
	* Follow the prerequisites for Linux.


# Running the Simulator

Download the simulator and set up the Docker environment:
	```bash
	sudo rm -fR ~/msc-dpod-simulator
	mkdir -p ~/msc-dpod-simulator/src/msc-dpod-simulator ~/msc-dpod-simulator/resources
	cd ~/msc-dpod-simulator/src/msc-dpod-simulator
	curl -s -L -o msc-dpod-simulator.tgz https://bitbucket.org/montier/msc-dpod-simulator/get/master.tar.gz
	tar -xzf msc-dpod-simulator.tgz -C . --strip-components=1
	cd ~/msc-dpod-simulator
	find . -name '*.sh' -type f | xargs chmod +x
	~/msc-dpod-simulator/src/msc-dpod-simulator/scripts/setupDockerEnv.sh
	```

Choose which containers to start and use the command in the following table to build and start them:

| Containers | RAM | Commmand |
| --- | --- | --- |
| **DPOD + 1 IDG** | 12GB | `~/msc-dpod-simulator/src/msc-dpod-simulator/scripts/dpodSimulator.sh -a build` |
| **DPOD + 2 IDGs** | 14GB | `~/msc-dpod-simulator/src/msc-dpod-simulator/scripts/dpodSimulator.sh -a build -i` |
| **DPOD + 1 IDG + Splunk** | 12GB | `~/msc-dpod-simulator/src/msc-dpod-simulator/scripts/dpodSimulator.sh -a build -k` |
| **DPOD + 2 IDGs + Splunk** | 14GB | `~/msc-dpod-simulator/src/msc-dpod-simulator/scripts/dpodSimulator.sh -a build -i -k` |

**Once the operation is complete, the simulator environment is up and running!**

This operation may take a while to complete. Make sure that the SSH session does not get disconnected during the command execution.


# Stopping the Simulator

This operation stops all containers, but keeps the images ready for rerunning:

```bash
~/msc-dpod-simulator/src/msc-dpod-simulator/scripts/dpodSimulator.sh -a stop
```

# Restarting the Simulator (after it has been stopped)

This operation starts all containers and simulates traffic on IDG and DPOD, using the flags previously set in the `build` action:

```bash
~/msc-dpod-simulator/src/msc-dpod-simulator/scripts/dpodSimulator.sh -a start
```

# Cleaning the simulator environment

This operation stops all simulator containers, removes them, and removes all simulator images and networks from the Docker configuration:

```bash
~/msc-dpod-simulator/src/msc-dpod-simulator/scripts/dpodSimulator.sh -a clean
```

# Login to DPOD / IDG / Splunk

After the simulator is up and running, you may login using the following credentials:

| Service | URL / Port | User Name | Password |
| --- | --- | --- | --- |
| DPOD Web | https://<VM/Local_IP_Address>:4433/op | admin | adminuser <sup>**(1)**</sup> |
| DPOD Admin Console | https://<VM/Local_IP_Address>:4433/admin | admin | adminuser <sup>**(1)**</sup> |
| IDG1 Web | https://<VM/Local_IP_Address>:9091 | admin | admin |
| IDG2 Web | https://<VM/Local_IP_Address>:9092 | admin | admin |
| Splunk Web | http://<VM/Local_IP_Address>:8000  | -  | - |
| DPOD SSH | <VM/Local_IP_Address>:4022 | root | dpod |
| IDG1 SSH | <VM/Local_IP_Address>:9021 | admin | admin |
| IDG2 SSH | <VM/Local_IP_Address>:9022 | admin | admin |

<sup>**(1)**</sup> After the first login, it is required to change the default password for **admin**.

# Logs

All scripts log files are located under `~/msc-dpod-simulator/resources/logs`.
