<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:dp="http://www.datapower.com/extensions"
    xmlns:dpconfig="http://www.datapower.com/param/config"
    extension-element-prefixes="dp"
    exclude-result-prefixes="dp dpconfig">

	<xsl:output method="xml"/>

	<xsl:template match="/">
	
		<xsl:variable name="account">
			<xsl:value-of select="/*[local-name() = 'Envelope']/*[local-name() = 'Body']/*[local-name() = 'getBalance']/*[local-name() = 'arg0']"/>
		</xsl:variable>
	
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ban="http://bankA.sample.ibm.com/">
			<soapenv:Header/>
			<soapenv:Body>
				<ban:getBalanceResponse>
					<xsl:choose>
<!--						<xsl:when test="/*/*/*/[arg0='7777777']">   -->
						<xsl:when test="$account != '7777777'">
							<return>-99999</return>
						</xsl:when>
						<xsl:otherwise>
							<return>20000</return>
						</xsl:otherwise>
					</xsl:choose>
				</ban:getBalanceResponse>
			</soapenv:Body>
		</soapenv:Envelope>
	</xsl:template>

</xsl:stylesheet>