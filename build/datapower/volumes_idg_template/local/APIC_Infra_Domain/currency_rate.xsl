<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:dp="http://www.datapower.com/extensions"
    xmlns:dpconfig="http://www.datapower.com/param/config"
    extension-element-prefixes="dp"
    exclude-result-prefixes="dp dpconfig">

	<xsl:output method="xml"/>

	<xsl:param name="dpconfig:Currency" select="''" />
	<dp:param name="dpconfig:Currency" xmlns="">
		<display>Currency</display>
		<description>
            Currency (USD/EUR)
		</description>
		<tab-override>basic</tab-override>
		<default/>
		<required-when>
			<condition evaluation="logical-true" />
		</required-when>
	</dp:param>
	
	
	<xsl:template match="/">
	
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ban="http://bankA.sample.ibm.com/">
			<soapenv:Header/>
			<soapenv:Body>
				<ban:ConvertBalanceResponse>
					<xsl:choose>
						<xsl:when test="$dpconfig:Currency = 'USD'">
							<rate>0.25</rate>
						</xsl:when>
						<xsl:otherwise>
							<rate>0.2</rate>
						</xsl:otherwise>
					</xsl:choose>
				</ban:ConvertBalanceResponse>
			</soapenv:Body>
		</soapenv:Envelope>
	</xsl:template>

</xsl:stylesheet>