var apim = require('apim');

apim.setvariable('ExchangeRate', apim.getvariable('CurrencyRateResponse'), 'set'); 
var CalculatedRate = Number(apim.getvariable('ExchangeRate'))* Number(apim.getvariable('CustomerBalanceToConvert'));

apim.setvariable('ConvertResponse.ConvertResponse.ConvertedBalance', CalculatedRate, 'set'); 
apim.setvariable('CalculatedConvertedBalance', CalculatedRate, 'set');

session.output.write(apim.getvariable('ConvertResponse'));
apim.output('application/json');