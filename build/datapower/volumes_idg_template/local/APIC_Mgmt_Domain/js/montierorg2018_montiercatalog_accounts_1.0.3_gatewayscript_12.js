var apim = require('apim');

// Compose json error response
var error_message = { 'httpCode': '500',
                      'httpMessage': 'Internal Server Error',
                      'moreInformation': 'Request is missing a mandatory field'
                     };
                     
// Set HTTP error info                     
apim.setvariable("message.status.code", 500);
apim.setvariable("message.status.reason", "Internal Server Error");
                     
context.message.body.write(error_message);

// Write error to console
console.error("Request is missing a mandatory field");