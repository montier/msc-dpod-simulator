var apim = require('apim');

// Write to log
console.log("Customer id: " + apim.getvariable('request.parsed_body.data.customer_id') + "; Customer Balance: " + apim.getvariable('CustomerBalance') + "; Content-type: " + apim.getvariable('message.headers.Content-Type'));

if (Number(apim.getvariable('CustomerBalance')) < Number(apim.getvariable('AmountToWithdraw'))) {
    session.output.write({"withdrawalResponse":"You do not have enough money in your account"});
}
else
{
   session.output.write({"withdrawalResponse":"Amount successfully withdrawn"});
}