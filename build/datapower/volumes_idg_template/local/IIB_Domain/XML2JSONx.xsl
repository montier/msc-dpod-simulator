<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx" exclude-result-prefixes="json ">

	<!-- 
       This script handles conversion of XML format to XML.
       Datapower can later convert JSONx to JSON.
       
       JSONx is created using predefined rules.
       These rules are the basis of converting JSONx format to XML.
       
       1. All JSONx objects are prefixed with "json:" prefix.
          All XMLs are wrapped with a parent <json:object></json:object>
          Example:
            In XML:
                    <SomeRequest>
                     <Ticker>IBM</Ticker>
                    </SomeRequest>
                    
            In JSONx:                   
                    <json:object>
                      <json:object name="SomeRequest">
                        <json:string name="Ticker">IBM</json:string>
                      </json:object>
                    </json:object>
                                        
            In JSON:                   
                    { "SomeRequest": 
                      { "Ticker" : "IBM" }
                    }
            
       2. These are the known/supported object types:
          A. json:object - This is a parent element.
          B. json:array -  will have a list of sub elements. 
          C. json:number - numbers with quation marks are string, while numbers without
                           quatation marks are treated as integers.
          D. json:null - An object defined JSON-wise as null
                          
       3. This script assumes that all parent elements are json "object" and not array.
        
        -->

	<xsl:output method="xml" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:element name="json:object">
			<!--<xsl:namespace name="xsi" select="'http://www.w3.org/2001/XMLSchema-instance'"/> 
      <xsl:namespace name="json" select="'http://www.ibm.com/xmlns/prod/2009/jsonx'"/> 
      <xsl:namespace name="schemaLocation" select="'http://www.datapower.com/schemas/json jsonx.xsd'"/>   -->
			<xsl:apply-templates select="*"/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="*">
		<xsl:choose>
			<!-- This is a parent element, and shall be treated as json:object -->
			<xsl:when test="*">
				<xsl:element name="json:object">
					<xsl:attribute name="name">
						<xsl:value-of select="local-name()"/>              
					</xsl:attribute>
					<xsl:apply-templates select="*"/>          
				</xsl:element>         
			</xsl:when>
			<!-- This is a leef element (no children), and shall be treated as json:string -->
			<xsl:otherwise>
				<xsl:element name="json:string">
					<xsl:attribute name="name">
						<xsl:value-of select="local-name()"/>
					</xsl:attribute>
					<xsl:value-of select="."/>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>  