<?xml version="1.0" encoding="UTF-8"?>

<!-- *MonTier* Copyright (C) 2015 MonTier Software (2015) LTD - All Rights Reserved *MonTier* This source is licensed under the Apache License 2.0 Objective: 
	========== Logic: ====== The XSL reads basic info from service vars, and uses dp:time-value() to extract the current time. Then, sets the fields of the 
	syslog msg to be sent to the log-target. Then, the log msg is constructed from these fields. Finally, if necessary, the syslog-msg is truncated since there 
	is a limit-size to the syslog msgs. Versions: ========= V1.0 - 21.2.2017 : Base version -->
	
	
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:env11="http://schemas.xmlsoap.org/soap/envelope/" xmlns:env12="http://www.w3.org/2001/12/soap-envelope"
	xmlns:dp="http://www.datapower.com/extensions" xmlns:dpconfig="http://www.datapower.com/param/config" xmlns:exslt="http://exslt.org/common"
	xmlns:date="http://exslt.org/dates-and-times" extension-element-prefixes="dp" exclude-result-prefixes="dp dpconfig env11 env12 exslt date fn xs fo ">
	<xsl:output method="xml" />

	<xsl:template match="/">
		<Request>
			<processingTime>
				<xsl:value-of select="request/args/arg[@name='processingTime']"/>
			</processingTime>
			<count>
				<xsl:value-of select="request/args/arg[@name='count']"/>
			</count>
		</Request>
		
	</xsl:template>

</xsl:stylesheet>