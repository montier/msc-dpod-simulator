<?xml version="1.0" encoding="UTF-8"?>

<!-- *MonTier* Copyright (C) 2015 MonTier Software (2015) LTD - All Rights Reserved *MonTier* This source is licensed under the Apache License 2.0  -->
		
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:env11="http://schemas.xmlsoap.org/soap/envelope/" xmlns:env12="http://www.w3.org/2001/12/soap-envelope"
	xmlns:dp="http://www.datapower.com/extensions" xmlns:dpconfig="http://www.datapower.com/param/config" xmlns:exslt="http://exslt.org/common"
	xmlns:date="http://exslt.org/dates-and-times" extension-element-prefixes="dp" exclude-result-prefixes="dp dpconfig env11 env12 exslt date fn xs fo ">
	<xsl:output method="xml" />

	
	<xsl:template match="/">
		
		<xsl:variable name="transactionRuleType" select="dp:variable('var://service/transaction-rule-type')" />  
		
		<!-- end span -->
		<xsl:variable name="extractedContext">
			<xsl:call-template name="endSpan">
				<xsl:with-param name="transactionRuleType" select="$transactionRuleType"/> 
			</xsl:call-template>
		</xsl:variable>
		
		<!-- gen context -->
		<xsl:variable name="generatedContext">
			<xsl:call-template name="genContext">
				<xsl:with-param name="extractedTraceId" select="substring($extractedContext,1,16)" /> 
				<xsl:with-param name="extractedSpanId" select="substring($extractedContext,18,16)" /> 
				<xsl:with-param name="extractedParentId" select="substring-before(substring($extractedContext,35), ':')" /> 
			</xsl:call-template>
		</xsl:variable>
		
		<!-- start span -->
		<xsl:call-template name="startSpan">
			<xsl:with-param name="generatedContext" select="$generatedContext" /> 
			<xsl:with-param name="transactionRuleType" select="$transactionRuleType"/>
		</xsl:call-template>

	</xsl:template>
	
 
	
	<!-- ==============   END SPAN    ============== -->
	
	<xsl:template name="endSpan">
		<xsl:param name="transactionRuleType" />
		
		<!-- If we are in a request rule, take the value from the request header , else - take it from the response header -->
		<xsl:variable name="extractedContext">
			<xsl:choose>
				<xsl:when test="$transactionRuleType = 'request'">
					<xsl:value-of select="dp:http-request-header('Uber-Trace-Id')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="dp:http-response-header('Uber-Trace-Id')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:message dp:priority="info">
			<xsl:text>OpenTracing:: endSpan :: </xsl:text>
			<xsl:value-of select="$extractedContext"/>
		</xsl:message>
		
		<xsl:copy-of select="$extractedContext" />
	
	</xsl:template>
	

	<!-- ==============   GENARATE CONTEXT    ============== -->
	
	<xsl:template name="genContext">
		<xsl:param name="extractedTraceId" />
		<xsl:param name="extractedSpanId" />
		<xsl:param name="extractedParentId" />
		
		<xsl:variable name="uuid" select="dp:generate-uuid()"/>
		
		<!-- generatedSpanId: extract 16 characters from uuid (cut-out the first 16, and remove minuses) -->
		<xsl:variable name="generatedSpanId" select="concat(substring($uuid,20,4), substring($uuid,25))"/>
		
		<xsl:message dp:priority="info">
			<xsl:text>OpenTracing:: uuid :: </xsl:text>
			<xsl:copy-of select="$uuid"/>
		</xsl:message>
		
		<!-- generatedParentId: extractedSpanId or 0 -->
		<xsl:variable name="generatedParentId">
			<xsl:choose>
				<xsl:when test="string-length($extractedSpanId) = 0">0</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="$extractedSpanId" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="generatedTraceId">
			<xsl:choose>
				<xsl:when test="string-length($extractedTraceId) = 0">
					<xsl:copy-of select="$generatedSpanId" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="$extractedTraceId" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="envJson">
			<xsl:call-template name="genEnvJson"/>
		</xsl:variable>
		
		<!-- new context:  extractedTraceId:generatedSpanId:generatedParentId:1 -->
		<xsl:variable name="generatedContext" select="concat($generatedTraceId, ':', $generatedSpanId, ':', $generatedParentId, ':1')"/>
		
		<xsl:message dp:priority="info">
			<xsl:text>OpenTracing:: generatedContext :: </xsl:text>
			<xsl:copy-of select="concat($generatedContext, ' ', $envJson)"/>
		</xsl:message>
		
		<xsl:copy-of select="$generatedContext" />
		
	</xsl:template>

	
	<!-- ==============   START SPAN    ============== -->
	<xsl:template name="genEnvJson">
		
		<xsl:variable name="incomingFullUrl" select="dp:variable('var://service/URL-in')" />
		<xsl:variable name="deviceName">
			<xsl:value-of select="dp:variable('var://service/system/ident')/identification /device-name"/>
		</xsl:variable> 
		<xsl:variable name="domainName" select="dp:variable('var://service/domain-name')"/>
		<xsl:variable name="protocol" select="dp:variable('var://service/protocol')"/>
		<xsl:variable name="clientIP" select="dp:variable('var://service/transaction-client')" />
					
		<xsl:variable name="generatedEnvJson">{"env":{"incomingURL":"<xsl:copy-of select="$incomingFullUrl"/>","device":"<xsl:copy-of select="$deviceName"/>","domain":"<xsl:copy-of select="$domainName"/>","protocol":"<xsl:copy-of select="$protocol"/>","clientIP":"<xsl:copy-of select="$clientIP"/>"}}</xsl:variable>
		
		<xsl:copy-of select="$generatedEnvJson" />
		
	</xsl:template>
	
	
	<!-- ==============   START SPAN    ============== -->
	
	<xsl:template name="startSpan">
		<xsl:param name="generatedContext"/>
		<xsl:param name="transactionRuleType"/>
		
		<xsl:choose>
			<xsl:when test="$transactionRuleType = 'request'">
				<dp:set-http-request-header name="'Uber-Trace-Id'" value="$generatedContext"/>
			</xsl:when>
			<xsl:otherwise>
				<dp:set-http-response-header name="'Uber-Trace-Id'" value="$generatedContext"/>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:message dp:priority="info">
			<xsl:text>OpenTracing:: startSpan :: </xsl:text>
			<xsl:value-of select="$generatedContext"/>
		</xsl:message>
		
	</xsl:template>

</xsl:stylesheet>