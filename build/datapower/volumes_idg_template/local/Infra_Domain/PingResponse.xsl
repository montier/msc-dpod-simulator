<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:dp="http://www.datapower.com/extensions" xmlns:dpconfig="http://www.datapower.com/param/config"
				xmlns:exslt="http://exslt.org/common"
				extension-element-prefixes="dp" 
				exclude-result-prefixes="dp dpconfig env11 env12 exslt">
	
	<xsl:output method="xml" />


	<xsl:template match="/">

		<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">
		   <soap:Header/>
		   <soap:Body>
			  <tem:PingResponse>
				 <tem:PingResult>Success !</tem:PingResult>
			  </tem:PingResponse>
		   </soap:Body>
		</soap:Envelope>

	</xsl:template>
</xsl:stylesheet>
