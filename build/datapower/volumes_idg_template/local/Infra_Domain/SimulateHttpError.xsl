<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:dp="http://www.datapower.com/extensions" xmlns:dpconfig="http://www.datapower.com/param/config"
				xmlns:exslt="http://exslt.org/common"
				extension-element-prefixes="dp" 
				exclude-result-prefixes="dp dpconfig env11 env12 exslt">

	<xsl:output method="xml" />


	<xsl:template match="/">
	
		<dp:set-response-header name="'x-dp-response-code'" value="'500 FAIL'"/>   
	
	</xsl:template>
</xsl:stylesheet>
