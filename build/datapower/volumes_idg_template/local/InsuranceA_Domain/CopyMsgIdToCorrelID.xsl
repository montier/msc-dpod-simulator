<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:dp="http://www.datapower.com/extensions"
				extension-element-prefixes="dp"
				exclude-result-prefixes="dp" version="1.0">

	<xsl:output method="xml"/>
	<xsl:template match="/">

		<xsl:variable name="entries" select="dp:request-header('MQMD')"/>
		<xsl:variable name="mqmd-headers" select="dp:parse($entries)"/>
		<xsl:variable name="CorrelId-RQ" select="$mqmd-headers//CorrelId"/>
		<xsl:variable name="MsgId-RQ" select="$mqmd-headers//MsgId"/>

		<xsl:message dp:priority="debug">
			<xsl:value-of select="concat('The Request MQMD : ', $entries)"/>
		</xsl:message>

		<xsl:variable name="current-headers" select="$mqmd-headers"/>
		<xsl:variable name="MQMDStr">
			<MQMD>
				<xsl:for-each select="$current-headers/MQMD/*[local-name()]">
					<xsl:variable name="header-name" select="local-name()"/>
					<xsl:choose>
						<xsl:when test="(normalize-space($header-name) = 'CorrelId')">
							<xsl:element name="CorrelId">
								<xsl:value-of select="$MsgId-RQ"/>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
							<xsl:element name="{$header-name}">
								<xsl:value-of select="."/>
							</xsl:element>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</MQMD>
		</xsl:variable>
		<xsl:variable name="MQMDStr2">
			<dp:serialize select="$MQMDStr" omit-xml-decl="yes"/>
		</xsl:variable>
		<dp:set-response-header name="'MQMD'" value="$MQMDStr2"/>
	</xsl:template>
</xsl:stylesheet>