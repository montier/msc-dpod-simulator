<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:env11="http://schemas.xmlsoap.org/soap/envelope/"
				xmlns:env12="http://www.w3.org/2001/12/soap-envelope"
				xmlns:dp="http://www.datapower.com/extensions" 
				xmlns:dpconfig="http://www.datapower.com/param/config"
			    xmlns:dpquery="http://www.datapower.com/param/query"
				xmlns:mmi="http://ca.com/unicenter/wsdm/wsdm31mmi"
				xmlns:regexp="http://exslt.org/regular-expressions"	
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="dp"
				exclude-result-prefixes="dp dpconfig env11 env12 exsl regexp"
				version="1.0">


	<xsl:param name="dpconfig:LDAPserverAddress" select="''"/>
	<dp:param name="dpconfig:LDAPserverAddress">
		<tab-override>basic</tab-override>
		<display>LDAP server Address</display>
		<required-when>
			<condition evaluation="logical-true" />
		</required-when>
	</dp:param>

	<xsl:param name="dpconfig:LDAPportNumber" select="''"/>
	<dp:param name="dpconfig:LDAPportNumber">
		<tab-override>basic</tab-override>
		<display>LDAP server port</display>
		<required-when>
			<condition evaluation="logical-true" />
		</required-when>
	</dp:param>

	<xsl:param name="dpconfig:bindUser_DN" select="''"/>
	<dp:param name="dpconfig:bindUser_DN">
		<tab-override>basic</tab-override>
		<display>bindUser DN</display>
		<required-when>
			<condition evaluation="logical-true" />
		</required-when>
	</dp:param>

	<xsl:param name="dpconfig:bindUserPassword" select="''"/>
	<dp:param name="dpconfig:bindUserPassword">
		<tab-override>basic</tab-override>
		<display>Bind User Password</display>
		<required-when>
			<condition evaluation="logical-true" />
		</required-when>
	</dp:param>

	<xsl:param name="dpconfig:Full_targetDN" select="''"/>
	<dp:param name="dpconfig:Full_targetDN">
		<tab-override>basic</tab-override>
		<display>Full target DN</display>
		<required-when>
			<condition evaluation="logical-true" />
		</required-when>
	</dp:param>
	
	<xsl:param name="dpconfig:userDNtoTest" select="''"/>
	<dp:param name="dpconfig:userDNtoTest">
		<tab-override>basic</tab-override>
		<display>user DN to test</display>
		<required-when>
			<condition evaluation="logical-true" />
		</required-when>
	</dp:param>


	<xsl:template match="/">
		<!--
		<xsl:variable name="LDAPserverAddress" select="string('hq.il.SHTUT')"/>
		<xsl:variable name="LDAPportNumber" select="string('SOMEPORT')"/>
		<xsl:variable name="bindUser_DN" select="string('CN=SomeUserCN,OU=Application Account,OU=Special Users,DC=hq,DC=il,DC=SHTUT')"/>
		<xsl:variable name="bindUserPassword" select="string('F%Vg6b')"/>
		<xsl:variable name="Full_targetDN" select="string('DC=hq,DC=il,DC=SHTUT')"/>
		<xsl:variable name="AttributeName" select="string('memberOf')"/>
-->
		<xsl:variable name="LDAPserverAddress" select="$dpconfig:LDAPserverAddress"/>
		<xsl:variable name="LDAPportNumber" select="$dpconfig:LDAPportNumber"/>
		<xsl:variable name="bindUser_DN" select="$dpconfig:bindUser_DN"/>
		<xsl:variable name="bindUserPassword" select="$dpconfig:bindUserPassword"/>
		<xsl:variable name="Full_targetDN" select="$dpconfig:Full_targetDN"/>
		<xsl:variable name="AttributeName" select="string('memberOf')"/>
		<xsl:variable name="CN" select="$dpconfig:userDNtoTest"/>
		<xsl:variable name="Filter">
			<xsl:value-of select="concat('cn=',$CN)"/>
		</xsl:variable>
		<xsl:variable name="Scope" select="string('sub')"/>
		<xsl:variable name="SSLProxyProfile" select="''"/>
		<xsl:variable name="LdapLBGroup" select="''"/>
		<xsl:variable name="ldapVersion" select="'v3'"/>

		<xsl:message dp:priority="info">
			<xsl:text>Filter: </xsl:text>
			<xsl:copy-of select="$Filter" />
		</xsl:message>	

		<xsl:variable name="search-results" select="dp:ldap-search($LDAPserverAddress, 
																	$LDAPportNumber, 
																	$bindUser_DN, 
																	$bindUserPassword, 
																	$Full_targetDN, 
																	$AttributeName,
																	$Filter, 
																	$Scope ,
																	$SSLProxyProfile , 
																	$LdapLBGroup,
																	$ldapVersion)"/>

		<xsl:message dp:priority="info">
			<xsl:text>LDAP 'Member-Of' Search results: </xsl:text>
			<xsl:copy-of select="$search-results" />
		</xsl:message>

	</xsl:template>


</xsl:stylesheet>
