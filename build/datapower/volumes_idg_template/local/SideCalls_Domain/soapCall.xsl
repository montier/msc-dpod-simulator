<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:env="http://www.w3.org/2003/05/soap-envelope"
    xmlns:dp="http://www.datapower.com/extensions"
    xmlns:dpconfig="http://www.datapower.com/param/config"
    xmlns:date="http://exslt.org/dates-and-times"
    extension-element-prefixes="dp date"
    exclude-result-prefixes="dp dpconfig date env">


    <!-- URL of log server the message is sent to -->
    <xsl:param name="dpconfig:URL" select="''"/>
    <dp:param name="dpconfig:URL" type="dmURL" required="true" xmlns="">
        <display>WebService URL</display>
        <description>The URL to which to send the soapRequest</description>
		<tab-override>basic</tab-override>
		<default/>
		<required-when>
			<condition evaluation="logical-true" />
		</required-when>
    </dp:param>
    
    
    <xsl:template match="/">
        <!-- make soap call to remote server -->  
		<xsl:variable name="log-message" select="."/>
        <xsl:variable name="reply" select="dp:soap-call($dpconfig:URL, $log-message)"/>
		
		<xsl:message dp:priority="info">
			<xsl:text>SOAP Reply: </xsl:text>
			<xsl:copy-of select="$reply"/>
		</xsl:message>
    </xsl:template>

</xsl:stylesheet>
