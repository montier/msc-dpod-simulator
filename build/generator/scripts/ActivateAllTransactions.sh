#!/bin/bash

#################################################################################################################
##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
#################################################################################################################

# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace
set -o pipefail

###########################
# Variables and Constants #
###########################
THIS_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
THIS_SCRIPT_DIR_NAME=$(basename "${THIS_SCRIPT_DIR}")
THIS_SCRIPT_FILE="$(basename -- "${BASH_SOURCE[0]}")"
THIS_SCRIPT_FILE="${THIS_SCRIPT_FILE%%.*}"
COMMON_UTILS_SOURCE="${THIS_SCRIPT_DIR}/commonUtils.sh"

source ${COMMON_UTILS_SOURCE}

NOW=$(current_date_time)

SCRIPTS_DIR="/MonTier/scripts"

LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="ActivateAllTransactions_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

JAVA_PATH="/MonTier/tools/java/bin/java"
MODIFY_TIME_ZONE_DIR="${THIS_SCRIPT_DIR}/ModifyTimeZone"
SUBSCRIBE_TO_API_DIR="${THIS_SCRIPT_DIR}/SubscribeToAPI"
ACTIVATE_PROBES_DIR="${THIS_SCRIPT_DIR}/ActivateProbes"
ABOUT_TO_EXPIRE_CERT_DIR="${THIS_SCRIPT_DIR}/CreateAboutToExpireCert"
ACTIVATE_B2B_DIR="${THIS_SCRIPT_DIR}/ActivateB2B"
API_PROBE_CAPTURE_DIR="${THIS_SCRIPT_DIR}/ApiProbeCapture"

RUN_IDG_TRANSACTIONS_DIR="${THIS_SCRIPT_DIR}/RunIdgTransactions"
SUBSCRIBE_TO_WSM_DIR="${THIS_SCRIPT_DIR}/SubscribeToWSM"
MODIFY_OBJECT_CONFIG_DIR="${THIS_SCRIPT_DIR}/ModifyObjectConfig"
RESTART_DOMAINS_DIR="${THIS_SCRIPT_DIR}/RestartDomains"
RUN_APIC_SCENARIOS_DIR="${THIS_SCRIPT_DIR}/RunApicScenarios"
GENERATE_SYSTEM_ERRORS_DIR="${THIS_SCRIPT_DIR}/GenerateSystemErrors"

# Catalog and api settings for probe capture
BANK_CATALOG_NAME="bankcatalog"
BALANCE_CONVERT_API_NAME="balance-convert"
ACOUNTS_BALANCE_API_NAME="accounts-balance"

DAYLIGHT_START_MONTH=""
DAYLIGHT_STOP_MONTH=""

IDG_SERVICE_NAME=""
IDG_NAME=""
SOMA_PORT=""
SOMA_USER=""
SOMA_PASS=""
DPOD_ADDRESS=""
DPOD_USER=""
DPOD_PASS=""
ES_ENCRYPTED_PASSWORD=""
DPOD_SSH_PASS=""

#############
# Functions #
#############
function main() {
	echo_log_file_path

	parse_parameters "$@"

	# One-time invocations
	modify_time_zone
	subscribe_to_api
	activate_probes
	create_about_to_expire_cert
	activate_b2b

	# Continuous invocations
	run_idg_transactions &
	run_early_completed_trans &
	subscribe_to_wsm &
	modify_object_config &
	restart_domains &
	run_apic_scenarios &
	generate_system_errors &
	activate_all_probe_captures &
}

function parse_parameters() {
	while [[ $# -gt 0 ]]; do
		case "$1" in
		-h | --help)
			show_help
			exit 0
			;;
		-i | --idg-service)
			IDG_SERVICE_NAME=$2
			if [[ -z ${IDG_SERVICE_NAME} ]]; then
				echo_error "Invalid parameters. IDG service name is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-n | --idg-name)
			IDG_NAME=$2
			if [[ -z ${IDG_NAME} ]]; then
				echo_error "Invalid parameters. DP name is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-a | --idg-address)
			IDG_ADDRESS=$2
			if [[ -z ${IDG_ADDRESS} ]]; then
				echo_error "Invalid parameters. DP address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-p | --soma-port)
			SOMA_PORT=$2
			if [[ -z ${SOMA_PORT} ]]; then
				echo_error "Invalid parameters. SOMA port is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-u | --soma-user)
			SOMA_USER=$2
			if [[ -z ${SOMA_USER} ]]; then
				echo_error "Invalid parameters. SOMA user is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-s | --soma-pass)
			SOMA_PASS=$2
			if [[ -z ${SOMA_PASS} ]]; then
				echo_error "Invalid parameters. SOMA password is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-d | --dpod-address)
			DPOD_ADDRESS=$2
			if [[ -z ${DPOD_ADDRESS} ]]; then
				echo_error "Invalid parameters. DPOD address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-r | --dpod-user)
			DPOD_USER=$2
			if [[ -z ${DPOD_USER} ]]; then
				echo_error "Invalid parameters. DPOD user is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-o | --dpod-pass)
			DPOD_PASS=$2
			if [[ -z ${DPOD_PASS} ]]; then
				echo_error "Invalid parameters. DPOD password is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-e | --es-password)
			ES_ENCRYPTED_PASSWORD=$2
			if [[ -z ${ES_ENCRYPTED_PASSWORD} ]]; then
				echo_error "Invalid parameters. Elasticsearch encripted password is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-c | --dpod-ssh-pass)
			DPOD_SSH_PASS=$2
			if [[ -z ${DPOD_SSH_PASS} ]]; then
				echo_error "Invalid parameters. Dpod container ssh password is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo_error "Invalid parameter : $1 ! "
			show_help
			exit 1
			;;
		esac
	done

	if [[ -z ${IDG_SERVICE_NAME} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--idg-service' is missing "
		exit 2
	fi

	if [[ -z ${IDG_NAME} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--idg-name' is missing "
		exit 2
	fi

	if [[ -z ${IDG_ADDRESS} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--idg-address' is missing "
		exit 2
	fi

	if [[ -z ${SOMA_PORT} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--soma-port' is missing "
		exit 2
	fi

	if [[ -z ${SOMA_USER} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--soma-user' is missing "
		exit 2
	fi

	if [[ -z ${SOMA_PASS} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--soma-pass' is missing "
		exit 2
	fi

	if [[ -z ${DPOD_USER} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--dpod-user' is missing "
		exit 2
	fi

	if [[ -z ${DPOD_PASS} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--dpod-pass' is missing "
		exit 2
	fi

	if [[ -z ${DPOD_ADDRESS} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--dpod-address' is missing "
		exit 2
	fi

	if [[ -z ${ES_ENCRYPTED_PASSWORD} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--es-password' is missing "
		exit 2
	fi

	if [[ -z ${DPOD_SSH_PASS} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--dpod-ssh-pass' is missing "
		exit 2
	fi
}

function show_help() {
	echo_info " "
	echo_info $"Usage: $0 -i|--idg-service -n|-idg-name -a|--idg-address -p|--soma-port -u|--soma-user -s|--soma-pass -d|--dpod-address -r|--dpod-user -o|--dpod-pass -e|--es-password -c|--dpod-ssh-pass"
	echo_info "	Or :  $0 -h|--help"
	echo_info " "
	echo_info "-i|--idg-service 		: Name of the IDG service"
	echo_info "-n|--idg-name    		: Name of the IDG"
	echo_info "-a|--idg-address			: Address of the IDG"
	echo_info "-p|--soma-port  			: SOMA port"
	echo_info "-u|--soma-user  			: SOMA user"
	echo_info "-s|--soma-pass  			: SOMA password"
	echo_info "-d|--dpod-address		: Address of the DPOD"
	echo_info "-r|--dpod-user  			: DPOD REST user"
	echo_info "-o|--dpod-pass  			: DPOD REST password"
	echo_info "-e|--es-password			: Elastic search encrypted password"
	echo_info "-c|--dpod-ssh-pass		: Dpod container ssh password"
	echo_info " "
	echo_info "-h|--help       			: Display help   "
	echo_info " "
}

function activate_idg_soma_req() {
	[[ "$#" -ne 2 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1

	local REQUEST_FILE=$1
	local CURL_RESPONSE_FILE=$2

	curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${REQUEST_FILE}" --insecure -w "\n" https://${IDG_ADDRESS}:${SOMA_PORT}/service/mgmt/current/ >${CURL_RESPONSE_FILE}
}

function modify_time_zone() {
	local GENERATED_FILES_DIR="${MODIFY_TIME_ZONE_DIR}/GeneratedFiles"
	mkdir -p ${GENERATED_FILES_DIR}

	local TEMPLATE_FILE="${MODIFY_TIME_ZONE_DIR}/Templates/ModifyTimeZone_Request.xml"
	local DP_TIMEZONES_OFFSET_LIST_FILE="${MODIFY_TIME_ZONE_DIR}/DataPowerTimezoneOffsets.txt"
	local REQUEST_FILE="${GENERATED_FILES_DIR}/${IDG_ADDRESS}_Request.xml"
	local CURL_RESPONSE_FILE="${GENERATED_FILES_DIR}/${IDG_ADDRESS}_Response.xml"
	local SAVE_DOMAIN_CONFIG_REQUEST="${MODIFY_TIME_ZONE_DIR}/SaveConfig_default_domain_Request.xml"
	local SAVE_DOMAIN_CONFIG_RESPONSE="${GENERATED_FILES_DIR}/SaveConfig_${IDG_ADDRESS}_default_domain_Response.xml"

	echo_info "${FUNCNAME}: Modifying device ${IDG_SERVICE_NAME} timezone"
	echo_info "${FUNCNAME}: Creating SOMA request"

	local DIRECTION_SIGN=$(date +"%z" | cut -c 1)
	local DIRECTION=""
	if [[ ${DIRECTION_SIGN} = "+" ]]; then
		DIRECTION="East"
	else
		DIRECTION="West"
	fi

	local HOURS_OFFSET=$(date +"%z" | cut -c 2-3)
	local MINUTES_OFFSET=$(date +"%z" | cut -c 4-5)
	local CURRENT_MONTH=$(date -d "$D" '+%m')

	set_daylight_months ${CURRENT_MONTH}

	cp ${TEMPLATE_FILE} ${REQUEST_FILE}
	sed -i "s/__DIRECTION__/${DIRECTION}/g" ${REQUEST_FILE}
	sed -i "s/__HOURS_OFFSET__/${HOURS_OFFSET}/g" ${REQUEST_FILE}
	sed -i "s/__MINUTES_OFFSET__/${MINUTES_OFFSET}/g" ${REQUEST_FILE}
	sed -i "s/__DAYLIGHT_START_MONTH__/${DAYLIGHT_START_MONTH}/g" ${REQUEST_FILE}
	sed -i "s/__DAYLIGHT_STOP_MONTH__/${DAYLIGHT_STOP_MONTH}/g" ${REQUEST_FILE}

	echo_info "${FUNCNAME}: Modifying Timezone configuration of device ${IDG_ADDRESS}"
	activate_idg_soma_req ${REQUEST_FILE} ${CURL_RESPONSE_FILE}

	echo_info "${FUNCNAME}: Saving device ${IDG_ADDRESS} Config"
	activate_idg_soma_req ${SAVE_DOMAIN_CONFIG_REQUEST} ${SAVE_DOMAIN_CONFIG_RESPONSE}
}

function set_daylight_months() {
	[[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1

	local CURRENT_MONTH=$1

	case "$CURRENT_MONTH" in
	01)
		DAYLIGHT_START_MONTH="February"
		DAYLIGHT_STOP_MONTH="March"
		;;
	02)
		DAYLIGHT_START_MONTH="March"
		DAYLIGHT_STOP_MONTH="April"
		;;
	03)
		DAYLIGHT_START_MONTH="April"
		DAYLIGHT_STOP_MONTH="May"
		;;
	04)
		DAYLIGHT_START_MONTH="May"
		DAYLIGHT_STOP_MONTH="June"
		;;
	05)
		DAYLIGHT_START_MONTH="June"
		DAYLIGHT_STOP_MONTH="July"
		;;
	06)
		DAYLIGHT_START_MONTH="July"
		DAYLIGHT_STOP_MONTH="August"
		;;
	07)
		DAYLIGHT_START_MONTH="August"
		DAYLIGHT_STOP_MONTH="September"
		;;
	08)
		DAYLIGHT_START_MONTH="September"
		DAYLIGHT_STOP_MONTH="October"
		;;
	09)
		DAYLIGHT_START_MONTH="October"
		DAYLIGHT_STOP_MONTH="November"
		;;
	10)
		DAYLIGHT_START_MONTH="November"
		DAYLIGHT_STOP_MONTH="December"
		;;
	11)
		DAYLIGHT_START_MONTH="December"
		DAYLIGHT_STOP_MONTH="January"
		;;
	12)
		DAYLIGHT_START_MONTH="January"
		DAYLIGHT_STOP_MONTH="February"
		;;
	*)
		info "Failed to locate next month for : $CURRENT_MONTH ! "
		echo "$(current_date_time): ERROR: Failed to locate next month for : $CURRENT_MONTH ! "
		exit 1
		;;
	esac
}

function subscribe_to_api() {
	local GENERATED_FILES_DIR="${SUBSCRIBE_TO_API_DIR}/GeneratedFiles"
	mkdir -p ${GENERATED_FILES_DIR}

	local CREATE_API_APPLICATION_TYPE_REQUEST="${SUBSCRIBE_TO_API_DIR}/SOMA_AddAPIApplicationType.xml"
	local UPDATE_API_COLLECTION_REQUEST="${SUBSCRIBE_TO_API_DIR}/SOMA_SetApplicationTypeForAPICollection.xml"
	local CREATE_API_APPLICATION_REQUEST="${SUBSCRIBE_TO_API_DIR}/SOMA_AddAPIApplication.xml"
	local CREATE_API_CLIENT_REQUEST="${SUBSCRIBE_TO_API_DIR}/SOMA_AddAPIClient.xml"
	local UPDATE_SUBSCIPTION_REQUEST="${SUBSCRIBE_TO_API_DIR}/SOMA_UpdateAPISubscription.xml"
	local SAVE_CONFIG_REQUEST="${SUBSCRIBE_TO_API_DIR}/SaveConfig_Request.xml"

	local CREATE_API_APPLICATION_TYPE_REPLY="${GENERATED_FILES_DIR}/SOMA_AddAPIApplicationType.xml"
	local UPDATE_API_COLLECTION_REPLY="${GENERATED_FILES_DIR}/SOMA_SetApplicationTypeForAPICollection.xml"
	local CREATE_API_APPLICATION_REPLY="${GENERATED_FILES_DIR}/SOMA_AddAPIApplication.xml"
	local CREATE_API_CLIENT_REPLY="${GENERATED_FILES_DIR}/SOMA_AddAPIClient.xml"
	local UPDATE_SUBSCIPTION_REPLY="${GENERATED_FILES_DIR}/SOMA_UpdateAPISubscription.xml"
	local SAVE_CONFIG_REPLY="${GENERATED_FILES_DIR}/SaveConfig_Reply.xml"

	echo_info "${FUNCNAME}: Activating API subscriptions on ${IDG_ADDRESS}"

	echo_info "${FUNCNAME}: Creating API application type"
	activate_idg_soma_req ${CREATE_API_APPLICATION_TYPE_REQUEST} ${CREATE_API_APPLICATION_TYPE_REPLY}

	echo_info "${FUNCNAME}: Updating API application type for collections"
	activate_idg_soma_req ${UPDATE_API_COLLECTION_REQUEST} ${UPDATE_API_COLLECTION_REPLY}

	echo_info "${FUNCNAME}: Creating API applications"
	activate_idg_soma_req ${CREATE_API_APPLICATION_REQUEST} ${CREATE_API_APPLICATION_REPLY}

	echo_info "${FUNCNAME}: Creating new clients"
	activate_idg_soma_req ${CREATE_API_CLIENT_REQUEST} ${CREATE_API_CLIENT_REPLY}

	echo_info "${FUNCNAME}: Updating api collection with the new subscription"
	activate_idg_soma_req ${UPDATE_SUBSCIPTION_REQUEST} ${UPDATE_SUBSCIPTION_REPLY}

	echo_info "${FUNCNAME}: Saving config for domain API for idg ${IDG_ADDRESS}"
	activate_idg_soma_req ${SAVE_CONFIG_REQUEST} ${SAVE_CONFIG_REPLY}
}

function activate_probes() {
	local SERVICES_LIST="${ACTIVATE_PROBES_DIR}/ServicesList.txt"
	echo_info "${FUNCNAME}: Activating Probes"

	while read p; do
		local GENERATED_FILES_DIR="${ACTIVATE_PROBES_DIR}/GeneratedFiles"
		mkdir -p ${GENERATED_FILES_DIR}

		local WSP_TEMPLATE_FILE="${ACTIVATE_PROBES_DIR}/Template_WSP_ActivateProbe_SomaRequest.xml"
		local MPGW_TEMPLATE_FILE="${ACTIVATE_PROBES_DIR}/Template_MPGW_ActivateProbe_SomaRequest.xml"

		local SERVICE_TYPE=$(echo ${p} | awk -F: '{print $1}')
		local DOMAIN_NAME=$(echo ${p} | awk -F: '{print $2}')
		local SERVICE_NAME=$(echo ${p} | awk -F: '{print $3}')

		local REQUEST_FILE="${GENERATED_FILES_DIR}/${SERVICE_NAME}_${DOMAIN_NAME}_Request.xml"
		local CURL_RESPONSE_FILE="${GENERATED_FILES_DIR}/${SERVICE_NAME}_${DOMAIN_NAME}_Response.xml"

		if [[ ${SERVICE_TYPE} == "WSP" ]]; then
			cp ${WSP_TEMPLATE_FILE} ${REQUEST_FILE}
		else
			cp ${MPGW_TEMPLATE_FILE} ${REQUEST_FILE}
		fi

		sed -i "s/__SERVICE_NAME__/${SERVICE_NAME}/g" ${REQUEST_FILE}
		sed -i "s/__DOMAIN_NAME__/${DOMAIN_NAME}/g" ${REQUEST_FILE}

		echo_info "${FUNCNAME}: Activating Probe for service ${SERVICE_NAME} on domain: ${DOMAIN_NAME}"
		activate_idg_soma_req ${REQUEST_FILE} ${CURL_RESPONSE_FILE}
	done <${SERVICES_LIST}
}

function create_about_to_expire_cert() {
	local TEMPLATES_DIR="${ABOUT_TO_EXPIRE_CERT_DIR}/Templates"
	local GENERATED_FILES_DIR="${ABOUT_TO_EXPIRE_CERT_DIR}/GeneratedFiles"
	mkdir -p ${GENERATED_FILES_DIR}

	local UPLOAD_FILE_TEMPLATE="${TEMPLATES_DIR}/Template_UploadFile_SomaRequest.xml"
	local UPLOAD_FILE_SOMA_REQUEST="${GENERATED_FILES_DIR}/UploadFile_SomaRequest.xml"

	local CONFIGURE_MONITOR_SOMA_REQUEST="${ABOUT_TO_EXPIRE_CERT_DIR}/ConfigureCertMonitor_SomaRequest.xml"
	local UPDATE_CERT_OBJECT_SOMA_REQUEST="${ABOUT_TO_EXPIRE_CERT_DIR}/UpdateCertObject_SomaRequest.xml"
	local SAVE_DOMAIN_CONFIG_REQUEST="${ABOUT_TO_EXPIRE_CERT_DIR}/SaveConfig_default_SomaRequest.xml"
	local SAVE_DEF_DOMAIN_CONFIG_REQUEST="${ABOUT_TO_EXPIRE_CERT_DIR}/SaveConfig_HotelsA_SomaRequest.xml"

	local UPLOAD_FILE_SOMA_RESPONSE="${GENERATED_FILES_DIR}/UploadFile_SomaResponse.xml"
	local CONFIGURE_MONITOR_SOMA_RESPONSE="${GENERATED_FILES_DIR}/ConfigureCertMontitor_SomaResponse.xml"
	local UPDATE_CERT_OBJECT_SOMA_RESPONSE="${GENERATED_FILES_DIR}/UpdateCertObject_SomaResponse.xml"
	local SAVE_DOMAIN_CONFIG_RESPONSE="${GENERATED_FILES_DIR}/SaveConfig_default_SomaResponse.xml"
	local SAVE_DEF_DOMAIN_CONFIG_RESPONSE="${GENERATED_FILES_DIR}/SaveConfig_HotelsA_SomaResponse.xml"
	local CERT_FILE="${GENERATED_FILES_DIR}/HotelsA.pem"
	local BASE64_CERT="${GENERATED_FILES_DIR}/base64Cert"

	DAYS_TILL_EXPIRY=50
	echo_info "${FUNCNAME}: Creating Certificates that are about to expire"

	# Create cert, using openSSL
	echo_info "${FUNCNAME}: Creating certificate, that will expire in ${DAYS_TILL_EXPIRY} days"
	openssl req -x509 -newkey rsa:4096 -keyout ${GENERATED_FILES_DIR}/key.pem -out ${CERT_FILE} -days ${DAYS_TILL_EXPIRY} -subj "/CN=HotelsA_Domain" -nodes

	echo_info "${FUNCNAME}: Encoding Cert in base64"
	cat ${CERT_FILE} | base64 >${BASE64_CERT}

	echo_info "${FUNCNAME}: Creating SOMA Request"
	cp ${UPLOAD_FILE_TEMPLATE} ${UPLOAD_FILE_SOMA_REQUEST}
	sed -i "/__BASE64__/r ${BASE64_CERT}" ${UPLOAD_FILE_SOMA_REQUEST}
	sed -i "/__BASE64__/d" ${UPLOAD_FILE_SOMA_REQUEST}

	echo_info "${FUNCNAME}: Uploading certificate to idg"
	activate_idg_soma_req ${UPLOAD_FILE_SOMA_REQUEST} ${UPLOAD_FILE_SOMA_RESPONSE}

	echo_info "${FUNCNAME}: Updating CryptoCert object to point to new certificate"
	activate_idg_soma_req ${UPDATE_CERT_OBJECT_SOMA_REQUEST} ${UPDATE_CERT_OBJECT_SOMA_RESPONSE}

	# TODO: Use the DPOD REST API for configuring the cert monitor once it's available
	echo_info "${FUNCNAME}: Reconfiguring CertMonitor object to force resending the expired certs log messages"
	activate_idg_soma_req ${CONFIGURE_MONITOR_SOMA_REQUEST} ${CONFIGURE_MONITOR_SOMA_RESPONSE}

	echo_info "${FUNCNAME}: Saving Domain Config"
	activate_idg_soma_req ${SAVE_DOMAIN_CONFIG_REQUEST} ${SAVE_DOMAIN_CONFIG_RESPONSE}

	echo_info "${FUNCNAME}: Saving Default Domain Config"
	activate_idg_soma_req ${SAVE_DEF_DOMAIN_CONFIG_REQUEST} ${SAVE_DEF_DOMAIN_CONFIG_RESPONSE}
}

function activate_b2b() {
	local GENERATED_FILES_DIR="${ACTIVATE_B2B_DIR}/GeneratedFiles"
	mkdir -p ${GENERATED_FILES_DIR}

	local ENABLE_RAID_REQUEST_FILE="${ACTIVATE_B2B_DIR}/EnableRaidVolube_Request.xml"
	local ENABLE_RAID_RESPONSE_FILE="${GENERATED_FILES_DIR}/EnableRaidVolube_Response.xml"
	local ENABLE_B2B_REQUEST_FILE="${ACTIVATE_B2B_DIR}/EnableB2BPersistance_Request.xml"
	local ENABLE_B2B_RESPONSE_FILE="${GENERATED_FILES_DIR}/EnableB2BPersistance_Response.xml"
	local SAVE_CONFIG_REQUEST_FILE="${ACTIVATE_B2B_DIR}/SaveConfig_default_Request.xml"
	local SAVE_CONFIG_RESPONSE_FILE="${GENERATED_FILES_DIR}/SaveConfig_default_Response.xml"

	echo_info "${FUNCNAME}: Enabling B2B"

	echo_info "${FUNCNAME}: Enabling RAID Volume"
	activate_idg_soma_req ${ENABLE_RAID_REQUEST_FILE} ${ENABLE_RAID_RESPONSE_FILE}

	echo_info "${FUNCNAME}: Enabling B2B Persistance"
	activate_idg_soma_req ${ENABLE_B2B_REQUEST_FILE} ${ENABLE_B2B_RESPONSE_FILE}

	echo_info "${FUNCNAME}: Saving Configuration"
	activate_idg_soma_req ${SAVE_CONFIG_REQUEST_FILE} ${SAVE_CONFIG_RESPONSE_FILE}
}

function run_idg_transactions() {
	echo_info "${FUNCNAME}: Activating all IDG transactions on DP ${IDG_SERVICE_NAME} address: ${IDG_ADDRESS}"
	${RUN_IDG_TRANSACTIONS_DIR}/RunIdgTransactions.sh -a ${IDG_ADDRESS} -i ${IDG_SERVICE_NAME} >"${LOG_DIR}/RunIdgTransactions_${IDG_SERVICE_NAME}.log" 2>&1 &
}

function subscribe_to_wsm() {
	echo_info "${FUNCNAME}: Activating WSM subscriptions"
	local GENERATED_FILES_DIR="${SUBSCRIBE_TO_WSM_DIR}/GeneratedFiles"
	mkdir -p ${GENERATED_FILES_DIR}

	local SLEEP_DURATION=610
	local RETRY_SLEEP_DURATION=5
	local CURL_RETRY_LIMIT=3
	local REST_URI="/op/api/v1/wsmSubscriptions"
	local TEMPLATE_PATH="${SUBSCRIBE_TO_WSM_DIR}/Templates/subscribeToWsm.json"
	local REQUEST_FILE="${GENERATED_FILES_DIR}/${IDG_NAME}_Request.json"
	local CURL_RESPONSE_FILE="${GENERATED_FILES_DIR}/${IDG_NAME}_Response.json"

	local REST_URL="https://${DPOD_ADDRESS}${REST_URI}"

	cp ${TEMPLATE_PATH} ${REQUEST_FILE}

	sed -i "s/__IDG_NAME__/${IDG_NAME}/g" ${REQUEST_FILE}

	while true; do
		local CURL_SUCCESS_INDICATION=0
		local CURL_RETRIES=0

		while [ ${CURL_SUCCESS_INDICATION} -eq 0 ] && [ ${CURL_RETRIES} -lt ${CURL_RETRY_LIMIT} ]; do
			echo_info "${FUNCNAME}: Using DPOD REST interface to create WSM Subscriptions for device: ${IDG_NAME}"
			curl --user ${DPOD_USER}:${DPOD_PASS} -X POST --insecure -w "\n" -d "@${REQUEST_FILE}" ${REST_URL} >${CURL_RESPONSE_FILE}

			CURL_SUCCESS_INDICATION=$(grep "\"resultCode\":\"SUCCESS\"" ${CURL_RESPONSE_FILE} | wc -l) || CURL_SUCCESS_INDICATION=0
			if [[ ${CURL_SUCCESS_INDICATION} -eq 0 ]]; then
				echo_info "${FUNCNAME}: DPOD REST response did not contain a success indication."
				CURL_RETRIES=$((CURL_RETRIES + 1))
			else
				CURL_SUCCESS_INDICATION=1
			fi
		done

		if [[ ${CURL_SUCCESS_INDICATION} -eq 0 ]]; then
			echo "$(current_date_time): ERROR: Failed to subscribe to WSM."
		fi

		echo_info "${FUNCNAME}: Sleeping for ${SLEEP_DURATION} seconds"
		sleep ${SLEEP_DURATION}
	done
}

function modify_object_config() {
	local GENERATED_FILES_DIR="${MODIFY_OBJECT_CONFIG_DIR}/GeneratedFiles"
	mkdir -p ${GENERATED_FILES_DIR}

	local SERVICES_LIST="${MODIFY_OBJECT_CONFIG_DIR}/ServicesList.txt"
	local TIME_TO_SLEEP_BEFORE_CHANGE=1200
	local TIME_TO_SLEEP_AFTER_CHANGE=3000

	echo_info "${FUNCNAME}: Sleeping ${TIME_TO_SLEEP_BEFORE_CHANGE} seconds before first change"
	sleep ${TIME_TO_SLEEP_BEFORE_CHANGE}

	while true; do
		while read p; do
			# Apply the change
			local DOMAIN_NAME=$(echo ${p} | awk -F: '{print $1}')
			local SERVICE_NAME=$(echo ${p} | awk -F: '{print $2}')

			local REQUEST_FILE="${MODIFY_OBJECT_CONFIG_DIR}/${SERVICE_NAME}_${DOMAIN_NAME}_Request.xml"
			local CURL_RESPONSE_FILE="${GENERATED_FILES_DIR}/${SERVICE_NAME}_${DOMAIN_NAME}_Response.xml"
			local SAVE_DOMAIN_CONFIG_REQUEST="${MODIFY_OBJECT_CONFIG_DIR}/SaveConfig_${DOMAIN_NAME}_Request.xml"
			local SAVE_DOMAIN_CONFIG_RESPONSE="${GENERATED_FILES_DIR}/SaveConfig_${DOMAIN_NAME}_Response.xml"

			echo_info "${FUNCNAME}: Modifying configuration of service ${SERVICE_NAME} on domain: ${DOMAIN_NAME}"
			activate_idg_soma_req ${REQUEST_FILE} ${CURL_RESPONSE_FILE}

			echo_info "${FUNCNAME}: Saving Domain ${DOMAIN_NAME} Config"
			activate_idg_soma_req ${SAVE_DOMAIN_CONFIG_REQUEST} ${SAVE_DOMAIN_CONFIG_RESPONSE}

			# Revert the change
			echo_info "${FUNCNAME}: Sleeping ${TIME_TO_SLEEP_AFTER_CHANGE} seconds before reverting change"
			sleep ${TIME_TO_SLEEP_AFTER_CHANGE}

			REQUEST_FILE="${MODIFY_OBJECT_CONFIG_DIR}/Revert_${SERVICE_NAME}_${DOMAIN_NAME}_Request.xml"
			CURL_RESPONSE_FILE="${GENERATED_FILES_DIR}/Revert_${SERVICE_NAME}_${DOMAIN_NAME}_Response.xml"
			SAVE_DOMAIN_CONFIG_RESPONSE="${GENERATED_FILES_DIR}/SaveConfig_Revert_${DOMAIN_NAME}_Response.xml"

			echo_info "${FUNCNAME}: Reverting configuration of service ${SERVICE_NAME} on domain: ${DOMAIN_NAME}"
			activate_idg_soma_req ${REQUEST_FILE} ${CURL_RESPONSE_FILE}

			echo_info "${FUNCNAME}: Saving Domain ${DOMAIN_NAME} reverted config"
			activate_idg_soma_req ${SAVE_DOMAIN_CONFIG_REQUEST} ${SAVE_DOMAIN_CONFIG_RESPONSE}

			echo_info "${FUNCNAME}: Sleeping ${TIME_TO_SLEEP_AFTER_CHANGE} seconds before next change"
			sleep ${TIME_TO_SLEEP_AFTER_CHANGE}

		done <${SERVICES_LIST}
	done
}

function restart_domains() {
	local GENERATED_FILES_DIR="${RESTART_DOMAINS_DIR}/GeneratedFiles"
	mkdir -p ${GENERATED_FILES_DIR}

	local TEMPLATE_FILE="${RESTART_DOMAINS_DIR}/Template_RestartDomain_SomaRequest.xml"
	local DOMAINS_LIST="${RESTART_DOMAINS_DIR}/DomainsList.txt"
	local TIME_TO_SLEEP_BEFORE_RESTART=600
	local TIME_TO_SLEEP_AFTER_RESTART=3600
	local TIME_TO_SLEEP_BETWEEN_DOMAIN=60

	echo_info "${FUNCNAME}: Restarting Domains"
	echo_info "${FUNCNAME}: Sleeping ${TIME_TO_SLEEP_BEFORE_RESTART} seconds before restarting domains"
	sleep ${TIME_TO_SLEEP_BEFORE_RESTART}

	while true; do
		while read p; do
			local DOMAIN_NAME=$(echo ${p} | awk -F: '{print $1}')

			local REQUEST_FILE="${RESTART_DOMAINS_DIR}/${DOMAIN_NAME}_Request.xml"
			local CURL_RESPONSE_FILE="${GENERATED_FILES_DIR}/${DOMAIN_NAME}_Response.xml"

			cp ${TEMPLATE_FILE} ${REQUEST_FILE}
			sed -i "s/__DOMAIN_NAME__/${DOMAIN_NAME}/g" ${REQUEST_FILE}

			echo_info "${FUNCNAME}: Restarting domain: ${DOMAIN_NAME}"
			activate_idg_soma_req ${REQUEST_FILE} ${CURL_RESPONSE_FILE}

			echo_info "${FUNCNAME}: Sleeping ${TIME_TO_SLEEP_BETWEEN_DOMAIN} seconds before next domain restart"
			sleep ${TIME_TO_SLEEP_BETWEEN_DOMAIN}

		done <${DOMAINS_LIST}

		echo_info "${FUNCNAME}: Sleeping ${TIME_TO_SLEEP_AFTER_RESTART} seconds before restarting domains again"
		sleep ${TIME_TO_SLEEP_AFTER_RESTART}
	done
}

function run_apic_scenarios() {
	local MONTIER_CRED_FILE="${RUN_APIC_SCENARIOS_DIR}/appCredentials_montierapp.sh"
	local DIGITAL_BANK_101_CRED_FILE="${RUN_APIC_SCENARIOS_DIR}/appCredentials_digitalbankingapp101.sh"
	local DIGITAL_BANK_102_CRED_FILE="${RUN_APIC_SCENARIOS_DIR}/appCredentials_digitalbankingapp102.sh"

	echo_info "${FUNCNAME}: Activating APIC transactions"
	${RUN_APIC_SCENARIOS_DIR}/RunApicScenarios.sh -a ${IDG_ADDRESS} -m ${MONTIER_CRED_FILE} -b1 ${DIGITAL_BANK_101_CRED_FILE} -b2 ${DIGITAL_BANK_102_CRED_FILE} >"${LOG_DIR}/RunApicScenarios_${IDG_SERVICE_NAME}.log" 2>&1 &
}

function generate_system_errors() {
	local SLEEP_DURATION=1800

	while true; do
		local GENERATED_FILES_DIR="${GENERATE_SYSTEM_ERRORS_DIR}/GeneratedFiles"
		mkdir -p ${GENERATED_FILES_DIR}

		local APP_DATA_LOADER_DST="/MonTier/GenerateSystemErrors"
		local DEFAULT_DOMAIN_TEMPLATE_FILE="${GENERATE_SYSTEM_ERRORS_DIR}/Template_defaultDomain_appDataLoader.properties"
		local DEFAULT_DOMAIN_PROPERTIES_FILE="${GENERATED_FILES_DIR}/defaultDomain_appDataLoader_${IDG_SERVICE_NAME}.properties"
		local DEFAULT_DOMAIN_PROPERTIES_DST_FILE="${APP_DATA_LOADER_DST}/defaultDomain_appDataLoader_${IDG_SERVICE_NAME}.properties"
		local FAILED_LOGINS_FILE="${GENERATE_SYSTEM_ERRORS_DIR}/failedLogins.txt"
		local SYSTEM_ISSUES_FILE="${GENERATE_SYSTEM_ERRORS_DIR}/systemIssues.txt"
		local JAVA_COMMAND="java -jar ${APP_DATA_LOADER_DST}/appDataLoader.jar"

		echo_info "${FUNCNAME}: Creating appDataLoader.properties from default domain template for IDG: ${IDG_SERVICE_NAME}"
		cp ${DEFAULT_DOMAIN_TEMPLATE_FILE} ${DEFAULT_DOMAIN_PROPERTIES_FILE}
		sed -i "s/__DEVICE__/${IDG_NAME}/g" ${DEFAULT_DOMAIN_PROPERTIES_FILE}
		sed -i "s&__ES_PASSWORD__&${ES_ENCRYPTED_PASSWORD}&g" ${DEFAULT_DOMAIN_PROPERTIES_FILE}

		local APP_DATA_LOADER_COMMAND="${JAVA_COMMAND} -p ${DEFAULT_DOMAIN_PROPERTIES_DST_FILE}"

		# Copying files to dpod container
		sshpass -p ${DPOD_SSH_PASS} scp -o StrictHostKeyChecking=no ${DEFAULT_DOMAIN_PROPERTIES_FILE} ${FAILED_LOGINS_FILE} ${SYSTEM_ISSUES_FILE} "root@"${DPOD_ADDRESS}:${APP_DATA_LOADER_DST}/ >>${LOG_FILE} 2>&1

		echo_info "${FUNCNAME}: Running appDataLoader for default domain: ${APP_DATA_LOADER_COMMAND}"
		sshpass -p ${DPOD_SSH_PASS} ssh -o StrictHostKeyChecking=no "root@"${DPOD_ADDRESS} -t "sh -l -c \"${APP_DATA_LOADER_COMMAND}\"" >>${LOG_FILE} 2>&1

		sleep ${SLEEP_DURATION}
	done
}

function activate_all_probe_captures() {
	local GENERATED_FILES_DIR="${API_PROBE_CAPTURE_DIR}/GeneratedFiles"
	mkdir -p ${GENERATED_FILES_DIR}

	local REST_URI="/op/api/v2/apiProbeCaptures/search"
	local REST_URL="https://${DPOD_ADDRESS}${REST_URI}"
	local REQUEST_FILE="${API_PROBE_CAPTURE_DIR}/searchApiProbeCaptures.json"
	local CURL_RESPONSE_FILE="${GENERATED_FILES_DIR}/searchApiProbeCaptures_${IDG_NAME}_Response.json"
	local SLEEP_DURATION=60

	echo_info "Watching if there are active/stopping probe captures..."
	while true; do
		curl --user ${DPOD_USER}:${DPOD_PASS} -X POST --insecure -d "@${REQUEST_FILE}" -w "\n" ${REST_URL} >${CURL_RESPONSE_FILE} 2>>${LOG_FILE}

		# Activating new probe captures if there are currently no active/stopping probe captures
		local PROBE_CAPTURES_COUNT=$(jq -r '.result.totalResultsCount' "${CURL_RESPONSE_FILE}" 2>>${LOG_FILE})
		if [[ ${PROBE_CAPTURES_COUNT} == 0 ]]; then
			activate_api_probe_capture ${BANK_CATALOG_NAME} ${BALANCE_CONVERT_API_NAME}
			activate_api_probe_capture ${BANK_CATALOG_NAME} ${ACOUNTS_BALANCE_API_NAME}
		fi

		sleep ${SLEEP_DURATION}
	done
}

function activate_api_probe_capture() {
	[[ "$#" -ne 2 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1

	local GENERATED_FILES_DIR="${API_PROBE_CAPTURE_DIR}/GeneratedFiles"
	mkdir -p ${GENERATED_FILES_DIR}

	local REST_URI="/op/api/v2/apiProbeCaptures/"
	local REST_URL="https://${DPOD_ADDRESS}${REST_URI}"

	local CATALOG_NAME=$1
	local API_NAME=$2

	local TEMPLATE_PATH="${API_PROBE_CAPTURE_DIR}/Templates/apiProbeCapture.json"
	local REQUEST_FILE="${GENERATED_FILES_DIR}/apiProbeCapture_${API_NAME}_${IDG_NAME}_Request.json"
	local CURL_RESPONSE_FILE="${GENERATED_FILES_DIR}/apiProbeCapture_${IDG_NAME}_Response.json"

	# Create request file
	cp ${TEMPLATE_PATH} ${REQUEST_FILE}
	sed -i "s/__IDG_NAME__/${IDG_NAME}/g" ${REQUEST_FILE}
	sed -i "s/__CATALOG_NAME__/${CATALOG_NAME}/g" ${REQUEST_FILE}
	sed -i "s/__API_NAME__/${API_NAME}/g" ${REQUEST_FILE}

	echo_info "Activating API probe capture..."
	local CURL_RETRY_LIMIT=1200
	local CURL_RETRIES=0
	local CAPTURE_SUCCESS_INDICATION=0

	while [[ ${CAPTURE_SUCCESS_INDICATION} -eq 0 ]] && [[ ${CURL_RETRIES} -lt ${CURL_RETRY_LIMIT} ]]; do
		curl --user ${DPOD_USER}:${DPOD_PASS} -X POST --insecure -d "@${REQUEST_FILE}" -w "\n" ${REST_URL} >${CURL_RESPONSE_FILE} 2>>${LOG_FILE}

		CAPTURE_SUCCESS_INDICATION=$(grep "\"resultMessage\":\"All API probe captures created successfully\"" ${CURL_RESPONSE_FILE} | wc -l) || CAPTURE_SUCCESS_INDICATION=0
		CURL_RETRIES=$((CURL_RETRIES + 1))
	done

	if [[ ${CAPTURE_SUCCESS_INDICATION} -eq 0 ]]; then
		echo_info "Failed to create API probe capture."
		echo_info "Please create API probe capture manually in the Web Console."
	fi
}

#################
# Invoking main #
#################
main "$@"
