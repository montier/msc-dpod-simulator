#!/bin/bash

#################################################################################################################
##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http:/www.perlfoundation.org/artistic_license_2_0
#################################################################################################################

# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace
set -o pipefail

###########################
# Variables and Constants #
###########################
THIS_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
THIS_SCRIPT_DIR_NAME=$(basename "${THIS_SCRIPT_DIR}")
THIS_SCRIPT_FILE="$(basename -- "${BASH_SOURCE[0]}")"
THIS_SCRIPT_FILE="${THIS_SCRIPT_FILE%%.*}"
COMMON_UTILS_SOURCE="${THIS_SCRIPT_DIR}/../commonUtils.sh"

source ${COMMON_UTILS_SOURCE}

NOW=$(current_date_time)

LOG_DIR="${THIS_SCRIPT_DIR}/logs"
LOG_FILE_NAME="runApicScenarios_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

REQUEST_FILES_DIR="${THIS_SCRIPT_DIR}"
CURL_OAUTH_RESPONSE_DIR="${THIS_SCRIPT_DIR}"
CURL_OAUTH_RESPONSE=""

IDG_ADDRESS=""
BANK_101_CREDENTIALS_FILE=""
BANK_102_CREDENTIALS_FILE=""
MONTIER_CREDENTIALS_FILE=""

CONTENT_TYPE_JSON="application/json"
HTTP_METHOD_POST="POST"
HTTP_METHOD_GET="GET"
OAUTH="OAUTH"
NO_OAUTH='NO_OAUTH'

# Oauth parmeters
OAUTH_PROVIDER_IP=""
OAUTH_PORT_HTTPS=9443
MONTIER_OAUTH_TOKEN_URI=""

# Accounts app paramenters
API_PROVIDER_IP=""
API_PORT_HTTPS=9443
MONTIER_CLIENT_ID=""
MONTIER_CLIENT_SECRET=""
WRONG_CLIENT_ID="e9b5ad0e7743c2466ca163d3f7777777"

ACCOUNTS_MISSING_ID_REQUEST_FILE=""
ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE=""
ACCOUNTS_MISSING_MONEY_REQUEST_FILE=""

ACCOUNTS_WITHDRAW_103_API_URI=""
ACCOUNTS_WITHDRAW_101_API_URI=""
ACCOUNTS_BALANCE_105_API_URI=""
ACCOUNTS_WITHDRAW_105_API_URI=""
ACCOUNTS_WITHDRAW_107_API_URI=""

# Bank app paramenters
BANK_101_CLIENT_ID=""
BANK_101_CLIENT_SECRET=""
BANK_102_CLIENT_ID=""
BANK_102_CLIENT_SECRET=""

BANK_OAUTH_TOKEN_URI=""

BALANCE_CONVER_101_API_URI=""
BALANCE_CONVER_102_API_URI=""
CONVERT_USD_REQUEST_FILE=""
CONVERT_EUR_REQUEST_FILE=""

# Oauth scopes
INVALID_SCOPE="InvalidScope"
UNAUTHORIZED_SCOPE="Details"
AUTHORIZED_SCOPE="Operations"

OAUTH_TOKEN=""
EXPIRED_OAUTH_TOKEN="AAIgZTliNWFkMGU3NzQzYzI0NjZjYTE2M2QzZmNmNTgwZjXSslUw45cCBM1QEAgJwhjfsyS2LEq1HCHFpUst30iRA_HNwx9sAF3UVTcKznDhzMqbKQ42oYHrpld5UFiRED75lR1J2HWJfQdi6ZiBwSxEp8eGakkchL8X26G48JmBOBo"
INVALID_OAUTH_TOKEN="7777777yODAxNGUzZjIxMjhkMWRhNWE0N2E4NmQ0YmQyOTgMOv1MbeAItWBtKOW8YIFwc1rZzx5LoMtdiYUZlHPsi61T5n3GGYdxq2FWTeKTbj04BUgO9dICAw9jffKV6hZxjQjNU6BvkY2WlWsSS-uZz_utjwwbkcYbeVVjAyRQxM8"

SLEEP_TIME_CYCLE=30
#############
# Functions #
#############
function main() {
	echo_log_file_path
	parse_parameters "$@"

	set_parameters

	# Executing all apis
	while true; do
		run_early_failed_apis
		run_account_apis
		run_bank_apis

		echo_info "*** Sleep for ${SLEEP_TIME_CYCLE} seconds on IDG ${IDG_ADDRESS} ***"
		sleep ${SLEEP_TIME_CYCLE}
	done
}

function parse_parameters() {
	while [[ -n "$1" ]]; do
		case "$1" in
		-h | --help)
			show_help
			exit 0
			;;
		-b1 | --digital-banking-app-101-credentials-file)
			BANK_101_CREDENTIALS_FILE=$2
			if [[ -z ${BANK_101_CREDENTIALS_FILE} ]]; then
				echo_error "Invalid parameters. Credentials file is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-b2 | --digital-banking-app-102-credentials-file)
			BANK_102_CREDENTIALS_FILE=$2
			if [[ -z ${BANK_102_CREDENTIALS_FILE} ]]; then
				echo_error "Invalid parameters. Credentials file is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-m | --montier-app-credentials-file)
			MONTIER_CREDENTIALS_FILE=$2
			if [[ -z ${MONTIER_CREDENTIALS_FILE} ]]; then
				echo_error "Invalid parameters. Credentials file is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-a | --idg-address)
			IDG_ADDRESS=$2
			if [[ -z ${IDG_ADDRESS} ]]; then
				echo_error "Invalid parameters. DP address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo_error "Invalid parameter : $1 ! "
			show_help
			exit 1
			;;
		esac
	done

	if [[ -z ${IDG_ADDRESS} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '-a | --idg-address' is missing "
		exit 2
	fi

	if [[ -z ${BANK_101_CREDENTIALS_FILE} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '-b1 | --digital-banking-app-101-credentials-file' is missing "
		exit 2
	fi

	if [[ -z ${BANK_102_CREDENTIALS_FILE} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '-b2 | --digital-banking-app-102-credentials-file' is missing "
		exit 2
	fi

	if [[ -z ${MONTIER_CREDENTIALS_FILE} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '-m | --montier-app-credentials-file' is missing "
		exit 2
	fi
}

function show_help() {
	echo_info $"Usage: $0 -a|--idg-address <Gateway IP Address> -f|--credentials-file <Secrets file> "
	echo_info "	Or :  $0 -h|--help"
	echo_info "-a|--idg-address			: Address of the IDG			    "
	echo_info "-b1|--digital-banking-app-101-credentials-file		: Credential files contains client id and secret for Digital Banking v101 app"
	echo_info "-b2|--digital-banking-app-102-credentials-file		: Credential files contains client id and secret for Digital Banking v102 app"
	echo_info "-m|--montier-app-credentials-file					: Credential files contains client id and secret for MonTier app"
	echo_info "-h|--help       			: Display help                      "
	echo_info "For Example: $0 -a 172.77.77.3 -f montier.secret.sh"
}

function set_parameters() {
	OAUTH_PROVIDER_IP=${IDG_ADDRESS}
	API_PROVIDER_IP=${IDG_ADDRESS}

	# Accounts app parameters
	source ${MONTIER_CREDENTIALS_FILE}
	MONTIER_CLIENT_ID=${APP_CLIENT_ID}
	MONTIER_CLIENT_SECRET=${APP_SECRET}
	MONTIER_ORG_NAME=${ORG_NAME}

	MONTIER_OAUTH_TOKEN_URI="/${MONTIER_ORG_NAME}/montiercatalog/montieroauth/oauth2/token"

	ACCOUNTS_MISSING_ID_REQUEST_FILE="${REQUEST_FILES_DIR}/Accounts_withdraw_missingCustomerID.json"
	ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE="${REQUEST_FILES_DIR}/Accounts_successfullWithdraw.json"
	ACCOUNTS_MISSING_MONEY_REQUEST_FILE="${REQUEST_FILES_DIR}/Accounts_withdraw_notEnoughMoneyInAccount.json"

	ACCOUNTS_WITHDRAW_101_API_URI="/${MONTIER_ORG_NAME}/montiercatalog/accounts-101/withdraw"
	ACCOUNTS_WITHDRAW_103_API_URI="/${MONTIER_ORG_NAME}/montiercatalog/accounts-103/withdraw"
	ACCOUNTS_BALANCE_105_API_URI="/${MONTIER_ORG_NAME}/montiercatalog/accounts-105/balance"
	ACCOUNTS_WITHDRAW_105_API_URI="/${MONTIER_ORG_NAME}/montiercatalog/accounts-105/withdraw"
	ACCOUNTS_WITHDRAW_107_API_URI="/${MONTIER_ORG_NAME}/montiercatalog/accounts-107/withdraw"
	WRONG_URL_API_URI="/${MONTIER_ORG_NAME}/montiercatalog/accounts-service/balance?customer_id=7777778"

	# Bank app parametrs
	CONVERT_USD_REQUEST_FILE="${REQUEST_FILES_DIR}/convert-balance_USD.json"
	CONVERT_EUR_REQUEST_FILE="${REQUEST_FILES_DIR}/convert-balance_EUR.json"

	source ${BANK_101_CREDENTIALS_FILE}
	BANK_101_CLIENT_ID=${APP_CLIENT_ID}
	BANK_101_CLIENT_SECRET=${APP_SECRET}

	source ${BANK_102_CREDENTIALS_FILE}
	BANK_102_CLIENT_ID=${APP_CLIENT_ID}
	BANK_102_CLIENT_SECRET=${APP_SECRET}

	BANK_ORG_NAME=${ORG_NAME}

	BANK_OAUTH_TOKEN_URI="/${BANK_ORG_NAME}/bankcatalog/montieroauth/oauth2/token"
	BALANCE_CONVER_101_API_URI="/${BANK_ORG_NAME}/bankcatalog/balance-convert-101/convert"
	BALANCE_CONVER_102_API_URI="/${BANK_ORG_NAME}/bankcatalog/balance-convert-102/convert"

	# Curl respose file - for token
	mkdir -p ${CURL_OAUTH_RESPONSE_DIR}
	CURL_OAUTH_RESPONSE="${CURL_OAUTH_RESPONSE_DIR}/oauthResponse_${IDG_ADDRESS}_${NOW}.json"
}

function run_early_failed_apis() {
	echo_info "========= Running EarlyFailed Scenarios on IDG ${IDG_ADDRESS}========== "

	echo_info "Running wrongURL scenario"
	activate_api ${WRONG_URL_API_URI} ${ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE} ${HTTP_METHOD_GET} ${CONTENT_TYPE_JSON} "NO_OAUTH"

	echo_info "Running invalidScope scenario"
	activate_api ${ACCOUNTS_WITHDRAW_105_API_URI} ${ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${MONTIER_CLIENT_ID} ${MONTIER_CLIENT_SECRET} ${INVALID_SCOPE} ${MONTIER_OAUTH_TOKEN_URI}

	echo_info "Running unauthorizedScope scenario"
	activate_api ${ACCOUNTS_WITHDRAW_105_API_URI} ${ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${MONTIER_CLIENT_ID} ${MONTIER_CLIENT_SECRET} ${UNAUTHORIZED_SCOPE} ${MONTIER_OAUTH_TOKEN_URI}

	echo_info "Running expiredToken scenario"
	activate_api ${ACCOUNTS_WITHDRAW_105_API_URI} ${ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${MONTIER_CLIENT_ID} ${MONTIER_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${MONTIER_OAUTH_TOKEN_URI} ${EXPIRED_OAUTH_TOKEN}

	echo_info "Running invalidToken scenario"
	activate_api ${ACCOUNTS_WITHDRAW_105_API_URI} ${ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${MONTIER_CLIENT_ID} ${MONTIER_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${MONTIER_OAUTH_TOKEN_URI} ${INVALID_OAUTH_TOKEN}

	echo_info "Running noClientID scenario"
	activate_api ${ACCOUNTS_WITHDRAW_105_API_URI} ${ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" "NO_CLIENT" ${MONTIER_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${MONTIER_OAUTH_TOKEN_URI} ${INVALID_OAUTH_TOKEN}

	echo_info "Running noToken_noClientID scenario"
	activate_api ${ACCOUNTS_WITHDRAW_105_API_URI} ${ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" "NO_CLIENT" ${MONTIER_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${MONTIER_OAUTH_TOKEN_URI} "NO_TOKEN"

	echo_info "Running noToken scenario"
	activate_api ${ACCOUNTS_WITHDRAW_105_API_URI} ${ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${MONTIER_CLIENT_ID} ${MONTIER_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${MONTIER_OAUTH_TOKEN_URI} "NO_TOKEN"

	echo_info "Running unauthorizedwrongClientIDScope scenario"
	activate_api ${ACCOUNTS_WITHDRAW_105_API_URI} ${ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${WRONG_CLIENT_ID} ${MONTIER_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${MONTIER_OAUTH_TOKEN_URI} "NO_TOKEN"
}

function run_account_apis() {
	echo_info "========= Running Accounts Applicative Scenarios on IDG ${IDG_ADDRESS}========== "

	echo_info "Running Accounts-101_missingElement scenario"
	activate_api ${ACCOUNTS_WITHDRAW_101_API_URI} ${ACCOUNTS_MISSING_ID_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${MONTIER_CLIENT_ID} ${MONTIER_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${MONTIER_OAUTH_TOKEN_URI}

	echo_info "Running Accounts-103_notEnoughMoneyInAccount scenario"
	activate_api ${ACCOUNTS_WITHDRAW_103_API_URI} ${ACCOUNTS_MISSING_MONEY_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${MONTIER_CLIENT_ID} ${MONTIER_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${MONTIER_OAUTH_TOKEN_URI}

	echo_info "Running Accounts-105_withdrawRequest scenario"
	activate_api ${ACCOUNTS_WITHDRAW_105_API_URI} ${ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${MONTIER_CLIENT_ID} ${MONTIER_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${MONTIER_OAUTH_TOKEN_URI}

	echo_info "Running Accounts-107_withdrawRequest scenario"
	activate_api ${ACCOUNTS_WITHDRAW_107_API_URI} ${ACCOUNTS_SUCCESSFULL_WITHDRAW_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "NO_OAUTH"
}

function run_bank_apis() {
	echo_info "========= Running Bank Applicative Scenarios on IDG ${IDG_ADDRESS}========== "

	echo_info "Running Convert-Balance-101_USD scenario"
	activate_api ${BALANCE_CONVER_101_API_URI} ${CONVERT_USD_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${BANK_101_CLIENT_ID} ${BANK_101_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${BANK_OAUTH_TOKEN_URI}

	echo_info "Running Convert-Balance-101_EUR scenario"
	activate_api ${BALANCE_CONVER_101_API_URI} ${CONVERT_EUR_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${BANK_101_CLIENT_ID} ${BANK_101_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${BANK_OAUTH_TOKEN_URI}

	echo_info "Running Convert-Balance-102_USD scenario"
	activate_api ${BALANCE_CONVER_102_API_URI} ${CONVERT_USD_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${BANK_102_CLIENT_ID} ${BANK_102_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${BANK_OAUTH_TOKEN_URI}

	echo_info "Running Convert-Balance-102_EUR scenario"
	activate_api ${BALANCE_CONVER_102_API_URI} ${CONVERT_EUR_REQUEST_FILE} ${HTTP_METHOD_POST} ${CONTENT_TYPE_JSON} "OAUTH" ${BANK_102_CLIENT_ID} ${BANK_102_CLIENT_SECRET} ${AUTHORIZED_SCOPE} ${BANK_OAUTH_TOKEN_URI}
}

function activate_api {
	[[ "$#" -ne 5 ]] && [[ "$#" -ne 9 ]] && [[ "$#" -ne 10 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
	local API_URI=$1
	local API_REQUEST_FILE=$2
	local HTTP_METHOD=$3
	local CONTENT_TYPE=$4
	local ACTIVATE_OAUTH_TOKEN=$5
	local CLIENT_ID=$6
	local CLIENT_SECRET=$7
	local OAUTH_SCOPE=$8
	local OAUTH_TOKEN_URI=$9
	local APP_OAUTH_TOKEN=${10}
	local OAUTH_HEADER=""
	local CLIENT_HEADER=""

	if [[ ${ACTIVATE_OAUTH_TOKEN} == 'OAUTH' ]]; then
		OAUTH_TOKEN=""
		if [[ -z ${APP_OAUTH_TOKEN} ]]; then
			get_oauth_token ${CLIENT_ID} ${CLIENT_SECRET} ${OAUTH_SCOPE} ${OAUTH_TOKEN_URI}
		else
			OAUTH_TOKEN=${APP_OAUTH_TOKEN}
		fi

		if [[ ${APP_OAUTH_TOKEN} != "NO_TOKEN" ]]; then
			OAUTH_HEADER="Authorization : Bearer ${OAUTH_TOKEN}"
		fi

		if [[ ${CLIENT_ID} != "NO_CLIENT" ]]; then
			CLIENT_HEADER="X-IBM-Client-Id : ${CLIENT_ID}"
		fi
	fi

	curl -s ${HTTP_METHOD} --header "Content-Type: ${CONTENT_TYPE}" --header "${OAUTH_HEADER}" --header "${CLIENT_HEADER}" -d "@${API_REQUEST_FILE}" --insecure -w "\n" "https://${API_PROVIDER_IP}:${API_PORT_HTTPS}${API_URI}" >>${LOG_FILE} 2>&1 &
}

function get_oauth_token() {
	[[ "$#" -ne 4 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
	local CLIENT_ID=$1
	local CLIENT_SECRET=$2
	local SCOPE=$3
	local OAUTH_TOKEN_URI=$4

	# Get the oath token
	curl -s -X POST -d"client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&grant_type=client_credentials&scope=${SCOPE}" --insecure -w "\n" "https://${OAUTH_PROVIDER_IP}:${OAUTH_PORT_HTTPS}${OAUTH_TOKEN_URI}" >${CURL_OAUTH_RESPONSE}

	OAUTH_TOKEN=$(cat ${CURL_OAUTH_RESPONSE} | awk -F'[,]' '{print $2}' | awk -F'["]' '{print $4}')

	rm -f ${CURL_OAUTH_RESPONSE}
}
#################
# Invoking main #
#################
main "$@"
