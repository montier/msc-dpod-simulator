#!/bin/bash

#################################################################################################################
##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http:/www.perlfoundation.org/artistic_license_2_0
#################################################################################################################

# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace
set -o pipefail

###########################
# Variables and Constants #
###########################
THIS_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
THIS_SCRIPT_DIR_NAME=$(basename "${THIS_SCRIPT_DIR}")
THIS_SCRIPT_FILE="$(basename -- "${BASH_SOURCE[0]}")"
THIS_SCRIPT_FILE="${THIS_SCRIPT_FILE%%.*}"
COMMON_UTILS_SOURCE="${THIS_SCRIPT_DIR}/../commonUtils.sh"

source ${COMMON_UTILS_SOURCE}

NOW=$(current_date_time)

LOG_DIR="${THIS_SCRIPT_DIR}/logs"
LOG_FILE_NAME="RunIdgTransactions_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

XML_REQUEST_FILE="${THIS_SCRIPT_DIR}/Request_XML.xml"
JSON_REQUEST_FILE="${THIS_SCRIPT_DIR}/Request_JSON.json"
EUR_REQUEST_FILE="${THIS_SCRIPT_DIR}/Request_EUR.xml"
USD_REQUEST_FILE="${THIS_SCRIPT_DIR}/Request_USD.xml"
B2B_REQUEST_FILE="${THIS_SCRIPT_DIR}/Request_B2B.xml"

SLEEP_TIME_CYCLE=180

#############
# Functions #
#############
function main() {
    echo_log_file_path
    parse_parameters "$@"

    echo_info "Runnig all transcastions on DP ${IDG_SEVICE_NAME} address: ${DP_ADDRESS}"
    run_all_transactions
}

function parse_parameters() {
    while [[ -n "$1" ]]; do
        case "$1" in
        -h | --help)
            show_help
            exit 0
            ;;
        -a | --idg-address)
            DP_ADDRESS=$2
            if [[ -z ${DP_ADDRESS} ]]; then
                echo_error "Invalid parameters. DP address is missing"
                show_help
                exit 1
            fi
            shift 2
            ;;
        -i | --idg-service)
            IDG_SEVICE_NAME=$2
            if [[ -z ${IDG_SEVICE_NAME} ]]; then
                echo_error "Invalid parameters. IDG service name is missing"
                show_help
                exit 1
            fi
            shift 2
            ;;
        *)
            echo_error "Invalid parameter: $1"
            show_help
            exit 1
            ;;
        esac
    done

    if [[ -z ${IDG_SEVICE_NAME} ]]; then
        echo_error "Invalid parameters. Mandatory Parameter '-i|--idg-name' is missing "
        exit 2
    fi

    if [[ -z ${DP_ADDRESS} ]]; then
        echo_error "Invalid parameters. Mandatory Parameter '-a | --idg-address' is missing "
        exit 2
    fi
}

function show_help() {
    echo_info " "
    echo_info $"Usage: $0 -i|--idg-name -a|--action"
    echo_info "	Or :  $0 -h|--help"
    echo_info ""
    echo_info "-i|--idg-name			    : IDG service name. This is a mandatory parameter."
    echo_info "-a|--action			    : The action to take. Can be 'start' or 'stop'. This is a mandatory parameter."
    echo_info "-h| --help                : Display help                      "
    echo_info " "
    echo_info "For Example: "
    echo_info "		$0 -i idg2"
    echo_info " "
    return 0
}

function run_all_transactions {
    # General XML and JSON requests
    while true; do
        bank_a_domain &
        bank_b_domain &
        morgage_a_domain &
        stock_trade_a_domain &
        flight_a_domain &
        airport_a_domain &
        hospital_a_domain &
        university_a_domain &
        train_a_domain &
        government_a_domain &
        insurance_a_domain &
        b2b_domain &
        sidecalls_a_domain &
        tcp_ssl_proxy_transations &
        early_completed &

        echo_info "*** Sleep for ${SLEEP_TIME_CYCLE} seconds ***"
        sleep ${SLEEP_TIME_CYCLE}
    done
}

function bank_a_domain {
    sleep 30
    echo_info "Executing BankA_Domain transactions"
    run_transaction 8001 /ChequeDeposit_MHX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8002 /ChequeDeposit_MHJ/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8003 /ChequeDeposit_MHN/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8004 /ChequeDeposit_XHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8005 /ChequeWithdrawal_WSS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8006 /ChequeWithdrawal_MSX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8007 /BalanceCheck_XLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8008 /BalanceCheck_WLS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8009 /BalanceCheck_MLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8010 /AccountStatus_MHJV/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8012 /AccountStatus_WHSW/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8013 /CreateAccount_MHXE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8014 /CreateAccount_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
}

function bank_b_domain {
    sleep 40
    echo_info "Executing BankB_Domain transactions"
    run_transaction 8017 /CloseAccount_WHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8018 /CloseAccount_MHX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8019 /CloseAccount_MHJ/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8020 /CloseAccount_MHN/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8021 /CloseAccount_XHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8022 /MakeAppointment_WSS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8023 /MakeAppointment_MSX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8024 /LoginRequest_XLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8025 /LoginRequest_WLS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8026 /LoginRequest_MLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8027 /ForgotPassword_MHJV/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8029 /ForgotPassword_WHSW/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8030 /LogoutRequest_MHXE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8031 /LogoutRequest_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
}

function morgage_a_domain {
    sleep 50
    echo_info "Executing MorgageA_Domain transactions"
    run_transaction 8034 /ManagePortfolio_WHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8035 /ManagePortfolio_MHX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8036 /ManagePortfolio_MHJ/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8037 /ManagePortfolio_MHN/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8038 /ManagePortfolio_XHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8039 /CreatePersonalProfile_WSS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8040 /CreatePersonalProfile_MSX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8041 /SimulateMortgagePlan_XLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8042 /SimulateMortgagePlan_WLS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8043 /SimulateMortgagePlan_MLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8044 /GetMortgageStatus_MHJV/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8046 /GetMortgageStatus_WHSW/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8047 /ApplyForMortgage_MHXE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8048 /ApplyForMortgage_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
}

function stock_trade_a_domain {
    sleep 60
    echo_info "Executing StockTradeA_Domain transactions"
    run_transaction 8051 /BuyStock_WHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8052 /BuyStock_MHX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8053 /BuyStock_MHJ/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8054 /BuyStock_MHN/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8055 /BuyStock_XHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8056 /CellStock_WSS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8057 /CellStock_MSX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8058 /CreateAlarmFromStock_XLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8059 /CreateAlarmFromStock_WLS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8060 /CreateAlarmFromStock_MLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8061 /StockQuotes_MHJV/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8063 /StockQuotes_WHSW/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8064 /GetStockAnalisys_MHXE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8065 /GetStockAnalisys_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
}

function flight_a_domain {
    sleep 70
    echo_info "Executing FlightA_Domain transactions"
    run_transaction 8068 /LookForConnectionFlights_WHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8069 /LookForConnectionFlights_MHX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8070 /LookForConnectionFlights_MHJ/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8071 /LookForConnectionFlights_MHN/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8072 /LookForConnectionFlights_XHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8073 /CheckFlightsFromOrigin_WSS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8074 /CheckFlightsFromOrigin_MSX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8075 /CheckFlightsToDestination_XLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8076 /CheckFlightsToDestination_WLS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8077 /CheckFlightsToDestination_MLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8078 /CheckCheapestFlightWithinDates_MHJV/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8080 /CheckCheapestFlightWithinDates_WHSW/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8081 /ChooseSeat_MHXE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8082 /ChooseSeat_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
}

function airport_a_domain {
    sleep 80
    echo_info "Executing AirportA_Domain transactions"
    run_transaction 8085 /ReportMissingLougage_WHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8086 /ReportMissingLougage_MHX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8087 /ReportMissingLougage_MHJ/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8088 /ReportMissingLougage_MHN/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8089 /ReportMissingLougage_XHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8090 /UpdateNoFlightZones_WSS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8091 /UpdateNoFlightZones_MSX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8092 /UpdateSecurityRegulations_XLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8093 /UpdateSecurityRegulations_WLS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8094 /UpdateSecurityRegulations_MLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8095 /UpdateWantedMenProfiles_MHJV/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8097 /UpdateWantedMenProfiles_WHSW/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8098 /AddFlightJurnal_MHXE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8099 /AddFlightJurnal_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
}

function hotels_a_domain {
    sleep 90
    echo_info "Executing HotelsA_Domain transactions"
    run_transaction 8102 /GetUserRatings_WHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8103 /GetUserRatings_MHX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8104 /GetUserRatings_MHJ/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8105 /GetUserRatings_MHN/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8106 /GetUserRatings_XHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8107 /GetOffersForHotels_WSS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8108 /GetOffersForHotels_MSX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8109 /GetRecomendationsForTours_XLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8110 /GetRecomendationsForTours_WLS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8111 /GetRecomendationsForTours_MLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8112 /scheduleTour_MHJV/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8114 /scheduleTour_WHSW/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8115 /CustomerComplaint_MHXE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8116 /CustomerComplaint_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
}

function university_a_domain {
    sleep 100
    echo_info "Executing UniversityA_Domain transactions"
    run_transaction 8119 /SubscribeToCourse_WHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8120 /SubscribeToCourse_MHX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8121 /SubscribeToCourse_MHJ/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8122 /SubscribeToCourse_MHN/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8123 /SubscribeToCourse_XHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8124 /CheckForExamGrade_WSS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8125 /CheckForExamGrade_MSX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8126 /HandInHomeWork_XLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8127 /HandInHomeWork_WLS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8128 /HandInHomeWork_MLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8129 /GetAvalableCourses_MHJV/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8131 /GetAvalableCourses_WHSW/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8132 /GetTeacherInfo_MHXE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8133 /GetTeacherInfo_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
}

function train_a_domain {
    sleep 110
    echo_info "Executing TrainA_Domain transactions"
    run_transaction 8136 /CheckForguttenItems_WHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8137 /CheckForguttenItems_MHX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8138 /CheckForguttenItems_MHJ/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8139 /CheckForguttenItems_MHN/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8140 /CheckForguttenItems_XHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8141 /CheckForTrainSchedule_WSS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8142 /CheckForTrainSchedule_MSX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8143 /FindBestRoute_XLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8144 /FindBestRoute_WLS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8145 /FindBestRoute_MLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8146 /ReplaceCabin_MHJV/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8148 /ReplaceCabin_WHSW/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8149 /GetNotificationForLateTrain_MHXE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8150 /GetNotificationForLateTrain_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
}

function government_a_domain {
    sleep 120
    echo_info "Executing GovernmentA_Domain transactions"
    run_transaction 8153 /ApplyForCitizenship_WHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8154 /ApplyForCitizenship_MHX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8155 /ApplyForCitizenship_MHJ/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8156 /ApplyForCitizenship_MHN/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8157 /ApplyForCitizenship_XHS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8158 /SubmitPapers_WSS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8159 /SubmitPapers_MSX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8160 /RequestTaxDiscount_XLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8161 /RequestTaxDiscount_WLS/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8162 /RequestTaxDiscount_MLX/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8163 /ApplyForVisa_MHJV/Service.asmx ${JSON_REQUEST_FILE} json
    run_transaction 8165 /ApplyForVisa_WHSW/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8166 /RequestRefugeeStatus_MHXE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 8167 /RequestRefugeeStatus_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
}

function insurance_a_domain {
    sleep 20
    run_transaction 8414 /CarInsurancePrices_MMX/Service.asmx ${XML_REQUEST_FILE} xml
}

function hospital_a_domain {
    sleep 25
    echo_info "Executing HospitalA_Domain transactions"
    run_transaction 9003 /CheckLaboratoryResults_401_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 9004 /UpdateDoctorSchedule_401p_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 9005 /CheckMRITestResutls_404_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 9006 /CheckXRayTestResult_404p_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 9007 /CallMorgue_500_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 9008 /WritePrescription_500p_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 9009 /HospitalizationSummery_000_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 9010 /EmergencyRoomVisitSummery_000p_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 9011 /RehabilitationStatus_200_WHSE/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 9048 /ListBloodTestResults/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 9049 /ListPatientTreatmentHistory/Service.asmx ${XML_REQUEST_FILE} xml
}

function b2b_domain {
    sleep 5
    echo_info "Executing B2B transactions"
    run_transaction 31001 /B2B_Request ${B2B_REQUEST_FILE} xml
}

function sidecalls_a_domain {
    sleep 10
    echo_info "Executing SideCalls_Domain transactions"
    run_transaction 7662 /SideCalls.asmx ${EUR_REQUEST_FILE} xml
    run_transaction 7662 /SideCalls.asmx ${USD_REQUEST_FILE} xml
}

function tcp_ssl_proxy_transations {
    sleep 15
    echo_info "Executing TCP/SSL/PROXY transactions"
    run_transaction 4434 /CheckPatientRecord-SSL-Pr/Service.asmx ${XML_REQUEST_FILE} xml https
    run_transaction 4433 /PatientXRayResults-E-SSL-Pr/Service.asmx ${XML_REQUEST_FILE} xml https
    run_transaction 9080 /CheckPatientHealth-TCP-Pr/Service.asmx ${XML_REQUEST_FILE} xml
    run_transaction 9081 /CheckOperatingRoomAvailability-E-TCP-Pr/Service.asmx ${XML_REQUEST_FILE} xml
}

function early_completed() {
    sleep 25
    # wrong CypherSpec
    run_transaction 1080 /ProxyToWrongCipher/Service.asmx ${XML_REQUEST_FILE} xml
    # wrong SSL version
    run_transaction 1080 /ProxyToWrongSSLVersion/Service.asmx ${XML_REQUEST_FILE} xml
    # wrong URI
    run_transaction 1080 /wrongURI/Service.asmx ${XML_REQUEST_FILE} xml
    # no ClientCert
    run_transaction 10443 /EarlySSLFailures/Service.asmx ${XML_REQUEST_FILE} xml https
    # non-SSL call
    run_transaction 10443 /EarlySSLFailures/Service.asmx ${XML_REQUEST_FILE} xml
}

function run_transaction() {
    [[ "$#" -ne 4 ]] && [[ "$#" -ne 5 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1

    local PORT=$1
    local URI=$2
    local REQUEST_FILE=$3
    local REQUEST_TYPE=$4
    local PROTOCOL=$5
    local HEADERS=""

    # Setting default values
    if [[ ${REQUEST_TYPE} == "json" ]]; then
        HEADERS="Content-Type: application/json; Access: application/json"
    fi

    if [[ -z "${PROTOCOL}" ]]; then
        PROTOCOL="http"
    fi

    curl -H ${HEADERS} -X POST -d "@${REQUEST_FILE}" --insecure -w "\n" ${PROTOCOL}://${DP_ADDRESS}:${PORT}${URI} >>${LOG_FILE} 2>&1 &
}

#################
# Invoking main #
#################
main "$@"
