#!/bin/bash
#
# Use this script to convert the Simulator Splunk app to a Splunkbase App so it can be uploaded to Splunkbase.
# The Splunkbase app is stored here so we can manage the changes, but it's not in use in the Simulator itself.

#################################################################################################################
##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
#################################################################################################################

# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace
set -o pipefail

###########################
# Variables and Constants #
###########################

THIS_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
THIS_SCRIPT_DIR_NAME=$(basename "${THIS_SCRIPT_DIR}")
THIS_SCRIPT_FILE="$(basename -- "${BASH_SOURCE[0]}")"
THIS_SCRIPT_FILE="${THIS_SCRIPT_FILE%%.*}"
COMMON_UTILS_SOURCE="${THIS_SCRIPT_DIR}/../../../scripts/commonUtils.sh"
LOG_FILE="/logs/${THIS_SCRIPT_FILE}/${THIS_SCRIPT_FILE}_$(date +"%F_%H-%M-%S").log"

#############
# Functions #
#############
function main() {
	source ${COMMON_UTILS_SOURCE}

	echo_log_file_path

	local SRC_DIR="${THIS_SCRIPT_DIR}/../volumes_splunk/config/dpod.app"
	local DST_APP_DIR="${THIS_SCRIPT_DIR}/dpod"
	local DST_TAR_FILE="/tmp/dpod.tar.gz"

	echo_info "Creating/updating the Splunkbase app in ${DST_APP_DIR}..."
	rm -fr "${DST_APP_DIR}"
	cp -r "${SRC_DIR}" "${DST_APP_DIR}"

	echo_info "Removing local configuration used for the simulator only..."
	rm -fr "${DST_APP_DIR}/bin" "${DST_APP_DIR}/local" "${DST_APP_DIR}/lookups/*" "${DST_APP_DIR}/metadata/local.meta"

	echo_info "Setting files and directories permissions..."
	find "${DST_APP_DIR}" -type d -exec chmod 755 {} \;
	find "${DST_APP_DIR}" -type f -exec chmod 644 {} \;

	echo_info "Creating the Splunkbase app tar file at ${DST_TAR_FILE}..."
	tar -zcvf "${DST_TAR_FILE}" -C "${DST_APP_DIR}/../" dpod

	echo_info "The Splunkbase app was updated and the tar file is at ${DST_TAR_FILE}"
}

#################
# Invoking main #
#################
main "$@"
