#!/bin/bash

#################################################################################################################
##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
#################################################################################################################

# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace
set -o pipefail
set -o nounset

###########################
# Variables and Constants #
###########################
THIS_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
THIS_SCRIPT_DIR_NAME=$(basename "${THIS_SCRIPT_DIR}")
THIS_SCRIPT_FILE="$(basename -- "${BASH_SOURCE[0]}")"
THIS_SCRIPT_FILE="${THIS_SCRIPT_FILE%%.*}"
COMMON_UTILS_SOURCE="${THIS_SCRIPT_DIR}/commonUtils.sh"

source ${COMMON_UTILS_SOURCE}

NOW=$(current_date_time)

SCRIPTS_DIR="/MonTier/scripts"

LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="activateSplunkOperations_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

CLEAN_DATA_FLAG_UP=0
LOOP_RESTART_FLAG_UP=0
RESTART_ARGS=""
TIME_TO_SLEEP_BETWEEN_RESTARTS=0

#############
# Functions #
#############
function main() {
	echo_log_file_path

	parse_parameters "$@"

	local RESTART_ARGS=""
	if [[ ${CLEAN_DATA_FLAG_UP} -eq 1 ]]; then
		RESTART_ARGS="${RESTART_ARGS} -c"
	fi
	if [[ ${LOOP_RESTART_FLAG_UP} -eq 1 ]]; then
		RESTART_ARGS="${RESTART_ARGS} -l ${TIME_TO_SLEEP_BETWEEN_RESTARTS}"
	fi

	echo_info "Activating splunk restart in the background"
	nohup ${THIS_SCRIPT_DIR}/restartSplunk.sh ${RESTART_ARGS} >>${LOG_FILE} 2>&1 &
}

function parse_parameters() {
	while [[ $# -gt 0 ]]; do
		case "$1" in
		-h | --help)
			show_help
			exit 0
			;;
		-c | --clean-data)
			CLEAN_DATA_FLAG_UP=1
			shift 1
			;;
		-l | --loop-restart)
			LOOP_RESTART_FLAG_UP=1
			TIME_TO_SLEEP_BETWEEN_RESTARTS=$2
			if [[ -z ${TIME_TO_SLEEP_BETWEEN_RESTARTS} ]]; then
				echo "ERROR : Invalid parameters. sleep duration is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo_error "Invalid parameter : $1 ! "
			show_help
			exit 1
			;;
		esac
	done
}

function show_help() {
	echo_info " "
	echo_info $"Usage: $0 -l|--loop-restart -c|--clean-data"
	echo_info "	Or   : $0 -h|--help"
	echo_info " "
	echo_info "-l|--loop-restart   : The number of seconds to sleep between restarts."
	echo_info "                      This is an optional parameter. "
	echo_info "                      If this parameter is not raised, a one-time restart will be performed. If raised, the restart will accur in an endless loop."
	echo_info "-c|--clean-data     : Clean all data. This is an optional flag."
	echo_info "-h| --help          : Display help"
	echo_info " "
}

#################
# Invoking main #
#################
main "$@"
