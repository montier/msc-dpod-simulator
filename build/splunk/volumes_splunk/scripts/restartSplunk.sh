#!/bin/bash

#################################################################################################################
##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
#################################################################################################################

#############
# functions #
#############

function current_date_time() {
	echo $(date +%F_%H-%M-%S)
}

function log() {
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}

	echo "$(current_date_time): ${priority} ${message}" >>${LOG_FILE}
}

function debug() {
	log DEBUG $@
}

function info() {
	log INFO $@
}

function warn() {
	log WARN $@
}

function error() {
	log ERROR $@
}

function critic() {
	log CRITIC $@
}

function show_help() {
	echo " "
	echo $"Usage: $0 -l|--loop -c|--clean-data "
	echo "	Or :  $0 -h|--help"
	echo ""
	echo "-l|--loop			        : The number of seconds to sleep between restarts."
	echo "									This is an optional parameter. "
	echo "                                  If this parameter is not raised, a one-time restart will be performed. If raised, the restart will accur in an endless loop."
	echo "-c|--clean-data			: Clean all data. This is an optional flag."
	echo "-h| --help                : Display help                      "
	echo " "
	echo "For Example: "
	echo "		$0 -c"
	echo "Or: 	$0 -l 86400 -c"
	echo "Or: 	$0"
	echo " "
	return 0
}

function restart_splunk() {

	local RESTART_SPLUNK_RC=0

	info "Stopping splunk"
	echo "$(current_date_time): INFO: Stopping splunk"
	/opt/splunk/bin/splunk stop >>${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		error "Failed to stop splunk. Abort"
		echo "$(current_date_time): ERROR: Failed to stop splunk. Abort"
		ABORT=1
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		if [[ ${CLEAN_DATA_FLAG_UP} -eq 1 ]]; then
			info "Cleanning splunk data"
			echo "$(current_date_time): INFO: Cleanning splunk data"
			/opt/splunk/bin/splunk clean eventdata -f >>${LOG_FILE} 2>&1
			if [ $? -ne 0 ]; then
				error "Failed to clean splunk data. Abort"
				echo "$(current_date_time): ERROR: Failed to clean splunk data. Abort"
				ABORT=1
			fi
		fi
	fi

	info "Starting splunk"
	echo "$(current_date_time): INFO: Starting splunk"
	/opt/splunk/bin/splunk start >>${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		error "Failed to start splunk. Abort"
		echo "$(current_date_time): ERROR: Failed to start splunk. Abort"
		ABORT=1
	fi

	return ${RESTART_SPLUNK_RC}
}

###########################
# variables and constants #
###########################

NOW=$(date +"%F_%H-%M-%S")

SCRIPTS_DIR="/MonTier/scripts/restartSplunk"

LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="restartSplunk_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

CLEAN_DATA_FLAG_UP=0
LOOP_FLAG_UP=0

ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
	-h | --help)
		show_help
		exit 0
		;;
	-c | --clean-data)
		CLEAN_DATA_FLAG_UP=1
		shift 1
		;;
	-l | --loop)
		LOOP_FLAG_UP=1
		TIME_TO_SLEEP_BETWEEN_RESTARTS=$2
		if [[ -z ${TIME_TO_SLEEP_BETWEEN_RESTARTS} ]]; then
			echo "ERROR : Invalid parameters. sleep duration is missing"
			show_help
			exit 1
		fi
		shift 2
		;;
	*)
		echo "ERROR : Invalid parameter : $1 ! "
		show_help
		exit 1
		;;
	esac
done

############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}

if [[ ${LOOP_FLAG_UP} -eq 1 ]]; then
	while [[ ${ABORT} -eq 0 ]]; do
		info "Sleeping ${TIME_TO_SLEEP_BETWEEN_RESTARTS} seconds before next splunk restart"
		echo "$(current_date_time): INFO: Sleeping ${TIME_TO_SLEEP_BETWEEN_RESTARTS} seconds before next splunk restart"
		sleep ${TIME_TO_SLEEP_BETWEEN_RESTARTS}
		restart_splunk
	done
else
	restart_splunk
fi

exit ${ABORT}
