#!/bin/bash

#################################################################################################################
##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
#################################################################################################################

# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace
set -o pipefail
set -o nounset

###########################
# Variables and Constants #
###########################
THIS_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
THIS_SCRIPT_DIR_NAME=$(basename "${THIS_SCRIPT_DIR}")
THIS_SCRIPT_FILE="$(basename -- "${BASH_SOURCE[0]}")"
THIS_SCRIPT_FILE="${THIS_SCRIPT_FILE%%.*}"
COMMON_UTILS_SOURCE="${THIS_SCRIPT_DIR}/commonUtils.sh"

LOG_FILE="" # No log file, but we must define the variable still

source ${COMMON_UTILS_SOURCE}

#############
# Functions #
#############
function main() {
	parse_parameters "$@"

	copy-splunk-config

	echo_info "Launching Splunk..."
	exec /sbin/entrypoint.sh start-service
}

function parse_parameters() {
	while [[ $# -gt 0 ]]; do
		case "$1" in
		-h | --help)
			show_help
			exit 0
			;;
		*)
			echo_error "Invalid parameter : $1 ! "
			show_help
			exit 1
			;;
		esac
	done
}

function show_help() {
	echo_info " "
	echo_info $"Usage: $0"
	echo_info "	Or :  $0 -h|--help"
	echo_info " "
	echo_info "-h|--help       			: Display help   "
	echo_info " "
}

function copy-splunk-config() {
	echo_info "Using user ${SPLUNK_USER}"

	echo_info "Copy system config..."
	sudo -u ${SPLUNK_USER} mkdir -p /opt/splunk/etc/system/local
	sudo -u ${SPLUNK_USER} cp -r /MonTier/config/system-prefs/* /opt/splunk/etc/system/local/

	echo_info "Copy user config..."
	sudo -u ${SPLUNK_USER} mkdir -p /opt/splunk/etc/users/admin/user-prefs/local
	sudo -u ${SPLUNK_USER} cp -r /MonTier/config/user-prefs/* /opt/splunk/etc/users/admin/user-prefs/local

	echo_info "Copy DPOD app..."
	sudo -u ${SPLUNK_USER} mkdir -p /opt/splunk/etc/apps/dpod
	sudo -u ${SPLUNK_USER} cp -r /MonTier/config/dpod.app/* /opt/splunk/etc/apps/dpod/

	echo_info "Copy introspection app config..."
	sudo -u ${SPLUNK_USER} cp -r /MonTier/config/introspection.app/* /opt/splunk/etc/apps/introspection_generator_addon/
}

#################
# Invoking main #
#################
main "$@"
