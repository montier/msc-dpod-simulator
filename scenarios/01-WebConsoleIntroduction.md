# Web Console Introduction

1. Sign in to the Web Console using an admin user.
2. Open Recent Activity dashboard (in the future - System Health dashboard).
3. ** Explain **: In the following demonstration we will walk you through some of the most common scenarios of using DPOD.
4. ** Explain **: DPOD provides a centralized view across all monitored DataPower Gateways via its Web Console.
5. Show Web Console layout very quickly:
    * *Side menus* on the left allow to navigate to the different parts of the Web Console
    * *User menu* - we are currently using the admin so all features are available for demonstrating
    * *Product views* - both IBM DataPower Gateways and API-Connect are supported. Each product has its own view within the Web Console.
    * *Filters* - allow us to focus on a time range, devices, domains, etc. Include multiple selection.

