# Troubleshooting a Back-End Latency Issue

## Background
Explain: Latency issues are one of the most common issues that DataPower administrators face.
DPOD is very efficient at troubleshooting latency issues.
As a DataPower administrator, you receive a complaint about the latency of a service.
The service latency was OK for a long time, but suddenly increased and constantly exceeded the SLA.

## Demo Steps
1. Open Service Latency dashboard.
2. **Explain:** Service Latency dashboard is used to investigate the latency of services.
3. **Explain:** In general, the purpose of the dashboards is to help the user identify or focus on a problem in order to investigate it.
4. **Explain:** Even if we don't know the exact service name, we can filter by device or domain and see the slowest services in the dashboard.
5. Choose the domain Domain: BankB_Domain and show the top slowest services within that domain.
6. Choose the service MakeAppointment_WSS.WSP and show its behaviour.
7. Show that the problem is in the back-end processing, and identify when the problem started.

8. **Explain**: In order to further investigate, DPOD allows us to drill-down to specific transactions.
9. Open Transactions page and go through the displayed columns.
10. Click a transaction ID and drill-down to a specific transaction.
11. Go through the details of the transaction - information, error analysis, elapsed time, memory analysis, raw messages, payload, extended latency and side calls.
12. Show that the problem is in the back-end processing in the elapsed time diagram.

13. **Explain**: We identified the source of the latency issue, but we still need to explain why the latency changed suddenly.
14. Open Service Configuration page. It should already have the correct filters for this specific service.
15. Click on one of the rows and drill-down to the service.
16. Show the information about the service, including the Change Audit.
17. Show that a change in the service configuration occurred at the time that the latency change occurred.
18. **Explain**: The change in the back-end URL of the service caused the change in the latency.

19. **Explain**: Now, that we found the problem, we would like to be alerted if it happens again in the future.
20. Open Service URI Calls dashboard and choose the same service: Service: MakeAppointment_WSS.WSP.
21. **Explain**: This dashboards displays the various URIs invoked by the service, and the latency percentiles.
22. Hover over one of the numbers and click the icon to create an alert.
23. **Explain**: DPOD can send alerts via Syslog or Email.
24. Open Alerts page and show the pre-configured alerts list.

## Features Presented
1. Service Latency dashboard
2. Filters + zoom
3. Transactions page
4. Transaction page: elapsed time, memory analysis, raw messages, payload, extended latency
5. Service Configuration page
6. Service Configuration Change Audit
7. Service URI Calls dashboard
8. Alerts