# Troubleshooting a Side-Call Latency Issue

## Background
As a DataPower administrator, you receive a complaint about the latency of a service.
The service latency seems to be unstable - sometimes the latency is low, sometimes it is high.

## Demo Steps
1.  Open Service Latency dashboard.
2.  Choose the unstable service: _*sideCalls_JIRA662.MPGW.*_
3.  Show that the problem is in DataPower request processing, and indeed sometimes happens and sometimes doesn't.
4.  **Explain**: In order to further investigate, DPOD allows us to drill-down to specific transactions.
5.  Open Transactions page and show that some transactions are fast and some are slow.
6.  Click on transaction IDs and drill-down to specific transactions.
7.  In the slow transactions, show that the problem is in DataPower request processing in the elapsed time diagram.
8.  Show that a probe is enabled, and this might cause latency issues in production environments. It is recommended to disable the probe, although this does not explain the issue.
9.  Show the difference in the payload between slow and fast transactions - values of USD are served slow while EUR are served fast.
10. Show the slow steps in the extended latency and the slow side calls (different servers for USD and EUR).
11. **Explain**: Depending on the payload, a different endpoint in used using side calls, which cause some of the transactions to have high latency.

## Features Presented
1. Service Latency dashboard
2. Transaction page: payload, extended latency, side calls
3. Probe detection