# Troubleshooting an API Error as A Developer using Self-Service
## Background
DPOD supports API transactions processed by API Connect within DataPower Gateway.
DPOD is very useful for non-production environments by providing limited access transactional information for developers and service consumers.
As a DataPower administrator, you may offload some of the investigation process to developers.

## Demo Steps
1. Switch to APIC product view.
2. Open Recent Activity dashboard.
3. Show the special filters with API terminology.
4. Open API Availability and show the page.

5. Open again Recent Activity dashboard.
6. Click on service XXX which is at the top of the latency box.
7. Show the special filters of the transactions page of APIC. Make sure to show customer APIs from 2 differnet catclog
8. Drill down to a transaction.
9. Show all the details that are specific to APIC and the latency issue is at the backend.
10. Show how the payload is transformed from JSON to XML.

11. Open Roles page.
12. Click on SelfService custom role.
13. Explain: A DPOD admin may define custom roles that limit the access of users to the information. For example, a user may be limited to specific services and domains.
14. Show the limitations of SelfService custom role - no payloads, no raw messages, specific domains.
    Now, if a developer receives the famous "Internal Error" message when invoking a request to DataPower, they can use their limited user to investigate the issue.
    The DataPower administrator can focus on production issues while the developer solves their issues.
15. Sign out and then sign in with the self-service user.
16. Switch to APIC product view.
17. Open transaction list page under Investigate Menu.
    **Explain**: In the list you will only see APIs from CatalogA.
17. Show the limited domains list and filter only erroneous transactions.
18. Click on an API and drill-down to a transaction.
19. Show the error analysis of the transaction.
20. Explain: DPOD analyses the raw messages of each transaction and summarizes the errors it detected.
    So even a user with no knowledge if DataPower may investigate problems without being exposed to sensitive data that might exist in payloads and raw messages.

21. Sign out and then sign in with the admin user.

22. Open again Recent Activity dashboard.
23. Click on service XXX which is at the top of the API Errors box.
24. Show 2 errors - one with OAUTH details missing, and one with missing client ID

# Features Presented
1. APIs Recent Activity dashboard
2. APIs Availability dashboard
3. APIs assembly execution flow and latencies breakdown