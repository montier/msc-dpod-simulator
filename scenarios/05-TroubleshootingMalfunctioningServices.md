# Troubleshooting Malfunctioning Services

## Background
Sometimes services seem to be completely out-of-service, and return errors only.
As a DataPower administrator, you receive a complaint that a new service is malfunctioning and always returns errors.

## Demo Steps
1. Open Service Activity page.
   **Explain**: We are trying to see if there are transactions of that service. If there are transactions, it means that the service is actually running.
2. Show that there are no transactions of that service - Domain: EarlyFailedTrans_Domain - so indeed there is a problem.

3. Open Failed Objects page.
   **Explain**: DPOD constantly scans for failed objects in the monitored DataPower Gateways. If the service object is down, it might be the reason the service is malfunctioning.
4. Filter by domain EarlyFailedTrans_Domain and show that the service object is not down, which means this is not the problem.

5. Open Expired Certificates page.
   **Explain**: DPOD displays a list of expired certificates and certificates that are about to expire. If a certificate of a service has expired, it might be the reason the service is malfunctioning.
6. Show that there are no expired certificates, which means this is not the problem.

7. Open Early Failing page.
   **Explain**: Some requests are rejected quite early in DataPower Gateway. For example, if the URL is incorrect, or a client certificate is missing when required. DPOD identifies such cases.
8. Show that there are many early failing requests - the consumer is not sending a client certificate which is why the requests are rejected.

## Features Presented
1. Service Activity page
2. Failed Objects page
3. Expired Certificates dashboard
4. Early Failing page