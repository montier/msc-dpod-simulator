# Troubleshooting a Major Out-of-Service Issue

## Background
As a DataPower administrator, you receive a complaint that during the night there was a temporary major out-of-service issue where many services did not respond for a couple of minutes.
Since then, everything seems to be normal.

## Demo Steps
1. Open System Overview dashboard.
   **Explain**: Such a major issue indicates there was an error in an entire device or a domain.
2. Show that the system activity was affected by the outage - the bars are lower during the time of the issue.
3. Show that all the metrics of the devices (CPU, memory) are normal, which indicate that there wasn't a resource problem at the device level.
4. Show that there are no special system errors from the devices, which indicate that there wasn't a problem at the device level.

5. Open Service Memory dashboard.
6. Show that the service memory was sampled during the outage, which means that the device was up and running - it is not something that affected the entire device.
7. Open Restarts dashboard.
   **Explain**: DPOD keeps track of DataPower's audit logs and detects restarts of domains and devices.
8. Show that there was a domain restart at the time of the outage which is the reason of the problem.

## Features Presented
1. System Overview dashboard
2. Restarts dashboard