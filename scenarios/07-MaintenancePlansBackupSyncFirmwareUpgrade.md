# Maintenance Plans: Backup, Sync & Firmware Upgrade
## Background
DPOD does not only monitor devices, but also performs maintenance tasks such as backups, configuration synchronization and firmware upgrade.
The Backup allows to perform a secure backup or domain export.
The Synchronization allows to replicate domains or entire devices, for high availability or DevOps scenarios.
The Firmware Upgrade allows to upgrade the firmware of several gateways using best practices.
All maintenance plans can be executed ad-hoc or can be scheduled.

## Terminology
Open Backup page and demonstrate the terminology on Daily Domains Export backup plan.
Maintenance Plan: A set of maintenance activities with a schedule definition and email/Syslog notifications. For example: "Daily backup of all gateways".
Maintenance Activity: An action performed on one or more domains or devices. For example: "Secure backup of all devices with a name that start with 'Prod*'".
Maintenance Task: A system task that is created for performing an action on a specific domain or device, to keep track of everything that is performed within an activity. Created by the system every time an activity is executed.
Maintenance Window: A period of time during which maintenance plans are allowed to start executing.
Error Policy: Controls what happens when a task fails (during either validation or execution):
When the error policy is "Halt" - all waiting tasks will be cancelled and will not execute. Executing tasks will not be interrupted.
When the error policy is "Ignore" - remaining tasks will continue to execute as normal.
Maintenance plans may be executed by schedule, via REST API or ad-hoc.
Pre and Post Plan Scripts: Customer can customize a plan execution with a pre and post scripts. the post scripts will have the error code of the overall plan thus allowing to handle error (such as notifying an enterprise monitoring system).


## Demo Steps
1. Open Backup → Add Activity page: Allows performing a secure backup for en entire device or a domain export for specific domains, with optionally executing a script and quiescing the target domain/device.
2. Open Synchronization → Add Activity page: Allows duplicating a domain, set of domains or an entire device for high availability or DevOps scenarios, with optionally executing a script and quiescing the target domain/device.
3. Open Firmware Upgrade → Add Activity page: Allows upgrading a firmware of several devices by uploading an image file to DPOD and based on best practices, such as performing quiesce and restarting before applying the upgrade.
4. Show the status of an executed backup plan.

## Features Presented
1. Maintenance Plans




