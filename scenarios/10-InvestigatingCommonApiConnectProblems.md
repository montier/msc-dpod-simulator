# Investigating Common API-Connect Problems

## Background
When invoking API-Connect APIs for the first time, a number of common problems might arise. This demo shows how to easily investigate these problems using DPOD Web Console.
Cases featured:
1. HTTP 404, URL not found
2. HTTP 401, OAuth scope invalid
3. HTTP 403, OAuth scope forbidden for the API
4. HTTP 500, missing element in message payload
5. HTTP 200 OK, with faulty reply at application-level

## Demo Steps

### Case 1 – HTTP 404, URL not found
1. When calling the API, the user got an HTTP 404 "not found" response.
2. Go into the "Investigate" section, and into the "Early-Failing" tab.
3. **Explanation:** Because the user got a 4xx response, we know that the call failed before the execution stage of the API. For these cases DPOD provides the Early-Failing page.
4. Enter the following filter: Path URL = https://172.17.100.70:443/montierproviderorg508/montiercatalog/accounts-service/balance
5. **Explanation:** We cannot filter by the API name, because we are not sure that the URL is correct and that we even reached the API.
6. Click on one of the invocations, scroll down to error analysis. The message is: "HTTP/1.1 404 Not Found: No resource match requested URI"
7. **Explanation:** The source of the problem is that we called the wrong URL. To find the right URL the user can go to the API-Connect Portal and look at the API properties.

### Case 2 – HTTP 401, Invalid OAuth scope
1. When calling the OAuth API in order to get an OAuth token, the user got an HTTP 401, "invalid scope" response.
2. Go into the "Investigate" section, and into the "Early-Failing" tab. 
3. **Explanation:** Because the user got a 4xx response, we know that the call failed before the execution stage of the API. For these cases DPOD provides the Early-Failing page.
4. Enter the following filters: API Name = oauth, Client IP = 172.17.100.131
5. **Explanation:** Filtering by the calling client IP helps us pinpoint the specific call we made to the API.
6. Click on one of the invocations, scroll down to error analysis. The message is: "Custom scope check encountered an error: scope ‘Detail’ not allowed"
7. **Explanation:** The source of the problem is that the scope is invalid – it probably doesn’t exist. To resolve this, the user can go to the API-Connect Portal and see all the valid scopes which are available.

### Case 3 – HTTP 403, OAuth scope forbidden
1. After getting an OAuth token with a valid scope, the user calls the API and gets an HTTP 403, "Forbidden" response.
2. Go into the "Investigate" section, and into the "Early-Failing" tab.
3. **Explanation:** Because the user got a 4xx response, we know that the call failed before the execution stage of the API. For these cases DPOD provides the Early-Failing page.
4. Enter the following filter: Error Reason = Forbidden
5. Click on one of the invocations, scroll down to error analysis. The message is: "Custom scope check encountered an error: invalid scope: did not match the requested resource "[Details]""
6. **Explanation:** Even though the scope is valid by itself, it is not authorized for the API we were trying to invoke. To resolve this, the user can go to the API-Connect Portal and see which scopes are authorized to invoke this API.

### Case 4 - HTTP 500, missing field
1. After getting a valid OAuth token with an authorized scope, the user called the API and got an HTTP 500, "Request is missing a mandatory field" response.
2. Go into the "Investigate" section. Go to the "Payload Capture" tab. Click "New Subscription". Show this page, and then cancel the action.
3. **Explanation:** Because the error message indicates there was something wrong with the request message, the user needs to turn on payload capture and invoke the API again. Show the new subscription page and explain how message capture works. Because message capture has already been activated beforehand in the simulator, there is no need to actually complete the action.
4. Go back to the "Investigate" section and the "Transactions" tab. Enter the following filter: Error Message = Request is missing a mandatory field
5. **Explanation:** In the previous cases the call failed prior to the execution stage. Because we got HTTP 500, we know that this time the call failed later during processing, which is why we are using the "Transactions" page and not the "Early Failing" tab.
6. Click on one of the transactions, scroll down to error analysis. The message is: "invalid object: the property ‘customer_id’ is missing"
7. **Explanation:** DPOD error analysis revealed that a mandatory field was not sent. We should look at the message payload to see what the request message actually looks like.
8. Scroll down and click the "Payload" tab. Then scroll down again to see the payload below. We see that the request message has a field called "customer" instead of the required "customer_id".
9. **Explanation:** This is the source of the problem. The consumer sent the wrong field name, probably due to human error or a recent change in the field name. To resolve the issue, we need to correct the request message and try again.

### Case 5 - HTTP 200 OK, faulty applicative reply
1. The user called the API, and got an "HTTP 200 OK" response. However, in the response message they got "You do not have enough money in your account" even though this should not the case.
2. Go into the "Investigate" section. Go to the "Policy Variables Capture" tab. Click "New Capture". Show this page, and then cancel the action.
3. **Explanation:** This is a different sort of error. The transaction completed successfully, but still the operation failed. To investigate such issues, we need to go into how the API processed the request. Variable capture is a unique DPOD feature which enables us to see the values of context variables in each stage of the execution. Show the new capture page and explain how variable capture works. Because it has already been activated beforehand in the simulator, there is no need to actually complete the action.
4. Go back to the "Investigate" section and the "Transactions" tab. Enter the following filters: API Name = accounts, API Version = 1.0.3
5. **Explanation:** Because the transaction finished successfully, it will appear under status "OK".
6. Go into one of the transactions. Scroll down to the execution flow diagram in the "Transaction Analysis" section. Click one of the policies and scroll to the "GetCustomerBalance" policy. Note that in the "Context Variables" section below appears wrong "CustomerBalance = -99,999".
7. **Explanation:** Variable capture is what makes it possible to see the values of context variables, in the execution of each policy. A value of "-99,999" does not make sense, since balances are never allowed to go so low. This indicates that the source of the problem is that for some reason the policy gets the wrong balance. To resolve this we need to contact the API owner and tell them about the problem.

## Features Presented
1. Early-Failing page
2. Transactions page
3. Error analysis for OAuth issues
4. Filter by: Called URL, API Name, API Version, Client IP, Error Reason, Error Message.
5. Payload capture
6. Policy variables capture
