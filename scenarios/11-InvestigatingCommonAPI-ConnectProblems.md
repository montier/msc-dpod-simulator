# Investigating latency problems in API-Connect

## Background
When working with API-Connect API’s, various latency and SLA issues may arise. This demo shows how to investigate these problems using the DPOD console.
Cases featured:
1.	Slow latencies
2.	Variable latencies – API response times vary, being sometimes slower and sometimes faster, with no apparent reason.


## Demo Steps

### Case 1 – Slow latencies
1.	A user reported that an API is working too slowly.
2.	Go to the dashboards section > Analytics > “API Latency” page. 
3. **Explanation**: We would like to verify that the user complaint is correct, and see the history of this API’s latency.
4.	Add filter:
	API Name = balance-convert
	App Name = digitalbankingapp102
5. **Explanation**: Filtering by App name will make sure that we see only invocations which were made by a specific consumer application. The filter results show us that latencies vary between 8 and 10 seconds, which is quite slow, as the user said.
6.	Go to the transactions page
7. **Explanation**: It is worth noting that we still see results filtered for the API name and App name that we selected in the previous page. This is a general feature of DPOD – filters are retained when navigating between various pages (when applicable), which makes the experience of using the DPOD console easy and seamless.
8.	Go into one of the transactions and scroll down to see the execution path diagram
9. **Explanation**: If we look closely, we can see that the policy InvokeGetBalance takes up most of the execution time, taking more than 6 seconds to complete
10.	Click InvokeGetBalance policy to see more details
11. **Explanation**: We can see that this policy performs a side call which takes 6 seconds. This is an internal side call to another API within the API Connect network - We know this by the way this URL is formed. This means that our API calls another, nested API, which is also managed by API Connect.
12.	Scroll up to the top of the page, and copy value from the field “Gl. Transaction ID” from the transaction information section.
13. **Explanation**: Global transaction ID’s are automatically generated by Gateway devices, and are retained when API calls are made to nested API’s. Therefore, we can use the global transaction ID to easily find the internal call made by this transaction.
14.	Go back to the transactions page, reset the filters, and add a new filter:
	Global Trans.s ID = (the value we copied)
15. **Explanation**: We get two transactions - One by balance_convert API, which is the first API that wsa executed, and another by accounts_balance, which is the nested API call.  
16.	Go into the transaction made by the nested API (accounts_balance) and scroll down to see the execution path diagram.
17. **Explanation**: You can see that the getBalanceInvoke policy takes up 5 out of the 5.5 seconds of the total latency.
18.	Click the getBalanceInvoke to see more details
19. **Explanation**: we see that a call is made to an external service, called “AccountService_102”. Note that this URL includes what appears to be a version number, 1.0.2. this will be significant later on.
	We can now say that the origin of the latency problem is the external URL. We can contact the service owner and inform them of the problem. However, we would like to investigate further.
20.	Go to the dashboards section > Analytics > “API Policies” dashboard. Add filter:
	API Name = accounts_balance
	Policy Name = getBalance: invoke
21. **Explanation: We would like to see whether this policy has always been slow, or perhaps something changed recently. As we already know what is the suspect policy, we can filter by specific policy to focus our search.
	Here we see something important. In version 1.0.2 of the API, executing this policy always took around 5 seconds (see average, maximum and minimum latency columns).  However, the same policy in version 1.0.1 took only 164 milliseconds to complete, which is ten times slower. So what happened? What is the difference between versions 1.0.1 and 1.0.2?
22.	Hover with the mouse over policy version 1.0.1 in the version column in the search results. A small popup window should appear. Select the option “Add to filters”
23. **Explanation**: This is a quick and easy way to add new filters. You can of course also add the same filter by entering it manually.
24.	Go to the transactions page and select one of the top transactions. Scroll down into the execution path diagram, and click “getBalance: invoke” policy to see more details.
25. **Explanation*: You can see that the policy calls a similar URL to the one we’ve seen in version 1.0.2. However, this time the URL has a different suffix - the version number 102 does not appear. This means that the older version of the nested API calls a different URL than the newer version. 
	So this is the source of the problem. In the new version of the API, a nested API was called, which also had a newer version. The new version of the nested API calls a different URL than the older one, which is significantly slower.  
	To resolve this, we need to call the owner of the external URL we are invoking, and tell them there are big latency differences between the two versions they’ve deployed. Now that we've pinpointed the issue using the DPOD console, it is up to the API owners to resolve the issue. 
26.	Go to the dashboards section > Analytics > “API URL Calls”. Reset the filters and add a new filter:
	URL Name =https://172.17.100.70:443/montierproviderorg508/bankcatalog/balance-convert-102/convert
27. **Explanation**: Before we move on, we want to make sure that if this situation happens again, we will know about it. DPOD allows us to create an alert that will notify us when any API is taking too long to complete. 
28.	Hover with the mouse over the average latency column. A popup menu will appear. Click “Add Alert on Latency (Service URI)”. 
29. **Explanation**: This opens the alert creation page. This alert will notify us if the average latency goes over a certain threshold, given various conditions which we can configure. As this is only for the sake of the demonstration, you can cancel the action.
30.	To summarize, in this scenario we’ve seen how we can use DPOD to identify the source of latency problems. By leveraging various DPOD dashboards, we can easily identify latency differences between API versions, and use transaction Global ID's to investigate nested calls. We can also create alerts to make sure latency problems do not repeat themselves.


### Case 2 – Variable latencies
1.	A user calls and says that the latency of a service changes from call to call. Sometimes it’s around 8 seconds, and at other times around 10 seconds. 
2.	Go to the dashboards section > select the “Payload Capture” tab > click “New Subscription”
3. 	**Explanation**: To investigate issues like this one, it is often necessary to turn on payload capture, so we can see whether variable latency durations are the result of different payload contents. After turning on the capture, we need to ask the client to re-issue the request so that data is collected. However, in this demo, the payload capture has already been turned on, so we can cancel this action.
4.	Go to the transactions page, reset the filters if there were any, and add a new filter:
	API = balance-convert
	API Version = 1.0.2
5.	**Explanation**: like the user said, by looking at the “Elapsed (ms)” column, we can see that some transactions finished after 8 seconds and some after 10.
6.	Right click a 10-seconds transaction, and select “Open in new windows. Do the same for an 8-seconds transaction. Now arrange the two new windows one on top of the other, so you can compare the two transactions. In both windows, scroll down to the execution path diagrams and compare them.
7.	**Explanation**: At first, we can’t see any significant difference between the transactions. The execution path is the same, and policy execution times are very similar – for example, “getBalance: input” executes at 122ms in one transactions, and 125ms in the other.
8.	In both windows, scroll to the right in the execution path diagrams until you can see the “invoke-GetExchangeRate” policy.
9.	**Explanation**: This policy executes at ~120 milliseconds in the faster transaction, and at ~2 seconds. This must be the source of the problem.
10.	In both windows, click the “invoke-GetExchangeRate” policy to see more details. Scroll down until you can see the side calls made by both transactions.
11.	**Explanation**: We see that the two transactions call different services - one for a currency of US Dollars (The URL ends in “/CurrencyRate-USD”), and another for Euros (ending in “/CurrencyRate-EUR”). So apparently one of these services is much slower than the other, which why one transaction was 2 seconds faster than the other. But why do these two invocations to the same API, result in calls to two different URLs? 
12.	In both windows, scroll down, click the “Payload” tab. Then scroll down again so you can compare both payloads.
13.	**Explanation**: In the transaction which called the USD service, we see that the field “Currency” in the message payload has a value of “USD”. In the other transaction, the value of the same field is “EUR”.
14.	We have pinpointed the source of the problem:  When consumers call the problematic API, they do so with different payloads.  This payload then determines which external service is called.  According to the currency requested by the consumer in the message payload, sometimes a US dollars service is called, and at other times a Euros service.  These two external services have very different latencies, which is why the total API latency is so different.
	To resolve this issue, we will probably need to contact the owners of the slower service and let them know that they have very high latencies. 
15.	To summarize, in both scenarios DPOD has allowed us to resolve complex latency issues simply and effortlessly.  Everything was done from a centralized console, without having to log into separate Gateway devices, call the owners of each API, or dig through the logs.


## Features Presented
1.	API Latency page
2.	API Policies page
3.	Transactions page
4.	Payload capture
5.	Execution flow diagram
6.	Filter by: API Name, API Version, App Name, Policy Name, Policy Version, Global Transaction ID
