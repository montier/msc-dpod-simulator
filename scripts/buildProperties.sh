#!/bin/bash

# Parameters for DPOD image files
DPOD_IMAGE_URL="https://www.dropbox.com/scl/fi/rygf7dwmypanmnmpedv30/DPOD-DOCKER-prod_1.0.22.1_2024_10_29_22_34_43.tar.gz?rlkey=xrexqclqcbbfx0lddmyarqxvi&st=913cqozw&dl=1"
DPOD_MD5_URL="https://www.dropbox.com/scl/fi/hzlg1318r90ab8oactben/DPOD-DOCKER-prod_1.0.22.1_2024_10_29_22_34_43.tar.gz.md5?rlkey=7wo3k5ktp2ox849a5ue9a2kc0&st=0oe8qmj7&dl=1"

# Parameters for DPOD resources files
RESOUCES_URL="https://www.dropbox.com/s/h25wi4fwqrnmsiv/app_resources.tgz"
RESOUCES_MD5_URL="https://www.dropbox.com/s/g9fs38scrg40iq1/app_resources.md5"

export RESOURCES_JDK_FILE="OpenJDK11U-jdk_x64_linux_hotspot_11.0.15_10.tar.gz"
export RESOURCES_APP_DATA_LOADER_FILE="appDataLoader.jar"

# Image versions for all containers
export IDG_IMAGE_TAG="icr.io/cpopen/datapower/datapower-limited:10.6.0.1"
export MQ_IMAGE_TAG="icr.io/ibm-messaging/mq:9.2.4.0-r1"
export GENERATOR_IMAGE_TAG="redhat/ubi9:latest"
export SPLUNK_IMAGE_TAG="splunk/splunk:8.2.8"
