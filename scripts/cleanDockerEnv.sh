#!/bin/bash

#################################################################################################################
##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
#################################################################################################################

# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace
set -o pipefail

###########################
# Variables and Constants #
###########################
THIS_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
THIS_SCRIPT_DIR_NAME=$(basename "${THIS_SCRIPT_DIR}")
THIS_SCRIPT_FILE="$(basename -- "${BASH_SOURCE[0]}")"
THIS_SCRIPT_FILE="${THIS_SCRIPT_FILE%%.*}"
COMMON_UTILS_SOURCE="${THIS_SCRIPT_DIR}/commonUtils.sh"

source ${COMMON_UTILS_SOURCE}

NOW=$(current_date_time)

RESOURCES_DIR=${THIS_SCRIPT_DIR}

LOG_DIR="${RESOURCES_DIR}/logs"
LOG_FILE_NAME="setupDockerEnv_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

IDG_VOLUME_DIR="${RESOURCES_DIR}/datapower/volumes*/*"

DOCUMENTATION_URL="https://bitbucket.org/montier/msc-dpod-simulator/src/master/"

#############
# Functions #
#############
function main() {
	echo_log_file_path

	parse_parameters "$@"
	remove_dockers
}

function parse_parameters() {
	while [[ -n "$1" ]]; do
		case "$1" in
		-h | --help)
			show_help
			exit 0
			;;
		*)
			shift 1
			;;
		esac
	done
}

function show_help() {
	echo_info $"Usage: $0 "
	echo_info "	Or :  $0 -h|--help"
	echo_info " "
	return 0
}

function remove_dockers() {
	local USER_RESPONSE=""

	echo_warn "This action will remove all docker components from this server."
	echo_warn "### Even non related containers to DPOD will be removed !!! ### "
	echo_warn "Please see 'dpodSimulator' documentation for more details : ${DOCUMENTATION_URL}"

	while [ ! "${USER_RESPONSE}" == "y" ] && [ ! "${USER_RESPONSE}" == "n" ]; do
		echo_info "Do you want to continue ? [y/n]: "
		read USER_RESPONSE
		echo_info " "
	done
	if [[ ${USER_RESPONSE} == "n" ]]; then
		echo_info "User aborted execution."
		exit 1
	else
		echo_info "User chose to continue script execution."
		echo_info "Deleting IDG volumes"
		rm -Rf ${IDG_VOLUME_DIR} >>${LOG_FILE} 2>&1

		echo_info "Checking for existing docker containers"
		NUM_OF_EXISTING_CONTAINERS=$(docker ps -a -q | wc -l)

		if [[ ${NUM_OF_EXISTING_CONTAINERS} -ne 0 ]]; then
			echo_info "Stopping all existing docker containers"
			docker stop $(docker ps -a -q) >>${LOG_FILE} 2>&1

			echo_info "Removing all existing docker containers"
			docker rm $(docker ps -a -q) >>${LOG_FILE} 2>&1
		else
			echo_info "There are no docker containers on the server"
		fi

		echo_info "Checking for existing docker images"
		NUM_OF_EXISTING_IMAGES=$(docker images -q | wc -l)

		if [[ ${NUM_OF_EXISTING_IMAGES} -ne 0 ]]; then
			echo_info "Removing all existing docker images"
			docker rmi $(docker images -q) -f >>${LOG_FILE} 2>&1
		else
			echo_info "There are no docker images on the server"
		fi

		echo_info "Removing all existing docker networks"
		docker network prune -f >>${LOG_FILE} 2>&1
	fi
}
#################
# Invoking main #
#################
main "$@"
