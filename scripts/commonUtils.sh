#!/bin/bash

#################################################################################################################
##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
#################################################################################################################

##########################
# Screen text color      #
##########################
NC='\033[0m' # No Color
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
RED="\033[0;31m"

##########################
# Logging                #
##########################
COMMON_UTILS_THIS_SCRIPT_BASENAME=""

function current_date_time() {
    date +%F_%H-%M-%S # Mac doesn't support printf for generating time strings
}

function log() {
    [[ "$#" -ne 2 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local PRIORITY="$1"
    local MESSAGE="$2"

    if [[ -z "${LOG_FILE}" ]]; then
        # No log file defined, don't log
        return
    fi

    if [[ -z "${COMMON_UTILS_THIS_SCRIPT_BASENAME}" ]]; then
        # Initialize and keep value so we don't execute this for every single line of log
        COMMON_UTILS_THIS_SCRIPT_BASENAME=$(basename $0)
    fi

    echo -e "$(current_date_time) ${COMMON_UTILS_THIS_SCRIPT_BASENAME}   ${PRIORITY} ${MESSAGE}" >>${LOG_FILE}
}

function debug() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    log DEBUG "$1"
}

function info() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    log INFO "$1"
}

function warn() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    log WARN "$1"
}

function error() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    log ERROR "$1"
}

function echo_debug() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local MSG="$1"
    debug "${MSG}"
    echo -e "$(current_date_time) DEBUG ${MSG}"
}

function echo_info() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local MSG="$1"
    info "${MSG}"
    echo -e "$(current_date_time) INFO  ${MSG}"
}

function echo_info_no_new_line() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local MSG="$1"
    info "${MSG}"
    echo -e -n "$(current_date_time) INFO  ${MSG}"
}

function echo_warn() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local MSG=$1
    warn "${MSG}"
    echo -e "$(current_date_time) WARN  ${MSG}"
}

function echo_error() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local MSG=$1
    error "${MSG}"
    echo -e "$(current_date_time) ERROR ${MSG}"
}

function echo_log_file_path() {
    echo "$(current_date_time) INFO  Log file is ${LOG_FILE}"
    local LOG_DIR="$(dirname "${LOG_FILE}")"
    mkdir -p ${LOG_DIR}
}

function read_info() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local MSG="$1"
    info "${MSG}"
    echo -e -n "$(current_date_time) INFO ${MSG}"
}

##########################
# Error handling         #
##########################
function on_error() {
    local LINE_NO="$1"
    echo_error "Error occurred on line $LINE_NO. Aborting."
    if [[ -n "${LOG_FILE}" ]]; then
        echo_error "Log file is ${LOG_FILE}"
    fi
    stop_print_progress
    exit 1
}

function catch_error() {
    [[ "$#" -ne 2 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local RETURN_CODE_TO_TEST="$1"
    local ERROR_MSG="$2"

    if [ ${RETURN_CODE_TO_TEST} -ne 0 ]; then
        echo_error "${ERROR_MSG}. Abort."
        ABORT=1
    fi

    stop_print_progress
}

function print_success_indication() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local RC="$1"

    if [[ ${RC} -eq 0 ]]; then
        echo_info "${GREEN}Action completed successfully!${NC} See detailed log file: ${LOG_FILE}"
        # info "Action completed successfully!"
        # echo -e "$(current_date_time) INFO  ${GREEN}Action completed successfully!${NC} See detailed log file: ${LOG_FILE} "
    else
        echo_info "${RED}Action failed!${NC} See detailed log file: ${LOG_FILE}"
        # error "Action failed"
        # echo -e "$(current_date_time) ERROR ${RED}Action failed!${NC} See detailed log file: ${LOG_FILE} "
    fi
}

###########################
# General                 #
###########################
function trim() {
    [[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local VALUE="$1"

    VALUE="${VALUE#"${VALUE%%[![:space:]]*}"}" # Remove spaces before
    VALUE="${VALUE%"${VALUE##*[![:space:]]}"}" # Remove spaces after
    printf '%s' "${VALUE}"
}

function lowercase() {
    echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}

###########################
# Progress                #
###########################
COMMON_UTILS_PRINT_PROGRESS_FLAG_FILE_PREFIX="/tmp/printProgressFlagFile.flag"

function start_print_progress() {
    local PROCESS_ID="$$"
    local PRINT_PROGRESS_ID="$(date +%s).${RANDOM}" # Mac does not support nanoseconds so we add a random number to the seconds from epoch
    echo "${PRINT_PROGRESS_ID}" >"${COMMON_UTILS_PRINT_PROGRESS_FLAG_FILE_PREFIX}.${PROCESS_ID}"
    print_progress "${PROCESS_ID}" "${PRINT_PROGRESS_ID}" &
}

function print_progress() {
    [[ "$#" -ne 2 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local PROCESS_ID="$1"
    local PRINT_PROGRESS_ID="$2"

    local SHOULD_PRINT_PROGRESS=1
    local SPINNER=("." ".." "...")
    while [[ "${SHOULD_PRINT_PROGRESS}" -eq 1 ]]; do
        local T_IN_SEC=$(printf '%(%s)T' -1)
        T_IN_SEC=$((T_IN_SEC % ${#SPINNER[@]}))
        echo -ne "$(current_date_time) ${SPINNER[${T_IN_SEC}]}    \r"
        sleep 1
        should_print_progress "${PROCESS_ID}" "${PRINT_PROGRESS_ID}" || SHOULD_PRINT_PROGRESS=0
    done
}

function should_print_progress() {
    [[ "$#" -ne 2 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local PROCESS_ID="$1"
    local PRINT_PROGRESS_ID="$2"

    # Check that the flag file still exists
    if [[ ! -f "${COMMON_UTILS_PRINT_PROGRESS_FLAG_FILE_PREFIX}.${PROCESS_ID}" ]]; then
        return 1
    fi

    # Check that the content of the flag file is still the same ID
    # When starting and stopping the print progress rapidly, there can be several print_progress processes sleeping and waiting for next round, but only one should print
    FLAG_FILE_CONTENT=$(cat "${COMMON_UTILS_PRINT_PROGRESS_FLAG_FILE_PREFIX}.${PROCESS_ID}" 2>/dev/null) || true # Let it fail if file was deleted
    FLAG_FILE_CONTENT=$(trim "${FLAG_FILE_CONTENT}")                                                             # Trim spaces
    if [[ "${FLAG_FILE_CONTENT}" != "${PRINT_PROGRESS_ID}" ]]; then
        return 1
    fi

    # Check that the original process is still running
    # This will stop the print progress when stopping the parent process (e.g. by pressing Ctrl+C)
    local PROCESS_EXISTS=1
    ps -p "${PROCESS_ID}" >/dev/null 2>&1 || PROCESS_EXISTS=0
    if [[ "${PROCESS_EXISTS}" -eq 0 ]]; then
        return 1
    fi

    return 0
}

function stop_print_progress() {
    local PROCESS_ID="$$"
    if [[ -f "${COMMON_UTILS_PRINT_PROGRESS_FLAG_FILE_PREFIX}.${PROCESS_ID}" ]]; then
        rm -f "${COMMON_UTILS_PRINT_PROGRESS_FLAG_FILE_PREFIX}.${PROCESS_ID}" >/dev/null 2>&1
    fi
}

###########################
# Download                #
###########################
function download_item() {
    [[ "$#" -ne 3 ]] && [[ "$#" -ne 5 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local ITEM_NAME="$1"
    local FILE_PATH="$2"
    local URL="$3"

    local OS=""
    [[ "$#" -ge 4 ]] && OS="$4"

    local MAX_TRIES=5
    [[ "$#" -ge 5 ]] && MAX_TRIES="$5"

    local SLEEP_INTERVAL=5
    local TIMEOUT_FLAG=15
    local RETRY_TIMES=1

    # wget is preffered over curl because it fails with rc!=0 when file is not downloaded.
    # curl can in some cases not fail, even when file was not downloded, and http code needs to be checked.

    # echo_info "Downloading ${ITEM_NAME} from ${URL}..."
    echo_info "Downloading ${ITEM_NAME}..."
    local IS_SUCCESS=1

    start_print_progress
    if [[ "Darwin" = "${COMMON_OS}" ]]; then
        curl -s -L -o ${FILE_PATH} ${URL} || IS_SUCCESS=0
    else
        wget --no-check-certificate --timeout=${TIMEOUT_FLAG} ${URL} -O ${FILE_PATH} -q || IS_SUCCESS=0
    fi
    stop_print_progress

    while [[ ${IS_SUCCESS} -eq 0 && ${RETRY_TIMES} -lt ${MAX_TRIES} ]]; do
        RETRY_TIMES=$((RETRY_TIMES + 1))
        echo_warn "Download failed, sleeping for ${SLEEP_INTERVAL} seconds before retrying..."
        sleep ${SLEEP_INTERVAL}
        echo_info "Retrying to download ${ITEM_NAME}, retry number ${RETRY_TIMES}, max tries: ${MAX_TRIES}..."
        IS_SUCCESS=1
        start_print_progress
        if [[ "Darwin" = "${COMMON_OS}" ]]; then
            curl -s -L -o ${FILE_PATH} ${URL} || IS_SUCCESS=0
        else
            wget --no-check-certificate --timeout=${TIMEOUT_FLAG} ${URL} -O ${FILE_PATH} -q || IS_SUCCESS=0
        fi
        stop_print_progress
    done

    if [ ${IS_SUCCESS} -eq 1 ]; then
        echo_info "Downloaded ${ITEM_NAME} successfully to ${FILE_PATH}"
        return 0
    fi

    echo_error "Failed to download ${ITEM_NAME}"
    return 1
}

function test_md5_signature() {
    [[ "$#" -ne 2 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local FILE_PATH="$1"
    local MD5_PATH="$2"

    local TEST_MD5_SIGNATURE_RC=0

    if [[ ! -f ${FILE_PATH} ]]; then
        echo_info "File ${FILE_PATH} does not exist"
        return 1
    fi

    if [[ ! -f ${MD5_PATH} ]]; then
        echo_info "File ${MD5_PATH} does not exist"
        return 1
    fi

    echo_info "Comparing ${FILE_PATH} md5sum to ${MD5_PATH} content..."

    local MD5_FILE_CONTENT=$(<"${MD5_PATH}")
    MD5_FILE_CONTENT=$(trim "${MD5_FILE_CONTENT}") # Trim spaces

    local CALCULATED_MD5=""
    if [[ "Darwin" = "${COMMON_OS}" ]]; then
        CALCULATED_MD5=$(md5 ${FILE_PATH} | awk '{print $4}')
    else
        CALCULATED_MD5=$(md5sum ${FILE_PATH} | awk '{print $1}')
    fi

    if [ "${MD5_FILE_CONTENT}" != "${CALCULATED_MD5}" ]; then
        echo_info "The md5sum of ${FILE_PATH} does not match the content of ${MD5_PATH}"
        return 1
    fi

    echo_info "The md5sum of ${FILE_PATH} matches the content of ${MD5_PATH}"
    return 0
}

function download_with_md5_test() {
    [[ "$#" -ne 6 ]] && [[ "$#" -ne 7 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local MD5_LOCAL_FILE="$1"
    local MD5_URL="$2"
    local ARTIFACT_LOCAL_FILE="$3"
    local ARTIFACT_URL="$4"
    local ARTIFACT_NAME="$5"

    local OS=""
    [[ "$#" -ge 6 ]] && OS="$6"

    local MAX_TRIES=5
    [[ "$#" -ge 7 ]] && MAX_TRIES="$7"

    local IS_SUCCESS

    download_item "${ARTIFACT_NAME} md5" "${MD5_LOCAL_FILE}" "${MD5_URL}" "${COMMON_OS}" "${MAX_TRIES}"

    # Skip comparing local artifact with downloaded md5 if local file doesn't exist or force download flag is set
    if [[ -f ${ARTIFACT_LOCAL_FILE} ]]; then
        IS_SUCCESS=1
        test_md5_signature ${ARTIFACT_LOCAL_FILE} ${MD5_LOCAL_FILE} || IS_SUCCESS=0
        if [ "${IS_SUCCESS}" -eq 1 ]; then
            echo_info "Local ${ARTIFACT_NAME} will be used."
            return 0
        fi
    fi

    download_item "${ARTIFACT_NAME}" "${ARTIFACT_LOCAL_FILE}" "${ARTIFACT_URL}" "${COMMON_OS}" "${MAX_TRIES}"

    IS_SUCCESS=1
    test_md5_signature ${ARTIFACT_LOCAL_FILE} ${MD5_LOCAL_FILE} || IS_SUCCESS=0
    if [ "${IS_SUCCESS}" -eq 1 ]; then
        echo_info "Downloaded ${ARTIFACT_NAME} will be used."
        return 0
    else
        return 1
    fi
}

###########################
# OS type                 #
###########################
COMMON_OS=""
COMMON_OS_ID=""
COMMON_OS_ID_LIKE=""

function discover_os_type() {
    COMMON_OS="$(uname)"
    if [ "${COMMON_OS}" = "Linux" ]; then
        COMMON_OS_ID=$(grep "^ID=" /etc/os-release | awk -F'=' '{print $2}')
        COMMON_OS_ID="${COMMON_OS_ID%\"}" # Removing double quotes suffix (if exists)
        COMMON_OS_ID="${COMMON_OS_ID#\"}" # Removing double quotes prefix (if exists)
        COMMON_OS_ID_LIKE=$(grep "^ID_LIKE=" /etc/os-release | awk -F'=' '{print $2}')
        COMMON_OS_ID_LIKE="${COMMON_OS_ID_LIKE%\"}" # Removing double quotes suffix (if exists)
        COMMON_OS_ID_LIKE="${COMMON_OS_ID_LIKE#\"}" # Removing double quotes prefix (if exists)
    fi
    info "Discovered OS: OS=${COMMON_OS}, OS_ID=${COMMON_OS_ID}, OS_ID_LIKE=${COMMON_OS_ID_LIKE}"
}
