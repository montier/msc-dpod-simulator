#!/bin/bash

#################################################################################################################
##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
#################################################################################################################

# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace
#set -o pipefail
set -o nounset
# set -x

###########################
# Variables and Constants #
###########################

THIS_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
THIS_SCRIPT_DIR_NAME=$(basename "${THIS_SCRIPT_DIR}")
THIS_SCRIPT_FILE="$(basename -- "${BASH_SOURCE[0]}")"
THIS_SCRIPT_FILE="${THIS_SCRIPT_FILE%%.*}"
COMMON_UTILS_SOURCE="${THIS_SCRIPT_DIR}/commonUtils.sh"

source ${COMMON_UTILS_SOURCE}

NOW=$(current_date_time)

RESOURCES_DIR="$(cd ${THIS_SCRIPT_DIR}/../../../resources >/dev/null && pwd)"
SCR_BUILD_DIR="${THIS_SCRIPT_DIR}/../build"
RESOURCES_BUILD_DIR="${RESOURCES_DIR}/build"
RESOURCES_DOWNLOADS_DIR="${RESOURCES_DIR}/downloads"

LOG_DIR="${RESOURCES_DIR}/logs"
LOG_FILE_NAME="dpodSimulator_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

source ${THIS_SCRIPT_DIR}/buildProperties.sh

LOCAL_DPOD_MD5_FILE="${RESOURCES_DOWNLOADS_DIR}/dpod.md5"
LOCAL_DPOD_IMAGE_FILE="${RESOURCES_DOWNLOADS_DIR}/dpod.tgz"
RESOURCES_MD5_FILE="${RESOURCES_DOWNLOADS_DIR}/app_resources.md5"
RESOUCES_FILE="${RESOURCES_DOWNLOADS_DIR}/app_resources.tgz"

STOP_TIMEOUT="60"
WAIT_BETWEEN_SERVICE_TESTS=15
MAX_SERVICE_TESTS=100
MAX_UI_WAIT_SECONDS=$((WAIT_BETWEEN_SERVICE_TESTS * MAX_SERVICE_TESTS))
SEC_TO_WAIT_AFTER_DEVICE_ADD="60"
SED_COMMAND=""
WAIT_BETWEEN_PAYLOAD_CAPTURE_REST_CALLS=5

# Parameters for idg setup in dpod
DPOD_DEFAULT_USER='admin'
DPOD_DEFAULT_PASS='adminuser'
DPOD_SIMULATOR_API_USER='SimulatorApi'
DPOD_SIMULATOR_API_PASS_ESC='Wt\$PORb3\&j8\&zpAr\$37g'
DPOD_SIMULATOR_API_PASS="${DPOD_SIMULATOR_API_PASS_ESC//\\/}"
DPOD_ADDRESS="172.77.77.4"
SOMA_PORT="5550"
ROMA_PORT="5554"
SOMA_USER='admin'
SOMA_PASS='admin'
LOG_TAR_HOST_ALIAS_TYPE='HOST_ALIAS'
LOG_TAR_HOST_ALIAS_ADDRESS='eth0_ipv4_1'
SYS_AGNT_NAME='MonTier-SyslogAgent-1'
SETUP_SYS_AGNT='true'
AUTO_SETUP_SYS_AGNT_NAME='MonTier-SyslogAgent-1'
WSM_AGNT_NAME='MonTier-WsmAgent-1'
AUTO_SETUP_WSM_AGNT_NAME='MonTier-WsmAgent-1'
SETUP_WSM='true'
SETUP_CERT_MON='true'
AUTO_SETUP_PATTERN='*'
DOMAIN_ANLYSIS_LEVEL='DEFAULT'
AUTO_SETUP_ANALYSYS_LEVEL='DEFAULT'

DPOD_VOLUMES_DIR="${RESOURCES_BUILD_DIR}/dpod/volumes"
MONTIER_SCRIPTS_PATH="/MonTier/scripts"
MONTIER_SCRIPTS_LOG_PATH="${MONTIER_SCRIPTS_PATH}/logs"
TRANSACTIONS_ACTIVATION_LOG_FILE="${MONTIER_SCRIPTS_LOG_PATH}/dpodSimulatorTransActivation.log"
TRANSACTIONS_ACTIVATION_SCRIPT="${MONTIER_SCRIPTS_PATH}/ActivateAllTransactions.sh"
POPULATE_DATA_DIR="${MONTIER_SCRIPTS_PATH}/PopulateData"
PAYLOAD_CAPTURE_DIR="${MONTIER_SCRIPTS_PATH}/PayloadCapture"

IDG_NAME_PREFIX="datapower-gateway-"
INITIAL_IDG_ADREESS_LAST_OCTET="10"

INITIAL_IDG_ADREESS_SEGMENT="172.77.77"
IDG_INITIAL_VOLUMES_PATH="${RESOURCES_BUILD_DIR}/datapower"
IDG_INITIAL_VOLUME_TEMPLATE_DIR="${IDG_INITIAL_VOLUMES_PATH}/volumes_idg_template"
IDG_VOLUME_DIR_PREFIX="${IDG_INITIAL_VOLUMES_PATH}/volumes_idg"

SPLUNK_SCRIPTS_DIR="${RESOURCES_BUILD_DIR}/splunk/volumes_splunk/scripts"
SPLUNK_INITIAL_CONF="${RESOURCES_BUILD_DIR}/splunk/volumes_splunk/*"
SPLUNK_VOLUME_DIR="${RESOURCES_BUILD_DIR}/splunk/volumes_splunk"
SPLUNK_DPOD_TRAN_VIEW_FILE="${SPLUNK_VOLUME_DIR}/config/dpod.app/local/data/ui/views/dpod_transactions_list.xml"
SPLUNK_OPERATIONS_SCRIPT="${MONTIER_SCRIPTS_PATH}/activateSplunkOperations.sh"
SLEEP_BETWEEN_SPLUNK_RESTARTS="86400"
SPLUNK_OPERATIONS_ACTIVATION_LOG_FILE="${MONTIER_SCRIPTS_LOG_PATH}/activateSplunkOperations.log"

COMPOSE_FILE_PATH="${RESOURCES_BUILD_DIR}/docker-compose.yml"
# This is because the default value (60) is sometimes not enough for 'docker-compose down' command and it times-out.
export COMPOSE_HTTP_TIMEOUT=300

CLEAN_SIMULATOR_TIMEOUT="120"

DOCKER_SERVICE_TO_START=generator
ADDITIONAL_IDG_FLAG_UP=0
SPLUNK_FLAG_UP=0
NETWORK_INTERFACE_FLAG_UP=0
EXTERNAL_SCRIPT_EXECUTION_FLAG_UP=0
DPOD_PORT_FLAG_UP=0

# User defined parameters
ACTION=""
NETWORK_INTERFACE_PARAMETER=""
EXTERNAL_SCRIPT_NAME=""
EXTERNAL_SCRIPT_SRC_PATH=""
NUMBER_OF_IDG_SERVICES_TO_HANDLE=1
DPOD_PORT=""

MIN_MEMORY_SIZE="10"
MIN_CPU_NO="8"
NEDDED_DISK_FOR_DOCKER_GB="25"
NEDDED_DISK_FOR_RESOURCES_GB="2"

GENERATOR_CONTAINER="generator-dpodsimulator"
DPOD_CONTAINER="dpod-dpodsimulator"
SPLUNK_CONTAINER="splunk-dpodsimulator"
MQ_CONTAINER="mq-dpodsimulator"

CHOSEN_HOST_ADDRESS="<hostIp>"
USER_INPUT_FILE_NAME="userInputBuildProperties.sh"
USER_INPUT_FILE_TEMPLATE="${THIS_SCRIPT_DIR}/${USER_INPUT_FILE_NAME}"
USER_INPUT_FILE="${RESOURCES_BUILD_DIR}/${USER_INPUT_FILE_NAME}"

INSTALL_CONTAINER_TOOLS_SCRIPT="${MONTIER_SCRIPTS_PATH}/InstallPrereq.sh"
DPOD_SSH_PASS="dpod"

DPOD_HOSTNAME=""
DPOD_HOSTANAME_FLAG_UP=0

WEB_CONF_FLAG_UP=0
WEB_CONF_SOURCE_FILE_NAME="web.conf"
WEB_CONF_DST_PATH="${SPLUNK_VOLUME_DIR}/config/system-prefs/${WEB_CONF_SOURCE_FILE_NAME}"
WEB_CONF_SRC_PATH=""

#############
# Functions #
#############
function main() {
	source ${COMMON_UTILS_SOURCE}

	echo_log_file_path
	parse_parameters "$@"

	echo_info ""
	echo_info "Welcome to the DPOD Simulator"
	echo_info "============================="

	if [[ ${ACTION} == "start" ]] || [[ ${ACTION} == "stop" ]]; then
		read_user_input_from_file
	fi

	discover_os_type

	if [[ "Darwin" = "${COMMON_OS}" ]]; then
		SED_COMMAND="sed -i ''"
	elif [[ "Linux" = "${COMMON_OS}" ]]; then
		SED_COMMAND="sed -i"
	else
		echo_error "OS type ${COMMON_OS} is not supported."
		echo_error "Supported OS types are: Rocky Linux, RHEL, Ubuntu and MacOS."
		return 1
	fi

	print_user_parameters

	echo_info "Enabling sudo for user ${USER}..."
	sudo touch /tmp/enable_sudo

	if [[ ${ACTION} == "build" ]]; then
		build_simulator
	elif [[ ${ACTION} == "start" ]]; then
		start_simulator
	elif [[ ${ACTION} == "stop" ]]; then
		stop_simulator
	elif [[ ${ACTION} == "clean" ]]; then
		clean_simulator
	fi
}

function parse_parameters() {
	while [[ $# -gt 0 ]]; do
		case "$1" in
		-h | --help)
			show_help
			exit 0
			;;
		-a | --action)
			ACTION=$2
			if [[ -z ${ACTION} ]]; then
				echo_error "Invalid parameters. Action is missing."
				show_help
				exit 1
			fi
			shift 2
			;;
		-i | --additional-idg)
			ADDITIONAL_IDG_FLAG_UP=1
			NUMBER_OF_IDG_SERVICES_TO_HANDLE=2
			if [[ $# -gt 1 ]]; then
				# Check that the next parameter is the number of idgs to handle or a flag
				local NUMERIC_REGEX='^[0-9]+$'
				if ! [[ $2 =~ ${NUMERIC_REGEX} ]]; then
					shift
				else
					NUMBER_OF_IDG_SERVICES_TO_HANDLE=$2
					shift 2
				fi
			else
				shift
			fi
			;;
		-k | --add-splunk)
			SPLUNK_FLAG_UP=1
			shift 1
			;;
		-d | --dpod-host-address)
			DPOD_HOSTANAME_FLAG_UP=1
			DPOD_HOSTNAME=$2
			if [[ -z ${DPOD_HOSTNAME} ]]; then
				echo_error "Invalid parameters. DPOD host address is missing."
				show_help
				exit 1
			fi
			shift 2
			;;
		-p | --dpod-port)
			DPOD_PORT_FLAG_UP=1
			DPOD_PORT=$2
			if [[ -z ${DPOD_PORT} ]]; then
				echo_error "Invalid parameters. DPOD port is missing."
				show_help
				exit 1
			fi
			shift 2
			;;
		-w | --web-conf)
			WEB_CONF_FLAG_UP=1
			WEB_CONF_SRC_PATH=$2
			if [[ -z ${WEB_CONF_SRC_PATH} ]]; then
				echo_error "Invalid parameters. Web.conf path is missing."
				show_help
				exit 1
			fi
			shift 2
			;;
		-n | --network-interface)
			NETWORK_INTERFACE_FLAG_UP=1
			NETWORK_INTERFACE_PARAMETER=$2
			if [[ -z ${NETWORK_INTERFACE_PARAMETER} ]]; then
				echo_error "Invalid parameters. Network interface is missing."
				show_help
				exit 1
			fi
			shift 2
			;;
		-s | --script-execution-path)
			EXTERNAL_SCRIPT_EXECUTION_FLAG_UP=1
			EXTERNAL_SCRIPT_SRC_PATH=$2
			if [[ -z ${EXTERNAL_SCRIPT_SRC_PATH} ]]; then
				echo_error "Invalid parameters. Script path is missing."
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo_error "Invalid parameter : $1"
			show_help
			exit 1
			;;
		esac
	done

	if [[ -z ${ACTION} ]]; then
		echo_error "Invalid parameters. Mandatory Parameter '--action' is missing."
		exit 2
	fi

	if [[ ! ${ACTION} == "build" ]] && [[ ! ${ACTION} == "start" ]] && [[ ! ${ACTION} == "stop" ]] && [[ ! ${ACTION} == "clean" ]]; then
		echo_error "Invalid parameters. Mandatory parameter '--action' value '${ACTION}' is not valid."
		exit 2
	fi

	if [[ ${ADDITIONAL_IDG_FLAG_UP} -eq 1 ]] && [[ ! ${ACTION} == "build" ]]; then
		echo_error "Invalid parameters. Optional parameter '-i' is valid only for the 'build' action."
		exit 2
	fi

	if [[ ${NUMBER_OF_IDG_SERVICES_TO_HANDLE} -gt 5 ]]; then
		echo_error "Invalid parameters. IDGs count '${NUMBER_OF_IDG_SERVICES_TO_HANDLE}' is not valid. Max value is 5."
		exit 2
	fi

	if [[ ${SPLUNK_FLAG_UP} -eq 1 ]] && [[ ! ${ACTION} == "build" ]]; then
		echo_error "Invalid parameters. Optional parameter '-k' is valid only for the 'build' action."
		exit 2
	fi

	if [[ -z ${DPOD_PORT} ]]; then
		DPOD_PORT="4433"
	fi

	if [[ ${EXTERNAL_SCRIPT_EXECUTION_FLAG_UP} -eq 1 ]]; then
		if [[ ! -f ${EXTERNAL_SCRIPT_SRC_PATH} ]]; then
			echo_error "External script files do not exist. Please check the script path: ${EXTERNAL_SCRIPT_SRC_PATH}"
			exit 1
		fi
		EXTERNAL_SCRIPT_NAME="$(basename -- "${EXTERNAL_SCRIPT_SRC_PATH}")"
		EXTERNAL_SCRIPT_NAME="${EXTERNAL_SCRIPT_NAME%%.*}"
		EXTERNAL_SCRIPT_SRC_PATH="${EXTERNAL_SCRIPT_SRC_PATH%%.*}"
	fi
}

function show_help() {
	echo_info $"Usage: $0 -a|--action (action) [-i|--additional-idg] [-k|--add-splunk]"
	echo_info "    Or: $0 -h|--help"
	echo_info " "
	echo_info "       -a|--action: The action to take. This is a mandatory parameter that must contain one of the following values:"
	echo_info "                build    - This operation builds all simulator containers"
	echo_info "                stop     - This operation stops all containers, but keeps the images ready for rerunning"
	echo_info "                start    - This operation starts all containers and simulates traffic on IDG and DPOD, using the flags previously set in the build action"
	echo_info "                clean    - This operation stops all simulator containers, removes them, and removes all simulator images and networks from the Docker configuration"
	echo_info " "
	echo_info "       -i|--additional-idg : If this flag if raised, aditional IDGs would be created."
	echo_info "       		   If no number given - there will created one additional IDG."
	echo_info "       		   Max number of IDG that can be created is 5."
	echo_info "                This is an optional parameter. Parameter is valid only for 'build' action."
	echo_info " "
	echo_info "       -k|--add-splunk: If this flag if raised, a splunk container will be created."
	echo_info "                This is an optional parameter. Parameter is valid only for 'build' action."
	echo_info " "
	echo_info "       -d|--dpod-host-address: DPOD hostname."
	echo_info "                This is an optional parameter. Parameter is valid only for 'build' action."
	echo_info " "
	echo_info "       -p|--dpod-port: DPOD custom port."
	echo_info "                This is an optional parameter. Parameter is valid only for 'build' action."
	echo_info " "
	echo_info "       -w|--web-conf: Splunk web.conf file."
	echo_info "                This is an optional parameter. Parameter is valid only for 'build' action."
	echo_info " "
	echo_info "       -n|--network-interface: The address of the network interface that will be used to connect to the Operations Dashboard Web UI"
	echo_info "                This is an optional parameter. Parameter is valid only for 'build' action."
	echo_info "			      If this parameter is not given and there are multiple interfaces on the host, the simulator will present the user with the available interfaces and user will choose one."
	echo_info " "
	echo_info "       -s | --script-execution-path: The path of an external script to execute."
	echo_info "                The script will be executed several times (once for each step as descibed below) from the Generator container."
	echo_info "                The external script will receive the following parameters:"
	echo_info "                   -s: Simulator step. This parameter contains one of the following values:"
	echo_info "                       before_adding_gateways: DPOD is up and running but the gateways have not been added and set up."
	echo_info "                       simulator_ready: The simulator environment is up and running and ready to be used."
	echo_info "                   -a: DPOD container IP address."
	echo_info "                   -u: DPOD simulator user name (useful for invoking REST API)."
	echo_info "                   -p: DPOD simulator password (useful for invoking REST API)."
	echo_info "                This is an optional parameter."
	echo_info " "
	echo_info "       -h|--help: Display help"
	echo_info " "
	echo_info "For Example: "
	echo_info "       $0 -a build -i -k"
	echo_info "   Or : "
	echo_info "       $0 -a start"
}

function read_user_input_from_file() {
	if [[ ! -f ${USER_INPUT_FILE} ]]; then
		echo_error "The DPOD simulator was never built on this host."
		echo_error "To start the DPOD Simulator, please use the 'build' action first."
		exit 1
	fi

	source ${USER_INPUT_FILE}
}

function print_user_parameters() {
	echo_info "The Simulator will use the following parameters:"
	echo_info "   Action: ${ACTION}"
	echo_info "   Number of DataPower gateways: ${NUMBER_OF_IDG_SERVICES_TO_HANDLE}"

	if [[ ! -z ${DPOD_HOSTNAME} ]]; then
		echo_info "   DPOD hostname: ${DPOD_HOSTNAME}"
	fi

	if [[ ! -z ${DPOD_PORT} ]]; then
		echo_info "   DPOD port: ${DPOD_PORT}"
	fi

	if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
		echo_info "   Launch Splunk: Yes"
	fi

	if [[ ${WEB_CONF_FLAG_UP} -eq 1 ]]; then
		echo_info "   Splunk web.conf source file: ${WEB_CONF_SRC_PATH}"
	fi

	if [[ ${NETWORK_INTERFACE_FLAG_UP} -eq 1 ]]; then
		echo_info "   Network interface: ${NETWORK_INTERFACE_PARAMETER}"
	fi

	if [[ ${EXTERNAL_SCRIPT_EXECUTION_FLAG_UP} -eq 1 ]]; then
		echo_info "   External script file(s) path: ${EXTERNAL_SCRIPT_SRC_PATH}"
	fi
}

function build_simulator() {
	check_previous_build
	clean_simulator
	verify_hardware_requirements

	# echo_info "Enabling ipv4 forwarding..."
	# if [[ "Darwin" = "${COMMON_OS}" ]]; then
	# 	sudo sysctl -w net.inet.ip.forwarding=1 >>${LOG_FILE} 2>&1
	# else
	# 	sudo sysctl -w net.ipv4.ip_forward=1 >>${LOG_FILE} 2>&1
	# fi

	if [[ ${NETWORK_INTERFACE_FLAG_UP} -eq 1 ]]; then
		CHOSEN_HOST_ADDRESS=${NETWORK_INTERFACE_PARAMETER}
	else
		choose_host_address
	fi
	echo_info "Using IP address ${CHOSEN_HOST_ADDRESS} for binding the Simulator services."

	if [[ -z ${DPOD_HOSTNAME} ]]; then
		DPOD_HOSTNAME=${CHOSEN_HOST_ADDRESS}
	fi

	echo_info ""
	echo_info "Building the DPOD Simulator may take up to an hour. Please make sure you have a fast and steady internet connection."

	handle_resources

	echo_info "Loading DPOD image to docker..."
	start_print_progress
	export DPOD_IMAGE_TAG="$(sudo -E docker load <${LOCAL_DPOD_IMAGE_FILE} | grep '^Loaded image: ' | cut -c15-)"
	stop_print_progress

	start_all_services "${DPOD_DEFAULT_USER}" "${DPOD_DEFAULT_PASS}" "${CHOSEN_HOST_ADDRESS}"

	setup_simulator_users_and_roles
	add_all_idg_to_dpod
	run_transactions_on_all_idg
	populate_data
	activate_payload_capture

	echo_info "Running timesync tasks..."
	sudo -E docker exec -i ${GENERATOR_CONTAINER} crond >>${LOG_FILE} 2>&1
	start_splunk_operation

	execute_exeternal_script "simulator_ready"

	save_user_input_to_file
	show_links
}

function start_simulator() {
	echo_info "Using IP address ${CHOSEN_HOST_ADDRESS} for binding the Simulator services."

	if [[ -z ${DPOD_HOSTNAME} ]]; then
		DPOD_HOSTNAME=${CHOSEN_HOST_ADDRESS}
	fi

	start_all_services "${DPOD_SIMULATOR_API_USER}" "${DPOD_SIMULATOR_API_PASS}" "${CHOSEN_HOST_ADDRESS}"

	populate_data
	run_transactions_on_all_idg
	activate_payload_capture

	echo_info "Running timesync tasks..."
	sudo -E docker exec -i ${GENERATOR_CONTAINER} crond >>${LOG_FILE} 2>&1
	start_splunk_operation

	execute_exeternal_script "simulator_ready"

	show_links
}

function stop_simulator() {
	echo_info "Stopping the DPOD Simulator..."
	start_print_progress
	sudo -E docker compose -f ${COMPOSE_FILE_PATH} stop -t ${STOP_TIMEOUT} >>${LOG_FILE} 2>&1
	stop_print_progress
	echo_info "The DPOD Simulator stopped successfully."
}

function check_previous_build() {
	local USER_RESPONSE="noreply"

	if [[ -f ${USER_INPUT_FILE} ]]; then
		echo_warn ""
		echo_warn "The DPOD Simulator has already been built on this host."
		echo_warn "A new 'build' will remove all Simulator-related objects, and build a new environment."

		while [[ ! "${USER_RESPONSE}" == "y" ]] && [[ ! "${USER_RESPONSE}" == "n" ]]; do
			echo_info_no_new_line "Do you want to continue? [y/n]: "
			read USER_RESPONSE
		done

		if [[ "${USER_RESPONSE}" == "n" ]]; then
			exit 1
		fi
	fi
}

function clean_simulator() {
	local OBJECTS_NOT_REMOVED=0
	local IS_SUCCESS=1

	echo_info "Stopping and cleaning the Simulator containers and images..."
	start_print_progress
	sudo -E docker compose -f ${COMPOSE_FILE_PATH} down --rmi all -v -t ${CLEAN_SIMULATOR_TIMEOUT} >>${LOG_FILE} 2>&1 || IS_SUCCESS=0
	stop_print_progress

	manual_clean_simulator

	echo_info "Removing old build environment..."
	sudo rm -Rf ${RESOURCES_BUILD_DIR} >>${LOG_FILE} 2>&1
}

function manual_clean_simulator() {
	echo_info "Removing existing Simulator Docker resources:"
	local MANUAL_CLEAN_SIMULATOR_SUCCESS=1

	echo_info "   Checking for existing 'dpodsimulator' Docker containers..."
	local NUM_OF_EXISTING_CONTAINERS=$(sudo -E docker ps -a -f name=dpodsimulator -q | wc -l)

	if [[ ${NUM_OF_EXISTING_CONTAINERS} -ne 0 ]]; then
		echo_info "   Stopping existing 'dpodsimulator' Docker containers..."
		start_print_progress
		sudo -E docker stop $(sudo -E docker ps -a -f name=dpodsimulator -q) >>${LOG_FILE} 2>&1 || MANUAL_CLEAN_SIMULATOR_SUCCESS=0
		stop_print_progress

		echo_info "   Removing existing 'dpodsimulator' Docker containers..."
		sudo -E docker rm -v -f $(sudo -E docker ps -a -f name=dpodsimulator -q) >>${LOG_FILE} 2>&1 || MANUAL_CLEAN_SIMULATOR_SUCCESS=0
	else
		echo_info "   There are no existing 'dpodsimulator' Docker containers."
	fi

	echo_info "   Checking for existing 'dpodsimulator' Docker images..."
	local NUM_OF_EXISTING_IMAGES=$(sudo -E docker images -f reference="*:dpodsimulator" -q | wc -l)

	if [[ ${NUM_OF_EXISTING_IMAGES} -ne 0 ]]; then
		echo_info "   Removing existing 'dpodsimulator' Docker images..."
		sudo -E docker rmi -f $(sudo -E docker images -f reference="*:dpodsimulator" -q) >>${LOG_FILE} 2>&1 || MANUAL_CLEAN_SIMULATOR_SUCCESS=0
	else
		echo_info "   There are no existing 'dpodsimulator' Docker images."
	fi

	echo_info "   Checking for existing 'dpod-developer-edition' Docker images..."
	local NUM_OF_EXISTING_DPOD_IMAGES=$(sudo -E docker images -f reference="*dpod-developer-edition" -q | wc -l)

	if [[ ${NUM_OF_EXISTING_DPOD_IMAGES} -ne 0 ]]; then
		echo_info "   Removing existing 'dpod-developer-edition' Docker images..."
		sudo -E docker rmi -f $(sudo -E docker images -f reference="dpod-developer-edition" -q) >>${LOG_FILE} 2>&1 || MANUAL_CLEAN_SIMULATOR_SUCCESS=0
	else
		echo_info "   There are no existing 'dpod-developer-edition' Docker images."
	fi

	echo_info "   Checking for existing 'dpodsimulator_net' Docker network..."
	local NUM_OF_EXISTING_NETWOKRS=$(sudo -E docker network ls | grep dpodsimulator_net | wc -l)

	if [[ ${NUM_OF_EXISTING_NETWOKRS} -ne 0 ]]; then
		echo_info "   Removing existing 'dpodsimulator_net' Docker network..."
		sudo -E docker network rm dpodsimulator_net >>${LOG_FILE} 2>&1 || MANUAL_CLEAN_SIMULATOR_SUCCESS=0
	else
		echo_info "   There is no existing 'dpodsimulator_net' Docker network."
	fi

	if [[ ${MANUAL_CLEAN_SIMULATOR_SUCCESS} -eq 0 ]]; then
		echo_error "Failed to clean the existing Simulator Docker objects."
		echo_error "Please manually clean the Docker objects and run the 'build' action again."
		exit 1
	fi
}

function verify_hardware_requirements() {
	echo_info "Verifying hardware requirements:"

	local MEMORY_CHECK=1
	memory_check || MEMORY_CHECK=0
	if [[ ${MEMORY_CHECK} -eq 0 ]]; then
		ask_for_hw_req_user_override
	fi

	local CPU_CHECK=1
	cpu_check || CPU_CHECK=0
	if [[ ${CPU_CHECK} -eq 0 ]]; then
		ask_for_hw_req_user_override
	fi

	disk_space_check
}

function memory_check() {
	info "   Verifying amount of memory..."
	local MEMORY_CHECK_RC=0

	# Required memory is the minimun 10GB and additional 2GB for each idg
	local REQUIRED_MEMORY_SIZE=$((MIN_MEMORY_SIZE + (NUMBER_OF_IDG_SERVICES_TO_HANDLE * 2)))

	if [[ "Darwin" = "${COMMON_OS}" ]]; then
		# When setting the OS RAM on VM it can be decimal, so we increase the acctual RAM by 1 for check
		RAM=$(system_profiler SPHardwareDataType | grep "  Memory:" | awk -F': ' '{print $2}' | awk -F' GB' '{print $1}' | awk -F'.' '{print $1}')
		RAM=$((RAM + 1))
	else
		RAM=$(awk '{ printf "%.0f", $2/1024/1024+0.5 ; exit}' /proc/meminfo)
	fi

	if [[ ${RAM} -lt ${REQUIRED_MEMORY_SIZE} ]]; then
		echo_warn "   There is not enough memory, required memory is ${REQUIRED_MEMORY_SIZE}GB and found only ${RAM}GB. ${RED}Failed${NC}"
		MEMORY_CHECK_RC=1
	else
		echo_info "   There is enough memory, required memory is ${REQUIRED_MEMORY_SIZE}GB and found ${RAM}GB. ${GREEN}Passed${NC}"
		MEMORY_CHECK_RC=0

		# Check MacOS Docker resource limit
		# When setting the Docker resource limit to 16GB, it will actually report less (~15.64GB), so we increase the acctual RAM by 1 for check
		if [[ "Darwin" = "${COMMON_OS}" ]]; then
			info "   Verifying Docker Desktop total memory configuration..."
			MACOS_RESOURCE_LIMIT_MEM=$(sudo -E docker system info | grep "Total Memory" | awk -F': ' '{print $2}' | awk -F'G' '{print $1}')
			MACOS_RESOURCE_LIMIT_MEM_INT=$(sudo -E docker system info | grep "Total Memory" | awk -F': ' '{print $2}' | awk -F'G' '{print $1}' | awk -F'.' '{print $1}')
			MACOS_RESOURCE_LIMIT_MEM_INT=$((MACOS_RESOURCE_LIMIT_MEM_INT + 1))
			if [[ -z "${MACOS_RESOURCE_LIMIT_MEM}" ]]; then
				echo_warn "   Cannot get Docker Desktop total memory configuration, make sure Docker Desktop is running. ${RED}Failed${NC}"
				MEMORY_CHECK_RC=1
			elif [[ ${MACOS_RESOURCE_LIMIT_MEM_INT} -lt ${REQUIRED_MEMORY_SIZE} ]]; then
				echo_warn "   Docker Desktop total memory configuration is too low, found only ${MACOS_RESOURCE_LIMIT_MEM}GB. ${RED}Failed${NC}"
				MEMORY_CHECK_RC=1
			else
				echo_info "   Docker Desktop total memory configuration is valid, found ${MACOS_RESOURCE_LIMIT_MEM}GB. ${GREEN}Passed${NC}"
				MEMORY_CHECK_RC=0
			fi
		fi
	fi

	return ${MEMORY_CHECK_RC}
}

function cpu_check {
	info "   Verifying amount of CPU cores..."
	local CORES=$(getconf _NPROCESSORS_ONLN)
	local CPU_CHECK_RC=0

	if [[ ${CORES} -lt ${MIN_CPU_NO} ]]; then
		echo_warn "   There are not enough CPU cores, required CPU cores are ${MIN_CPU_NO} and found only ${CORES}. ${RED}Failed${NC}"
		CPU_CHECK_RC=1
	else
		echo_info "   There are enough CPU cores, required CPU cores are ${MIN_CPU_NO} and found ${CORES}. ${GREEN}Passed${NC}"
		CPU_CHECK_RC=0

		# check macOS docker resource limit
		if [[ "Darwin" = "${COMMON_OS}" ]]; then
			info "   Verifying Docker Desktop CPU cores configuration..."
			MACOS_RESOURCE_LIMIT_CPU=$(sudo -E docker system info | grep "CPUs" | awk -F': ' '{print $2}')
			if [[ -z "${MACOS_RESOURCE_LIMIT_CPU}" ]]; then
				echo_warn "   Cannot get Docker Desktop CPU cores configuration, make sure Docker Desktop is running. ${RED}Failed${NC}"
				CPU_CHECK_RC=1
			elif [[ ${MACOS_RESOURCE_LIMIT_CPU} -lt ${MIN_CPU_NO} ]]; then
				echo_warn "   Docker Desktop CPU cores configuration is too low, found only ${MACOS_RESOURCE_LIMIT_CPU}. ${RED}Failed${NC}"
				CPU_CHECK_RC=1
			else
				echo_info "   Docker Desktop CPU cores configuration is valid, found ${MACOS_RESOURCE_LIMIT_CPU}. ${GREEN}Passed${NC}"
				CPU_CHECK_RC=0
			fi
		fi
	fi

	return ${CPU_CHECK_RC}
}

function ask_for_hw_req_user_override() {
	local USER_RESPONSE="noreply"

	while [[ ! "${USER_RESPONSE}" == "y" ]] && [[ ! "${USER_RESPONSE}" == "n" ]]; do
		echo_info_no_new_line "Do you want to continue? [y/n]: "
		read USER_RESPONSE
	done

	if [[ ${USER_RESPONSE} == "n" ]]; then
		exit 1
	fi
}

function disk_space_check() {
	info "   Verifying available disk space..."

	local DOCKER_RUNTIME_DIR="/var/lib/docker"
	if [[ "Darwin" = "${COMMON_OS}" ]]; then
		DOCKER_RUNTIME_DIR="${HOME}/Library"
	fi

	local NEDDED_DISK_FOR_DOCKER_KB=$((1000000 * NEDDED_DISK_FOR_DOCKER_GB))
	AVAILABLE_DISK_FOR_DOCKER_KB=$(df -k ${DOCKER_RUNTIME_DIR} | grep -iv filesystem | awk '{print $4}')
	AVAILABLE_DISK_FOR_DOCKER=$(df -h ${DOCKER_RUNTIME_DIR} | grep -iv filesystem | awk '{print $4}')

	if [[ ${AVAILABLE_DISK_FOR_DOCKER_KB} -lt ${NEDDED_DISK_FOR_DOCKER_KB} ]]; then
		echo_error "   There is not enough disk space available in ${DOCKER_RUNTIME_DIR}, required ${NEDDED_DISK_FOR_DOCKER_GB}GB and found only ${AVAILABLE_DISK_FOR_DOCKER}. ${RED}Failed${NC}"
		exit 1
	else
		echo_info "   There is enough disk space available in ${DOCKER_RUNTIME_DIR}, required ${NEDDED_DISK_FOR_DOCKER_GB}GB and found ${AVAILABLE_DISK_FOR_DOCKER}. ${GREEN}Passed${NC}"
	fi

	local NEDDED_DISK_FOR_RESOURCES_KB=$((1000000 * NEDDED_DISK_FOR_RESOURCES_GB))
	AVAILABLE_DISK_FOR_RESOURCE_DIR_KB=$(df -k ${RESOURCES_DIR} | grep -iv filesystem | awk '{print $4}')
	AVAILABLE_DISK_FOR_RESOURCE_DIR=$(df -h ${RESOURCES_DIR} | grep -iv filesystem | awk '{print $4}')

	if [[ ${AVAILABLE_DISK_FOR_RESOURCE_DIR_KB} -lt ${NEDDED_DISK_FOR_RESOURCES_KB} ]]; then
		echo_error "   There is not enough disk space available in ${RESOURCES_DIR}, required ${NEDDED_DISK_FOR_RESOURCES_GB}GB and found only ${AVAILABLE_DISK_FOR_RESOURCE_DIR}. ${RED}Failed${NC}"
		exit 1
	else
		echo_info "   There is enough disk space available in ${RESOURCES_DIR}, required ${NEDDED_DISK_FOR_RESOURCES_GB}GB and found ${AVAILABLE_DISK_FOR_RESOURCE_DIR}. ${GREEN}Passed${NC}"
	fi
}

function choose_host_address() {
	local ADDRESS_LIST=""
	local DOCKER_SUBNET=0
	local FIXED_ADDRESS_LIST=""
	local USER_RESPONSE=""
	local USER_RESP_VALID=0
	local IP_ADDRESS_NO=0

	if [[ "Darwin" = "${COMMON_OS}" ]]; then
		ADDRESS_LIST=$(ifconfig | grep inet | grep -v inet6 | grep -v 127.0.0.1 | awk '{print $2}')
	else
		ADDRESS_LIST=$(hostname -I)
	fi

	echo_info "Detecting host IP addresses..."
	for HOST_ADDRESS_FROM_LIST in ${ADDRESS_LIST}; do
		if [[ ! ${HOST_ADDRESS_FROM_LIST} == 172.77.77* ]]; then
			if [[ "Darwin" = "${COMMON_OS}" ]]; then
				DOCKER_SUBNET=0
			else
				DOCKER_SUBNET=$(ip addr | grep ${HOST_ADDRESS_FROM_LIST} | grep docker0 | wc -l)
			fi

			if [[ ${DOCKER_SUBNET} -eq 0 ]]; then
				if [[ ${IP_ADDRESS_NO} -eq 0 ]]; then
					FIXED_ADDRESS_LIST="${HOST_ADDRESS_FROM_LIST}"
				else
					FIXED_ADDRESS_LIST="${FIXED_ADDRESS_LIST} ${HOST_ADDRESS_FROM_LIST}"
				fi
				IP_ADDRESS_NO=$((IP_ADDRESS_NO + 1))
			else
				info "Skipping address ${HOST_ADDRESS_FROM_LIST}. It is a docker internal network."
			fi
		fi
	done

	local SERVER_IP=""
	if [[ ${IP_ADDRESS_NO} -eq 1 ]]; then
		SERVER_IP=${FIXED_ADDRESS_LIST[0]}
		info "There is a single IP address configured on this host: ${SERVER_IP}"
	else
		while [[ (x"$USER_RESPONSE" != "xy" && x"$USER_RESPONSE" != "xY" && x"$USER_RESPONSE" != "xyes" && x"$USER_RESPONSE" != "xYES") ]]; do
			USER_RESP_VALID=0
			while [[ "$USER_RESP_VALID" = 0 ]]; do
				echo_info ""
				echo_info "Please select an IP address for binding the DPOD Simulator services (DPOD, DataPower gateways, Splunk)."
				echo_info "The current configured IPs on this host are: "
				for CURR_IP in ${FIXED_ADDRESS_LIST}; do
					echo_info "   ${CURR_IP}"
				done
				echo_info ""
				echo_info_no_new_line "Please enter the IP address for binding: "
				read SERVER_IP

				USER_RESP_VALID=0
				if [[ ! -z ${SERVER_IP} ]]; then
					if [[ " ${FIXED_ADDRESS_LIST[@]} " =~ " ${SERVER_IP} " ]]; then
						USER_RESP_VALID=1
					else
						echo_info "The entered IP address is not one of the configured IPs on this host."
					fi
				fi
			done

			echo_info ""
			echo_info "You selected the IP address: ${SERVER_IP}"

			USER_RESPONSE=""
			while [[ "$USER_RESPONSE" != "y" ]] && [[ "$USER_RESPONSE" != "Y" ]] &&
				[[ "$USER_RESPONSE" != "YES" ]] && [[ "$USER_RESPONSE" != "yes" ]] &&
				[[ "$USER_RESPONSE" != "n" ]] && [[ "$USER_RESPONSE" != "N" ]] &&
				[[ "$USER_RESPONSE" != "NO" ]] && [[ "$USER_RESPONSE" != "no" ]]; do
				echo_info_no_new_line "Is this correct? [y/n]: "
				read USER_RESPONSE
			done
		done
		info "User chose ${SERVER_IP} for binding the Simulator services."
		echo_info ""
	fi

	CHOSEN_HOST_ADDRESS=${SERVER_IP}
}

function handle_resources() {
	local APP_RESOURCES_DIR="${RESOURCES_DOWNLOADS_DIR}/app_resources"
	local GENERATOR_DST_DIR="${RESOURCES_BUILD_DIR}/generator"
	local INSTALLS_DST_DIR="${GENERATOR_DST_DIR}/installs"
	local SCRIPTS_DST_DIR="${GENERATOR_DST_DIR}/scripts"
	local GENERATE_SYSTEM_ERRORS_DST_DIR="${RESOURCES_BUILD_DIR}/dpod/GenerateSystemErrors"
	local EXTERNAL_SCRIPTS_DST_DIR="${SCRIPTS_DST_DIR}/ExternalScripts"

	echo_info "Creating a new build environment..."
	mkdir -p ${RESOURCES_BUILD_DIR} >>${LOG_FILE} 2>&1
	cp -R ${SCR_BUILD_DIR}/* ${RESOURCES_BUILD_DIR} >>${LOG_FILE} 2>&1
	mkdir -p ${RESOURCES_DOWNLOADS_DIR} >>${LOG_FILE} 2>&1

	if [[ "${DPOD_IMAGE_URL}" == "/"* ]]; then
		LOCAL_DPOD_IMAGE_FILE="${DPOD_IMAGE_URL}"
		LOCAL_DPOD_MD5_FILE="${DPOD_MD5_URL}"
	else
		# Download DPOD image
		download_with_md5_test ${LOCAL_DPOD_MD5_FILE} ${DPOD_MD5_URL} ${LOCAL_DPOD_IMAGE_FILE} ${DPOD_IMAGE_URL} "DPOD image" ${COMMON_OS}
	fi

	if [[ "${RESOUCES_URL}" == "/"* ]]; then
		RESOUCES_FILE="${RESOUCES_URL}"
		RESOURCES_MD5_FILE="${RESOUCES_MD5_URL}"
	else
		# Download resources
		download_with_md5_test ${RESOURCES_MD5_FILE} ${RESOUCES_MD5_URL} ${RESOUCES_FILE} ${RESOUCES_URL} "resources bundle" ${COMMON_OS}
	fi
	# Generate IDG initial volumes
	generate_idg_volumes

	echo_info "Extracting resources bundle..."
	tar -zxvf ${RESOUCES_FILE} -C ${RESOURCES_DOWNLOADS_DIR} >>${LOG_FILE} 2>&1

	echo_info "Updating Generator resources..."
	mkdir -p ${INSTALLS_DST_DIR} >>${LOG_FILE} 2>&1
	cp -fr ${APP_RESOURCES_DIR}/${RESOURCES_JDK_FILE} ${INSTALLS_DST_DIR} >>${LOG_FILE} 2>&1
	cp ${COMMON_UTILS_SOURCE} ${SCRIPTS_DST_DIR}/ >>${LOG_FILE} 2>&1

	if [[ ${EXTERNAL_SCRIPT_EXECUTION_FLAG_UP} -eq 1 ]]; then
		echo_info "Copying external script ${EXTERNAL_SCRIPT_SRC_PATH} to Generator files directory..."
		mkdir -p ${EXTERNAL_SCRIPTS_DST_DIR} >>${LOG_FILE} 2>&1
		cp -fr ${EXTERNAL_SCRIPT_SRC_PATH}.* ${EXTERNAL_SCRIPTS_DST_DIR} >>${LOG_FILE} 2>&1
	fi

	echo_info "Updating DPOD resources..."
	mkdir -p ${GENERATE_SYSTEM_ERRORS_DST_DIR} >>${LOG_FILE} 2>&1
	cp ${APP_RESOURCES_DIR}/${RESOURCES_APP_DATA_LOADER_FILE} ${GENERATE_SYSTEM_ERRORS_DST_DIR}/ >>${LOG_FILE} 2>&1

	if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
		echo_info "Updating Splunk resources..."
		${SED_COMMAND} "s/__HOST_ADDRESS__/${DPOD_HOSTNAME}/g" ${SPLUNK_DPOD_TRAN_VIEW_FILE}
		${SED_COMMAND} "s/__DPOD_PORT__/${DPOD_PORT}/g" ${SPLUNK_DPOD_TRAN_VIEW_FILE}
		cp ${COMMON_UTILS_SOURCE} ${SPLUNK_SCRIPTS_DIR}/ >>${LOG_FILE} 2>&1
		if [[ ${WEB_CONF_FLAG_UP} -eq 1 ]]; then
			cp ${WEB_CONF_SRC_PATH} ${WEB_CONF_DST_PATH}
		fi
	fi

	chmod -R 777 ${RESOURCES_BUILD_DIR} >>${LOG_FILE} 2>&1
	rm -Rf ${APP_RESOURCES_DIR} >>${LOG_FILE} 2>&1

	if [[ "Darwin" = "${COMMON_OS}" ]]; then
		cp /etc/localtime ${RESOURCES_BUILD_DIR}/dpod/
		cp /etc/localtime ${RESOURCES_BUILD_DIR}/generator/
		cp /etc/localtime ${RESOURCES_BUILD_DIR}/splunk/
	fi
}

function generate_idg_volumes() {
	local IDG_SERVICE_NUM=0

	echo_info "Removing old DataPower gateways configuration..."
	find ${IDG_INITIAL_VOLUMES_PATH} -name "volumes_idg*" | grep -w -v "${IDG_INITIAL_VOLUME_TEMPLATE_DIR}" | xargs rm -Rf

	while [[ ${IDG_SERVICE_NUM} -lt ${NUMBER_OF_IDG_SERVICES_TO_HANDLE} ]]; do
		IDG_SERVICE_NUM=$((IDG_SERVICE_NUM + 1))
		echo_info "Generating volumes for DataPower gateway ${IDG_SERVICE_NUM}..."
		NEW_IDG_VOLUME="${IDG_VOLUME_DIR_PREFIX}${IDG_SERVICE_NUM}"

		mkdir -p ${NEW_IDG_VOLUME}
		cp -R ${IDG_INITIAL_VOLUME_TEMPLATE_DIR}/* ${NEW_IDG_VOLUME} >>${LOG_FILE} 2>&1

		# Generate IDG address and replace the IP address placeholders
		NEW_IDG_ADDRESS_LAST_OCTET=$((INITIAL_IDG_ADREESS_LAST_OCTET + IDG_SERVICE_NUM))
		NEW_IDG_ADDRESS="${INITIAL_IDG_ADREESS_SEGMENT}.${NEW_IDG_ADDRESS_LAST_OCTET}"
		NEW_IDG_AUTO_STARTUP_FILE="${NEW_IDG_VOLUME}/config/auto-startup.cfg"
		${SED_COMMAND} "s/__IDG_ADDRESS__/${NEW_IDG_ADDRESS}/g" ${NEW_IDG_AUTO_STARTUP_FILE} >>${LOG_FILE} 2>&1
		info "Address of DataPower gateway ${IDG_SERVICE_NUM} will be set to ${NEW_IDG_ADDRESS}"
	done
}

function start_all_services() {
	[[ "$#" -ne 3 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1

	local DPOD_USER="$1"
	local DPOD_PASS="$2"
	local CHOSEN_HOST_ADDRESS="$3"

	echo_info "Starting Generator container..."
	start_print_progress
	sudo -E docker compose -f ${COMPOSE_FILE_PATH} up -d generator >>${LOG_FILE} 2>&1
	stop_print_progress
	wait_for_generator_to_be_ready
	echo_info "Generator container is up and running."

	echo_info "Starting MQ container..."
	start_print_progress
	sudo -E docker compose -f ${COMPOSE_FILE_PATH} up -d mq >>${LOG_FILE} 2>&1
	stop_print_progress
	wait_for_mq_to_be_ready
	echo_info "MQ container is up and running."

	local IDG_SERVICE_NUM=0
	local NEW_IDG_ADDRESS_LAST_OCTET=$((INITIAL_IDG_ADREESS_LAST_OCTET + IDG_SERVICE_NUM))
	local IDG_ADDRESS="${INITIAL_IDG_ADREESS_SEGMENT}.${NEW_IDG_ADDRESS_LAST_OCTET}"

	while [[ ${IDG_SERVICE_NUM} -lt ${NUMBER_OF_IDG_SERVICES_TO_HANDLE} ]]; do
		IDG_SERVICE_NUM=$((IDG_SERVICE_NUM + 1))
		NEW_IDG_ADDRESS_LAST_OCTET=$((INITIAL_IDG_ADREESS_LAST_OCTET + IDG_SERVICE_NUM))
		IDG_ADDRESS="${INITIAL_IDG_ADREESS_SEGMENT}.${NEW_IDG_ADDRESS_LAST_OCTET}"

		echo_info "Starting DataPower gateway ${IDG_SERVICE_NUM} container..."
		start_print_progress
		sudo -E docker compose -f ${COMPOSE_FILE_PATH} up -d idg${IDG_SERVICE_NUM} >>${LOG_FILE} 2>&1
		stop_print_progress
		wait_for_idg_to_be_ready ${IDG_ADDRESS} ${IDG_SERVICE_NUM}
		echo_info "DataPower gateway ${IDG_SERVICE_NUM} container is up and running."
	done

	echo_info "Starting DPOD container..."
	start_print_progress
	mkdir -p "${DPOD_VOLUMES_DIR}/logs/httpd" "${DPOD_VOLUMES_DIR}/data" "${DPOD_VOLUMES_DIR}/installs/logs"
	export CHOSEN_HOST_ADDRESS
	sudo -E docker compose -f ${COMPOSE_FILE_PATH} up -d dpod >>${LOG_FILE} 2>&1
	stop_print_progress
	wait_for_dpod_to_be_ready "${DPOD_USER}" "${DPOD_PASS}"
	echo_info "DPOD container is up and running."

	if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
		echo_info "Starting Splunk container..."
		start_print_progress
		sudo -E docker compose -f ${COMPOSE_FILE_PATH} up -d splunk >>${LOG_FILE} 2>&1
		stop_print_progress
		wait_for_splunk_to_be_ready
		echo_info "Splunk container is up and running."
	fi
}

function wait_for_generator_to_be_ready() {
	local GENERATOR_RESPONSE_FILE="${LOG_DIR}/genertor_container_response.log"
	local TRIES_COUNT=0
	local GENERATOR_STATE=""

	echo_info "Waiting for Generator container to be ready..."
	start_print_progress
	while [[ ! ${GENERATOR_STATE} == '"running"' ]] && [[ ${TRIES_COUNT} -lt ${MAX_SERVICE_TESTS} ]]; do
		GENERATOR_STATE=$(sudo -E docker inspect --format='{{json .State.Status}}' ${GENERATOR_CONTAINER}) 2>>${LOG_FILE} || true # Ignoring errors
		TRIES_COUNT=$((TRIES_COUNT + 1))
		sleep ${WAIT_BETWEEN_SERVICE_TESTS}
	done
	stop_print_progress

	if [[ ! ${GENERATOR_STATE} == '"running"' ]]; then
		echo_error "Generator container failed to start."
		return 1
	fi
}

function wait_for_mq_to_be_ready() {
	local MQ_RESPONSE_FILE="${LOG_DIR}/mq_dspmq_response.log"
	local SUCCESS_INDICATION=0
	local TRIES_COUNT=0

	echo_info "Waiting for MQ container to be ready..."
	start_print_progress
	while [[ ${SUCCESS_INDICATION} -eq 0 ]] && [[ ${TRIES_COUNT} -lt ${MAX_SERVICE_TESTS} ]]; do
		sudo -E docker exec -i ${MQ_CONTAINER} dspmq >${MQ_RESPONSE_FILE} 2>>${LOG_FILE} || true # Ignoring errors

		# Check for success indication in response
		SUCCESS_INDICATION=$(grep "STATUS(Running)" ${MQ_RESPONSE_FILE} | wc -l)
		TRIES_COUNT=$((TRIES_COUNT + 1))

		sleep ${WAIT_BETWEEN_SERVICE_TESTS}
	done
	stop_print_progress

	if [[ ${SUCCESS_INDICATION} -eq 0 ]]; then
		echo_error "MQ container failed to start."
		return 1
	fi
}

function wait_for_idg_to_be_ready() {
	[[ "$#" -ne 2 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1

	local IDG_SERVER_ADDRESS=$1
	local IDG_NUM=$2
	local SCRIPTS_DIR="/MonTier/scripts"
	local GET_DEVICE_INFO_REQUEST="${SCRIPTS_DIR}/CheckDockerServices/SOMA_GetDeviceInfo_Request.xml"
	local IDG_SOMA_URL="https://${IDG_SERVER_ADDRESS}:${SOMA_PORT}/service/mgmt/amp/1.0"

	local CURL_RESPONSE_FILE="${LOG_DIR}/idg_${IDG_SERVER_ADDRESS}_deviceInfoResponse.log"

	local SUCCESS_INDICATION=0
	local TRIES_COUNT=0

	echo_info "Waiting for DataPower gateway ${IDG_NUM} container to be ready..."
	start_print_progress
	while [[ ${SUCCESS_INDICATION} -eq 0 ]] && [[ ${TRIES_COUNT} -lt ${MAX_SERVICE_TESTS} ]]; do
		sudo -E docker exec -i ${GENERATOR_CONTAINER} \
			curl -XPOST -s -w "\nResultCode:%{http_code}" --user ${SOMA_USER}:${SOMA_PASS} -X POST \
			-d "@${GET_DEVICE_INFO_REQUEST}" --insecure ${IDG_SOMA_URL} >${CURL_RESPONSE_FILE} 2>>${LOG_FILE} || true # Ignoring errors

		# Check for success indication in response
		SUCCESS_INDICATION=$(grep "ResultCode:200" ${CURL_RESPONSE_FILE} | wc -l)
		TRIES_COUNT=$((TRIES_COUNT + 1))

		sleep ${WAIT_BETWEEN_SERVICE_TESTS}
	done
	stop_print_progress

	if [[ ${SUCCESS_INDICATION} -eq 0 ]]; then
		echo_error "DataPower gateway ${IDG_NUM} container failed to start."
		return 1
	fi
}

function wait_for_dpod_to_be_ready() {
	[[ "$#" -ne 2 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1

	local DPOD_USER="$1"
	local DPOD_PASS="$2"
	local WAIT_FOR_UI_RESPONSE_FILE="${LOG_DIR}/TestUI_Response.log"
	local UI_TEST_REST_URI="/op/api/v2/reports/APIC"
	local SUCCESS_INDICATION=0
	local DEVICE_EXISTS_INDICATION=0
	local TRIES_COUNT=0

	echo_info "Waiting for DPOD container to be ready..."
	start_print_progress
	while [[ ${SUCCESS_INDICATION} -eq 0 ]] && [[ ${TRIES_COUNT} -lt ${MAX_SERVICE_TESTS} ]]; do
		sudo -E docker exec -i ${GENERATOR_CONTAINER} \
			curl -s --user "${DPOD_USER}:${DPOD_PASS}" -X GET --insecure -w "\nResultCode:%{http_code}" \
			"https://${DPOD_ADDRESS}${UI_TEST_REST_URI}" >${WAIT_FOR_UI_RESPONSE_FILE} 2>>${LOG_FILE} || true # Ignoring errors

		# Check for success indication in response
		SUCCESS_INDICATION=$(grep "ResultCode:200" ${WAIT_FOR_UI_RESPONSE_FILE} | wc -l)
		TRIES_COUNT=$((TRIES_COUNT + 1))

		sleep ${WAIT_BETWEEN_SERVICE_TESTS}
	done
	stop_print_progress

	if [[ ${SUCCESS_INDICATION} -eq 0 ]]; then
		echo_error "DPOD container failed to start."
		return 1
	fi

	# Configure ssh
	sudo -E docker exec -i -u root ${DPOD_CONTAINER} \
		/bin/su - -c "sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config;  service sshd restart" \
		>>${LOG_FILE} 2>&1
}

function wait_for_splunk_to_be_ready() {
	local SPLUNK_RESPONSE_FILE="${LOG_DIR}/splunk_container_response.log"
	local TRIES_COUNT=0
	local SPLUNK_STATUS=""

	echo_info "Waiting for Splunk container to be ready..."
	start_print_progress
	while [[ ! ${SPLUNK_STATUS} == '"healthy"' ]] && [[ ${TRIES_COUNT} -lt ${MAX_SERVICE_TESTS} ]]; do
		# Check that splunk is up and healthy
		SPLUNK_STATUS=$(sudo -E docker inspect --format='{{json .State.Health.Status}}' ${SPLUNK_CONTAINER}) 2>>${LOG_FILE} || true # Ignoring errors
		TRIES_COUNT=$((TRIES_COUNT + 1))
		sleep ${WAIT_BETWEEN_SERVICE_TESTS}
	done
	stop_print_progress

	if [[ ! ${SPLUNK_STATUS} == '"healthy"' ]]; then
		stop_print_progress
		echo_error "Splunk container failed to start."
		exit 1
	fi
}

function setup_simulator_users_and_roles() {
	local CURL_RESPONSE_FILE="${LOG_DIR}/CreateUsersAndRoles_Response.json"
	local REST_URI="/op/api/v1/simulator/populateUsers"

	echo_info "Creating DPOD users and roles via REST API..."
	sudo -E docker exec -i ${GENERATOR_CONTAINER} \
		curl --user ${DPOD_DEFAULT_USER}:${DPOD_DEFAULT_PASS} -X POST --header 'dpod-simulator: 922cc43d39844d22873bfc85bf1dcdac' \
		--insecure -w "\n" https://${DPOD_ADDRESS}${REST_URI} >${CURL_RESPONSE_FILE} 2>>${LOG_FILE}
}

function add_all_idg_to_dpod() {
	execute_exeternal_script "before_adding_gateways"

	local IDG_SERVICE_NUM=0
	while [[ ${IDG_SERVICE_NUM} -lt ${NUMBER_OF_IDG_SERVICES_TO_HANDLE} ]]; do
		IDG_SERVICE_NUM=$((IDG_SERVICE_NUM + 1))

		local NEW_IDG_ADDRESS_LAST_OCTET=$((INITIAL_IDG_ADREESS_LAST_OCTET + IDG_SERVICE_NUM))
		local IDG_ADDRESS="${INITIAL_IDG_ADREESS_SEGMENT}.${NEW_IDG_ADDRESS_LAST_OCTET}"
		local IDG_NAME="${IDG_NAME_PREFIX}${IDG_SERVICE_NUM}"
		add_idg_to_dpod idg${IDG_SERVICE_NUM} ${IDG_ADDRESS} ${LOG_TAR_HOST_ALIAS_TYPE} ${LOG_TAR_HOST_ALIAS_ADDRESS} ${IDG_NAME}
	done
}

function run_transactions_on_all_idg() {
	local IDG_SERVICE_NUM=0
	while [[ ${IDG_SERVICE_NUM} -lt ${NUMBER_OF_IDG_SERVICES_TO_HANDLE} ]]; do
		IDG_SERVICE_NUM=$((IDG_SERVICE_NUM + 1))

		local NEW_IDG_ADDRESS_LAST_OCTET=$((INITIAL_IDG_ADREESS_LAST_OCTET + IDG_SERVICE_NUM))
		local IDG_ADDRESS="${INITIAL_IDG_ADREESS_SEGMENT}.${NEW_IDG_ADDRESS_LAST_OCTET}"
		local IDG_NAME="${IDG_NAME_PREFIX}${IDG_SERVICE_NUM}"
		run_transactions idg${IDG_SERVICE_NUM} ${IDG_ADDRESS} ${IDG_NAME}
	done
}

function execute_exeternal_script() {
	[[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1
	local SIMULATOR_STEP=$1

	if [[ ${EXTERNAL_SCRIPT_EXECUTION_FLAG_UP} -ne 1 ]]; then
		return 0
	fi

	echo_info "Executing external script (step: ${SIMULATOR_STEP}): ${EXTERNAL_SCRIPT_NAME}..."
	local EXTERNAL_SCRIPT_LOG_FILE="${MONTIER_SCRIPTS_LOG_PATH}/${EXTERNAL_SCRIPT_NAME}_${SIMULATOR_STEP}.log"
	local EXTERNAL_SCRIPT_PATH="${MONTIER_SCRIPTS_PATH}/ExternalScripts/${EXTERNAL_SCRIPT_NAME}.sh"
	sudo -E docker exec -i -u root ${GENERATOR_CONTAINER} \
		sh -c "${EXTERNAL_SCRIPT_PATH} -s ${SIMULATOR_STEP} -a ${DPOD_ADDRESS} -u ${DPOD_SIMULATOR_API_USER} -p ${DPOD_SIMULATOR_API_PASS_ESC} > ${EXTERNAL_SCRIPT_LOG_FILE} 2>&1" \
		>>${LOG_FILE} 2>&1
}

function populate_data() {
	local REST_URI="/op/api/v1/simulator/populateData"
	local REST_URL="https://${DPOD_ADDRESS}${REST_URI}"
	local POPULATE_REQUEST_FILE="${POPULATE_DATA_DIR}/PopulateData_Request.json"
	local CURL_RESPONSE_FILE="${LOG_DIR}/PopulateData_Response.json"
	local PUBLISH_SYSLOGS="false"

	if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
		PUBLISH_SYSLOGS="true"
	fi
	sudo -E docker exec -i -u root ${GENERATOR_CONTAINER} \
		sh -c "sed -i \"s/__PUBLISH_SYSLOGS__/${PUBLISH_SYSLOGS}/g\" ${POPULATE_REQUEST_FILE}" >>${LOG_FILE} 2>&1

	echo_info "Populating DPOD data (alerts syslog publishing is set to: ${PUBLISH_SYSLOGS})..."
	sudo -E docker exec -i ${GENERATOR_CONTAINER} \
		curl --user ${DPOD_SIMULATOR_API_USER}:${DPOD_SIMULATOR_API_PASS} -X POST --header 'dpod-simulator: 922cc43d39844d22873bfc85bf1dcdac' \
		--insecure -d "@${POPULATE_REQUEST_FILE}" -w "\n" https://${DPOD_ADDRESS}${REST_URI} >${CURL_RESPONSE_FILE} 2>>${LOG_FILE}
}

function add_idg_to_dpod() {
	[[ "$#" -ne 5 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1

	local IDG_SERVICE_NAME=$1
	local IDG_ADDRESS=$2
	local LOG_TAR_TYPE=$3
	local LOG_TAR_ADDRESS=$4
	local IDG_NAME=$5
	local ADD_IDG_TO_DPOD_RESPONSE_FILE="${LOG_DIR}/${IDG_SERVICE_NAME}_DeviceRestCallResponse.log"
	local DEVICE_EXISTS_INDICATION=0
	local SUCCESS_INDICATION=0
	local TIMES_TO_TRY=10
	local TRIES_COUNT=0

	echo_info "Adding DataPower gateway ${IDG_SERVICE_NAME} to DPOD via REST API..."

	while [[ ${SUCCESS_INDICATION} -eq 0 ]] && [[ ${TRIES_COUNT} -lt ${TIMES_TO_TRY} ]]; do
		TRIES_COUNT=$((TRIES_COUNT + 1))
		start_print_progress
		sudo -E docker exec -i ${GENERATOR_CONTAINER} \
			curl -s --user ${DPOD_SIMULATOR_API_USER}:${DPOD_SIMULATOR_API_PASS} -X POST --insecure -w "\n" \
			https://${DPOD_ADDRESS}/op/api/v1/devices?name=${IDG_NAME}\&host=${IDG_ADDRESS}\&somaPort=${SOMA_PORT}\&romaPort=${ROMA_PORT}\&somaPassword=${SOMA_PASS}\&somaUser=${SOMA_USER}\&logTargetAddressType=${LOG_TAR_TYPE}\&logTargetAddress=${LOG_TAR_ADDRESS}\&syslogAgentName=${SYS_AGNT_NAME}\&setupSyslogForDomains=${SETUP_SYS_AGNT}\&domainsAnalysisLevel=${DOMAIN_ANLYSIS_LEVEL}\&wsmAgentName=${WSM_AGNT_NAME}\&setupWsmForDomains=${SETUP_WSM}\&setupCertificateMonitor=${SETUP_CERT_MON}\&autoSetupPattern=${AUTO_SETUP_PATTERN}\&autoSetupSyslogAgentName=${AUTO_SETUP_SYS_AGNT_NAME}\&autoSetupWsmAgentName=${AUTO_SETUP_WSM_AGNT_NAME}\&autoSetupAnalysisLevel=${AUTO_SETUP_ANALYSYS_LEVEL} \
			>${ADD_IDG_TO_DPOD_RESPONSE_FILE} 2>>${LOG_FILE}
		stop_print_progress

		# Check for success indication in response
		SUCCESS_INDICATION=$(grep "\"resultCode\":\"SUCCESS\"" ${ADD_IDG_TO_DPOD_RESPONSE_FILE} | wc -l)
		if [[ ${SUCCESS_INDICATION} -eq 0 ]]; then
			echo_warn "Failed to add DataPower gateway ${IDG_SERVICE_NAME} to DPOD via REST API."
			if [[ ${TRIES_COUNT} -lt ${TIMES_TO_TRY} ]]; then
				echo_info "Will try again in ${SEC_TO_WAIT_AFTER_DEVICE_ADD} seconds..."
				sleep ${SEC_TO_WAIT_AFTER_DEVICE_ADD}
			fi
		fi
	done

	if [[ ${SUCCESS_INDICATION} -eq 0 ]]; then
		echo_error "Failed to add DataPower gateway ${IDG_SERVICE_NAME} to DPOD via REST API. You may need to manually add the device to DPOD later."
	fi
}

function run_transactions() {
	[[ "$#" -ne 3 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1

	local IDG_SERVICE_NAME=$1
	local IDG_ADDRESS=$2
	local IDG_NAME=$3
	local ES_ENCRYPTED_PASSWORD=$(sudo -E docker exec -i -u root ${DPOD_CONTAINER} /bin/su - -c "grep elasticsearch.restclient.password /app/ui/MonTier-UI/conf/MonTierUI.conf" | awk -F= '{print $2}')

	echo_info "Starting transactions on DataPower gateway ${IDG_SERVICE_NAME}..."
	sudo -E docker exec -d -u root ${GENERATOR_CONTAINER} \
		sh -c "${TRANSACTIONS_ACTIVATION_SCRIPT} -i ${IDG_SERVICE_NAME} -n ${IDG_NAME} -a ${IDG_ADDRESS} -p ${SOMA_PORT} -u ${SOMA_USER} -s ${SOMA_PASS} -d ${DPOD_ADDRESS} -r ${DPOD_SIMULATOR_API_USER} -o ${DPOD_SIMULATOR_API_PASS_ESC} -e "${ES_ENCRYPTED_PASSWORD}" -c ${DPOD_SSH_PASS} > ${TRANSACTIONS_ACTIVATION_LOG_FILE} 2> ${TRANSACTIONS_ACTIVATION_LOG_FILE}" \
		>>${LOG_FILE} 2>&1
}

function start_splunk_operation() {
	if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
		echo_info "Running Splunk operations..."
		sudo -E docker exec -d -u root ${SPLUNK_CONTAINER} \
			sh -c "${SPLUNK_OPERATIONS_SCRIPT} -c -l ${SLEEP_BETWEEN_SPLUNK_RESTARTS} > ${SPLUNK_OPERATIONS_ACTIVATION_LOG_FILE} 2>&1" \
			>>${LOG_FILE} 2>&1
	fi
}

function activate_payload_capture() {
	local REST_URI="/op/api/v2/payloadCaptures/APIC"
	local REST_URL="https://${DPOD_ADDRESS}${REST_URI}"
	local PAYLOAD_CAPTURE_REQUEST_FILE="${PAYLOAD_CAPTURE_DIR}/payloadCapture.json"
	local CURL_RESPONSE_FILE="${LOG_DIR}/payloadCapture_Response.json"
	local CURL_RETRY_LIMIT=120
	local CURL_RETRIES=0
	local CAPTURE_SUCCESS_INDICATION=0
	local NO_CATALOGS_FOUND_INDICATION=0

	echo_info "Activating APIC payload capture..."
	start_print_progress
	while [[ ${CAPTURE_SUCCESS_INDICATION} -eq 0 ]] && [[ ${CURL_RETRIES} -lt ${CURL_RETRY_LIMIT} ]]; do
		CURL_RETRIES=$((CURL_RETRIES + 1))
		sudo -E docker exec -i ${GENERATOR_CONTAINER} \
			curl --user ${DPOD_SIMULATOR_API_USER}:${DPOD_SIMULATOR_API_PASS} -X POST \
			--header 'dpod-simulator: 922cc43d39844d22873bfc85bf1dcdac' --insecure \
			-d "@${PAYLOAD_CAPTURE_REQUEST_FILE}" -w "\n" ${REST_URL} >${CURL_RESPONSE_FILE} 2>>${LOG_FILE}

		# Check for success and no catalogs indications in response
		CAPTURE_SUCCESS_INDICATION=$(grep "\"resultCode\":\"SUCCESS\"" ${CURL_RESPONSE_FILE} | wc -l) || CAPTURE_SUCCESS_INDICATION=0
		NO_CATALOGS_FOUND_INDICATION=$(grep "\"resultErrorCode\":\"NO_CATALOGS_FOUND\"" ${CURL_RESPONSE_FILE} | wc -l) || NO_CATALOGS_FOUND_INDICATION=0
		if [[ ${CAPTURE_SUCCESS_INDICATION} -eq 0 ]]; then
			if [[ ${NO_CATALOGS_FOUND_INDICATION} -eq 0 ]]; then
				echo_warn "Failed to activate APIC payload capture."
			fi
			sleep ${WAIT_BETWEEN_PAYLOAD_CAPTURE_REST_CALLS}
		fi
	done
	stop_print_progress

	if [[ ${CAPTURE_SUCCESS_INDICATION} -eq 0 ]]; then
		echo_error "Failed to activate APIC payload capture. Please create payload capture manually in the Web Console."
	fi
}

function save_user_input_to_file() {
	echo_info "Saving build properties to file..."

	# Delete old (if exists) and create new file from template
	rm -f ${USER_INPUT_FILE} >>${LOG_FILE} 2>&1
	cp ${USER_INPUT_FILE_TEMPLATE} ${USER_INPUT_FILE} >>${LOG_FILE} 2>&1

	${SED_COMMAND} "s/__ADDITIONAL_IDG_FLAG_UP__/${ADDITIONAL_IDG_FLAG_UP}/g" ${USER_INPUT_FILE} >>${LOG_FILE} 2>&1
	${SED_COMMAND} "s/__SPLUNK_FLAG_UP__/${SPLUNK_FLAG_UP}/g" ${USER_INPUT_FILE} >>${LOG_FILE} 2>&1
	${SED_COMMAND} "s/__CHOSEN_HOST_ADDRESS__/${CHOSEN_HOST_ADDRESS}/g" ${USER_INPUT_FILE} >>${LOG_FILE} 2>&1
	${SED_COMMAND} "s/__NUMBER_OF_IDG_SERVICES_TO_HANDLE__/${NUMBER_OF_IDG_SERVICES_TO_HANDLE}/g" ${USER_INPUT_FILE} >>${LOG_FILE} 2>&1
	${SED_COMMAND} "s/__EXTERNAL_SCRIPT_EXECUTION_FLAG_UP__/${EXTERNAL_SCRIPT_EXECUTION_FLAG_UP}/g" ${USER_INPUT_FILE} >>${LOG_FILE} 2>&1
	if [[ ${EXTERNAL_SCRIPT_EXECUTION_FLAG_UP} -eq 1 ]]; then
		${SED_COMMAND} "s#__EXTERNAL_SCRIPT_SRC_PATH__#${EXTERNAL_SCRIPT_SRC_PATH}#g" ${USER_INPUT_FILE} >>${LOG_FILE} 2>&1
	fi
}

function show_links() {
	echo_info ""
	echo_info "The DPOD Simulator is ready!"
	echo_info "============================"
	echo_info "To access DPOD use the following URLs:"
	echo_info "   https://${CHOSEN_HOST_ADDRESS}:4433/op"
	echo_info "   https://${CHOSEN_HOST_ADDRESS}:4433/admin"

	if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
		echo_info "To access Splunk use the following URL:"
		echo_info "   http://${CHOSEN_HOST_ADDRESS}:8000"
	fi

	local IDG_LOCAL_INITIAL_UI_PORT=9090
	local IDG_SERVICE_NUM=0
	while [[ ${IDG_SERVICE_NUM} -lt ${NUMBER_OF_IDG_SERVICES_TO_HANDLE} ]]; do
		IDG_SERVICE_NUM=$((IDG_SERVICE_NUM + 1))
		local IDG_LOCAL_UI_PORT=$((IDG_LOCAL_INITIAL_UI_PORT + IDG_SERVICE_NUM))
		echo_info "To access DataPower gateway ${IDG_SERVICE_NUM} Web UI use the following URL:"
		echo_info "   https://${CHOSEN_HOST_ADDRESS}:${IDG_LOCAL_UI_PORT}"
	done

	echo_info " "
}

#################
# Invoking main #
#################
main "$@"
