#!/bin/bash

# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace
set -o pipefail
set -o nounset

###########################
# Variables and Constants #
###########################
THIS_SCRIPT_COMMAND="$0 $@"
THIS_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

#############
# Functions #
#############
function current_date_time() {
    printf '%(%F_%H-%M-%S)T' -1
}

function log_message() {
    [[ "$#" -ne 2 ]] && log_message "ERROR" "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local LOG_LEVEL="$1"
    local LOG_MESSAGE="$2"

    echo -e "$(current_date_time) ${LOG_LEVEL} ${LOG_MESSAGE}"
}

function on_error() {
    local LINE_NO="$1"
    log_message "ERROR" "Error occurred on line $LINE_NO. Aborting."
    exit 1
}

function main() {
    delete_autostart_entry
    configure_gnome_settings
    add_desktop_launchers
    shutdown_server
}

function delete_autostart_entry() {
    log_message "INFO" "Deleting autostart entry..."
    rm ~/.config/autostart/simulator-ova-setup.desktop
    log_message "INFO" "---------------------------------------------------------------------------"
}

# Use 'dconf dump /' to inspect current system settings
# Use 'dconf watch /' to inspect system settings changes while changing them
# To set defaults for all users - copy a settings.keyfile to /etc/dconf/db/local.d/ and execute 'dconf update'
function configure_gnome_settings() {
    log_message "INFO" "Configuring GNOME settings..."
    gsettings set org.gnome.desktop.screensaver lock-enabled false
    gsettings set org.gnome.desktop.session idle-delay 0
    gsettings set org.gnome.settings-daemon.peripherals.keyboard numlock-state on
    gsettings set org.gnome.system.location enabled false
    gsettings set org.gnome.nautilus.preferences default-folder-viewer list-view
    gsettings set org.gnome.nautilus.list-view default-zoom-level small
    gsettings set org.gnome.nautilus.icon-view default-zoom-level small
    gsettings set org.gtk.Settings.FileChooser show-hidden true
    gsettings set org.gnome.Terminal.Legacy.Settings theme-variant dark
    gsettings set org.gnome.nautilus.window-state geometry '1500x798+100+100'
    gsettings set org.gnome.nautilus.window-state maximized false

    local TERMINAL_PROFILE=$(gsettings get org.gnome.Terminal.ProfilesList default)
    TERMINAL_PROFILE=${TERMINAL_PROFILE:1:-1} # remove leading and trailing single quotes
    gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${TERMINAL_PROFILE}/" default-size-columns 160
    gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${TERMINAL_PROFILE}/" default-size-rows 45

    log_message "INFO" "---------------------------------------------------------------------------"
}

function add_desktop_launchers() {
    log_message "INFO" "Adding desktop launchers..."
    mkdir -p ~/Desktop/
    cp ${THIS_SCRIPT_DIR}/desktop-launchers/* ~/Desktop/
    for CURR_DESKTOP_LAUNCHER in ~/Desktop/*.desktop; do
        gio set "$CURR_DESKTOP_LAUNCHER" "metadata::trusted" yes
    done
    log_message "INFO" "---------------------------------------------------------------------------"
}

function shutdown_server() {
    log_message "INFO" "Simulator OVA is ready."
    read -e -r -p "Press [ENTER] to shutdown: "
    shutdown -h now
}

#################
# Invoking main #
#################
main "$@"
