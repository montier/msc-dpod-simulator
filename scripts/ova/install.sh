#!/bin/bash

# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace
set -o pipefail
set -o nounset

###########################
# Variables and Constants #
###########################
THIS_SCRIPT_COMMAND="$0 $@"
THIS_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
USER_NAME=admin

#############
# Functions #
#############
function current_date_time() {
    printf '%(%F_%H-%M-%S)T' -1
}

function log_message() {
    [[ "$#" -ne 2 ]] && log_message "ERROR" "Wrong number of arguments for ${FUNCNAME}" && exit 1
    local LOG_LEVEL="$1"
    local LOG_MESSAGE="$2"

    echo -e "$(current_date_time) ${LOG_LEVEL} ${LOG_MESSAGE}"
}

function on_error() {
    local LINE_NO="$1"
    log_message "ERROR" "Error occurred on line $LINE_NO. Aborting."
    exit 1
}

function main() {
    disable_ip_v6
    disable_firewall
    update_os
    install_basic_software
    install_ntp
    configure_os
    setup_docker_environment
    create_user
    create_user_autostart_entry
    reboot_server
}

function disable_ip_v6() {
    log_message "INFO" "Disabling IPv6..."
    sysctl -w net.ipv6.conf.all.disable_ipv6=1
    sysctl -w net.ipv6.conf.default.disable_ipv6=1
    echo "net.ipv6.conf.all.disable_ipv6=1" >>/etc/sysctl.conf
    echo "net.ipv6.conf.default.disable_ipv6=1" >>/etc/sysctl.conf
    log_message "INFO" "---------------------------------------------------------------------------"
}

function disable_firewall() {
    log_message "INFO" "Disabling firewalld..."
    yum -y -q install firewalld
    systemctl stop firewalld
    systemctl disable firewalld
    log_message "INFO" "---------------------------------------------------------------------------"
}

function update_os() {
    log_message "INFO" "Updating OS..."
    yum -y -q update
    yum -y -q install yum-utils
    package-cleanup -y -q --oldkernels --count=1
    log_message "INFO" "---------------------------------------------------------------------------"
}

function install_basic_software() {
    log_message "INFO" "Installing GNOME Desktop..."
    yum -y -q install epel-release
    yum -y -q groupinstall "GNOME Desktop"

    log_message "INFO" "Configuring system to start graphical interface..."
    systemctl set-default graphical.target

    log_message "INFO" "Skipping initial user creation and initial setup wizard..."
    sed -i '/\[daemon\]/ a InitialSetupEnable=false' /etc/gdm/custom.conf
    sed -i 's/^Exec=.*$/Exec=\/bin\/true/' /etc/xdg/autostart/gnome-initial-setup-first-login.desktop

    log_message "INFO" "Adding YUM Repositories..."
    cp ${THIS_SCRIPT_DIR}/*.repo /etc/yum.repos.d/

    log_message "INFO" "Installing managed applications..."
    yum -y -q install open-vm-tools open-vm-tools-desktop open-vm-tools-devel telnet vim culmus-* alef-fonts* dejavu-* google-noto-sans-hebrew-fonts
    yum -y -q install --nogpgcheck google-chrome-stable # Chrome's GPG cannot be checked in CentOS 7 as of Chrome 114
    log_message "INFO" "---------------------------------------------------------------------------"
}

function install_ntp() {
    log_message "INFO" "Installing NTP..."
    systemctl stop chronyd
    systemctl disable chronyd
    yum -y -q install ntp
    systemctl enable ntpd
    systemctl start ntpd
    log_message "INFO" "---------------------------------------------------------------------------"
}

function configure_os() {
    log_message "INFO" "Configuring OS..."
    echo fs.inotify.max_user_watches=524288 | tee -a /etc/sysctl.conf && sysctl -p
    log_message "INFO" "Setting sudo timeout..."
    echo "Defaults timestamp_timeout=120" >/etc/sudoers.d/timeout
    log_message "INFO" "---------------------------------------------------------------------------"
}

function setup_docker_environment() {
    log_message "INFO" "Setting up Docker environment..."
    ~/msc-dpod-simulator/src/msc-dpod-simulator/scripts/setupDockerEnv.sh
    log_message "INFO" "---------------------------------------------------------------------------"
}

function create_user() {
    log_message "INFO" "Creating user..."
    useradd -m -s /bin/bash ${USER_NAME}
    usermod -g wheel ${USER_NAME}
    echo 'DpodDemo1@' | sudo passwd --stdin "${USER_NAME}"
    log_message "INFO" "---------------------------------------------------------------------------"
}

function create_user_autostart_entry() {
    log_message "INFO" "Creating user autostart entry..."
    mkdir -p /home/${USER_NAME}/.config/autostart
    cp ${THIS_SCRIPT_DIR}/simulator-ova-setup.desktop /home/${USER_NAME}/.config/autostart/
    chown -R ${USER_NAME} /home/${USER_NAME}/.config

    cp -r ${THIS_SCRIPT_DIR} /home/${USER_NAME}/simulator-ova-setup
    chown -R ${USER_NAME} /home/${USER_NAME}/simulator-ova-setup
    log_message "INFO" "---------------------------------------------------------------------------"
}

function reboot_server() {
    log_message "INFO" "Setup will continue in Gnome Desktop after reboot."
    read -e -r -p "Press [ENTER] to reboot: "
    reboot
}

#################
# Invoking main #
#################
main "$@"
