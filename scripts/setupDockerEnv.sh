#!/bin/bash

#################################################################################################################
##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
#################################################################################################################

# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace
set -o pipefail
set -o nounset

###########################
# Variables and Constants #
###########################
THIS_SCRIPT_COMMAND="$0 $@"
THIS_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
THIS_SCRIPT_FILE="$(basename -- "${BASH_SOURCE[0]}")"
THIS_SCRIPT_FILE="${THIS_SCRIPT_FILE%%.*}"
COMMON_UTILS_SOURCE="${THIS_SCRIPT_DIR}/commonUtils.sh"

source ${COMMON_UTILS_SOURCE}

NOW="$(current_date_time)"
LOG_DIR="${THIS_SCRIPT_DIR}/logs"
LOG_FILE_NAME="setupDockerEnv_${NOW}.log"
LOG_FILE="${LOG_DIR}/${LOG_FILE_NAME}"

##############
# Parameters #
##############
ASSUME_YES=0

#############
# Functions #
#############
function main() {
	echo_log_file_path

	parse_parameters "$@"

	echo_info "Setup Docker Environment"
	echo_info "========================"

	echo_info "Enabling sudo for user ${USER}..."
	sudo touch /tmp/enable_sudo

	discover_os_type

	if [[ " ${COMMON_OS_ID} ${COMMON_OS_ID_LIKE} " == *" rhel "* ]]; then
		handle_redhat
	elif [[ " ${COMMON_OS_ID} ${COMMON_OS_ID_LIKE} " == *" ubuntu "* ]]; then
		handle_ubuntu
	fi

	echo_info "Setup completed successfully."
}

function parse_parameters() {
	while [[ $# -gt 0 ]]; do
		case "$1" in
		-h | --help)
			show_help
			exit 0
			;;
		-y | --assume-yes)
			ASSUME_YES=1
			shift
			;;
		*)
			echo_error "Unknown parameter $1"
			show_help
			exit 1
			;;
		esac
	done
}

function show_help() {
	echo_info "$(
		cat <<EOF
Displaying help
Usage:
  ${THIS_SCRIPT_FILE}.sh [OPTIONS]

Options:
  -y, --assume-yes  Assume yes to all questions.
  -h, --help        Display help.
EOF
	)"
}

function handle_ubuntu() {
	local IS_WGET_EXISTS=1
	wget --version >>${LOG_FILE} 2>&1 || IS_WGET_EXISTS=0
	if [[ ${IS_WGET_EXISTS} -eq 0 ]]; then
		confirm "wget is not installed. Do you want to install wget now?"
		echo_info "Installing wget..."
		sudo apt-get update >>${LOG_FILE} 2>&1
		sudo apt-get install -y wget >>${LOG_FILE} 2>&1
	else
		echo_info "wget is already installed"
	fi

	local IS_SSHPASS_EXISTS=1
	sshpass -V >>${LOG_FILE} 2>&1 || IS_SSHPASS_EXISTS=0
	if [[ ${IS_SSHPASS_EXISTS} -eq 0 ]]; then
		confirm "sshpass is not installed. Do you want to install sshpass now?"
		echo_info "Installing sshpass..."
		sudo apt-get update >>${LOG_FILE} 2>&1
		sudo apt-get install -y sshpass >>${LOG_FILE} 2>&1
	else
		echo_info "sshpass is already installed"
	fi

	local IS_DOCKER_EXISTS=1
	docker --version >>${LOG_FILE} 2>&1 || IS_DOCKER_EXISTS=0
	if [[ ${IS_DOCKER_EXISTS} -eq 0 ]]; then
		confirm "Docker-CE is not installed. Do you want to install Docker-CE now?"
		echo_info "Installing Docker-CE..."
		sudo apt-get update >>${LOG_FILE} 2>&1
		sudo apt-get install -y ca-certificates >>${LOG_FILE} 2>&1
		sudo install -m 0755 -d /etc/apt/keyrings >>${LOG_FILE} 2>&1
		sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc >>${LOG_FILE} 2>&1
		sudo chmod a+r /etc/apt/keyrings/docker.asc >>${LOG_FILE} 2>&1

		# Add the repository to Apt sources:
		echo \
			"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" |
			sudo tee /etc/apt/sources.list.d/docker.list >/dev/null
		sudo apt-get update >>${LOG_FILE} 2>&1
		sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin >>${LOG_FILE} 2>&1
	else
		echo_info "Docker-CE is already installed"
	fi

	# Using "service" command for WSL Ubuntu which does not support systemd and ignoring errors (e.g.: Docker already running)
	sudo systemctl start docker >>${LOG_FILE} 2>&1 || sudo service docker start >>${LOG_FILE} 2>&1 || true

	local IS_DOCKER_COMPOSE_EXISTS=1
	sudo docker compose version >>${LOG_FILE} 2>&1 || IS_DOCKER_COMPOSE_EXISTS=0
	if [[ ${IS_DOCKER_COMPOSE_EXISTS} -eq 0 ]]; then
		confirm "Docker Compose is not installed. Do you want to install Docker Compose now?"
		echo_info "Installing Docker Compose..."
		sudo apt-get update >>${LOG_FILE} 2>&1
		sudo apt-get install -y docker-compose-plugin >>${LOG_FILE} 2>&1
	else
		echo_info "Docker Compose is already installed"
	fi
}

function handle_redhat() {
	local IS_WGET_EXISTS=1
	wget --version >>${LOG_FILE} 2>&1 || IS_WGET_EXISTS=0
	if [[ ${IS_WGET_EXISTS} -eq 0 ]]; then
		confirm "wget is not installed. Do you want to install wget now?"
		echo_info "Installing wget..."
		sudo dnf -y -q install wget >>${LOG_FILE} 2>&1
	else
		echo_info "wget is already installed"
	fi

	local IS_SSHPASS_EXISTS=1
	sshpass -V >>${LOG_FILE} 2>&1 || IS_SSHPASS_EXISTS=0
	if [[ ${IS_SSHPASS_EXISTS} -eq 0 ]]; then
		confirm "sshpass is not installed. Do you want to install sshpass now?"
		echo_info "Installing sshpass..."
		sudo dnf -y -q install sshpass >>${LOG_FILE} 2>&1
	else
		echo_info "sshpass is already installed"
	fi

	local IS_DOCKER_EXISTS=1
	docker --version >>${LOG_FILE} 2>&1 || IS_DOCKER_EXISTS=0
	if [[ ${IS_DOCKER_EXISTS} -eq 0 ]]; then
		confirm "Docker-CE is not installed. Do you want to install Docker-CE now?"
		echo_info "Installing Docker-CE..."
		sudo dnf -y config-manager --add-repo "https://download.docker.com/linux/centos/docker-ce.repo" >>${LOG_FILE} 2>&1
		sudo dnf -y install docker-ce docker-ce-cli >>${LOG_FILE} 2>&1
		sudo systemctl enable --now docker >>${LOG_FILE} 2>&1
	else
		echo_info "Docker-CE is already installed"
	fi

	local IS_DOCKER_COMPOSE_EXISTS=1
	sudo docker compose version >>${LOG_FILE} 2>&1 || IS_DOCKER_COMPOSE_EXISTS=0
	if [[ ${IS_DOCKER_COMPOSE_EXISTS} -eq 0 ]]; then
		confirm "Docker Compose is not installed. Do you want to install Docker Compose now?"
		echo_info "Installing Docker Compose..."
		sudo dnf -y -q install docker-compose-plugin >>${LOG_FILE} 2>&1
	else
		echo_info "Docker Compose is already installed"
	fi
}

function confirm() {
	[[ "$#" -ne 1 ]] && echo_error "Wrong number of arguments for ${FUNCNAME}" && exit 1

	if [[ ${ASSUME_YES} -eq 1 ]]; then
		return
	fi

	local QUESTION=$1
	local REPLY
	while true; do
		read -r -p "${QUESTION} [y/N]: " REPLY

		if [[ "${REPLY}" =~ ^[Yy]$ ]]; then
			return
		elif [[ "${REPLY}" =~ ^[Nn]$ ]] || [[ -z "${REPLY}" ]]; then
			echo_info "Please manually install the software and rerun the script"
			exit 1
		fi
	done
}

#################
# Invoking main #
#################
main "$@"
